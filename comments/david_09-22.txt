
  David's comments from 09/22

===============================================================================

General:

- ** Q: where to put figure? Separate page? And of section?
- ** Q: should I write T'_TM , Var(T_TM) for toy model expressions ?

- Treat Greenland as same for all datasets.
  make script ??


- ** Issues with inter-annual variability. 
     David calls it 'monthly variability' sometimes or even 'monthly to decadal'
     Is section 2.3.1 ok now?

- use '=' instead of '\approx' when stating toy model parameterizations.
  ** redo diagrams !!

- when to add moisture contours ??? 
  David mentioned it about the autocorrelations 


-------------------------------------------------------------------------------

Chapter 2:

- equation 2.3.2 about 'summer average' Var(X)

- Should I lighten the caption in 2.3.4 as 'same as fig 2.12 but with ...'

-------------------------------------------------------------------------------

Chapter 3:


- 3.1.2 opening statement. ???

- David agrees about not showing melting snow analysis.

- David is fine with 3.3.3.

- end of 3.3.4 about 'globally' and 'across-dataset evaluations'

- David agrees with dropping 'anomaly' in 3.4

- issues with end of 3.4.2 (how m b' might contribute to E')

- ** include inter-dataset comp of H s' onto m' in 3.4.3 

- 3.4.7 <E_0,F> must be > 0 in order to parameterize E_0 with F. David is wrong.
        as well as nu_E >= 0 ...

- 3.5.1 issues with 'chi' restriction.
- 3.5.2 issues with 'land-surface processes'. They are not feedbacks!!!

-------------------------------------------------------------------------------

Chapter 4:

- 4.1.3 issues with gamma lw having no spatial dependence. (dropped on 09/27)

- issues with land-surface amplified/damped, defined by gamma^2 Var(T) / Var(F)!
  -- probably clear enough now.

-------------------------------------------------------------------------------

Chapter 5:

-------------------------------------------------------------------------------

===============================================================================

