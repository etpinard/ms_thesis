
  Marcia's comments from 09/22

===============================================================================

General: 

- ** call 'G' heat -- transport -- through ground

- find a way to quantify 'small' errors.

- more reference to relevant equations in captions!

-------------------------------------------------------------------------------

Chapter 1:

- more about T 2-meters. 

- need a separate paragraph for 'very clear statement of what your procedure
  will be, how you will use the GCMs, the toy model and the observations.

- koster.tex : interchange 'small/large' and 'Var(T)' , make ratio bigger.

- Put table 1.1 in an appendix

-------------------------------------------------------------------------------

Chapter 2:

- ** summary after each subsections ** 
     nah don't think so, that's what 2.4 is for.

- 2.1 should focus only on what the observations consist of. 
  Then discuss what you find in terms of the land atmosphere interactions from
  each.

  Perhaps add a few more lines describing U. of Delaware observations
  -- what is there to described ?

- keep 2.3.1 in 2.3 but add one line to introduce 2.3.

- ** issues with 'sample'


- *** 2.3.2: add 'soil moisture' ++ 'soil moisture in datasets'


- make a sub sub section for E and one for H_s

- 2.3.4 put correlation results in displayed equations.

-------------------------------------------------------------------------------

Chapter 3:

- 3.1 Include units when enumerating budget terms.
  -- 09/27 now in appendix --

- 3.1 Wouldn't memory damp the fluctuations ? 

- ** Appendix about mean budget residuals is not necessary. sweet!
- No additional analysis about the effects of melting snow. sweet!

- 3.2.2 You won't use C_eff, you'll tune the sensitivity which had a time scale
  in it. Why not just non-dimensionality time and then discuss the sensitivity,
  avoid this discussion?

- 3.2.3 Conclusions are all quantitative. You will approximate their
  contributions to the overall uncertainties in T and m quantitatively; lay this
  out now.

- 3.2.3 where does the m bar contour fit on the Bowen ratio plots.

- 3.3.3 Consider --- ??? A general concerned about presentation of projections

- 3.3.4 'physically consistent' condition should be first.

- 3.3.4 Should define a symbol for 'fraction of variance explained'

- 3.4 dropping 'surface' is fine, not 'anomaly'


-------------------------------------------------------------------------------

===============================================================================

