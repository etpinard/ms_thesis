#! /bin/bash
#
# Script that backups thesis/ onto the UW hard drive.
#
# ===============================================================================

# Address of UW server
UW='etienne@cyclone.atmos.washington.edu'

# Path to hard drive
PATH1='/home/disk/p/etienne/thesis/'

# Thesis path on laptop 
# (with trailing '/' to copy content and but not the parent folder)
IN='/home/etienne/documents/proj/thesis/'

# Find today's date and make final out folder 
DATE=`date +"%m-%d"`
OUT='thesis_'$DATE'/'
ZIP='thesis_'$DATE.zip

# Destination
DESTINATION="$UW:$PATH1$OUT"

# -------------------------------------------------------------------------------

## 1) clean up 'thesis/' folder
latexmk -c 
rm thesis.dvi
rm thesis.ps
rm thesis.bbl

## 2) zip '.tex' files 
zip -R $ZIP content/* thesis.tex uwthesis.cls thesis_references.bib thesis_font.sty thesis_macros.tex thesis.pdf

## 3) Backups files using rsync
echo -e "\nBackuping $IN to ... \n\t $DESTINATION"
rsync -ah --info=progress2 "$IN" "$DESTINATION"

# -a (archives) , perfect for backups (see rsync -help)
# -h (human-readable) 

rm $ZIP

# -------------------------------------------------------------------------------
