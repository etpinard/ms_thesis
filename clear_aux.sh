#! /bin/bash

rm content/*.aux
rm thesis.aux thesis.log
rm thesis.lof thesis.toc
rm thesis.bbl thesis.blg
rm thesis.ps thesis.dvi
