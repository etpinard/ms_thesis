%
% Chapter 2
%
% -------------------------------------------------------------------------------
\section{Observations}
\label{sec:la_obs}
%
% - refer to glossary about summer?
% - more about U.Del obs?
%
% obs_Tbar , obs_Pbar  as  fig:obs_T_P
%
% -------------------------------------------------------------------------------

Past studies on summertime surface temperature variability have identified the
role of evapotranspiration ($E$) during anomalously warm years (see chapter
\ref{chp:intro} for references). Decreased $E$ leads to higher-than-usual
surface temperatures through a reduction in evaporative cooling at the surface. 
%
More generally, we note that evapotranspiration over land is an important
component of Earth's hydrological cycle. For instance, using the ERA-Interim
reanalysis product, \citet{vanderent10} found that on a continental scale 40\%
of all precipitation falling over land originates from land $E$. Energetically,
$E$ over land has appreciable effects: it uses roughly 50\% of the total
insolation reaching land surfaces \citep{sene10}. 
%
As implied by the Koster diagram (figure \ref{fig:koster}), the behavior of
evapotranspiration over land is heterogeneous across different climate regimes.
%
To better our understanding of the impacts of land-atmosphere interactions on
surface temperature at the global scale, it is useful to partition Earth's
climate into characteristic regimes. 
%
To do so, we use the University of Delaware global data bank of gridded
observations of surface temperature and precipitation \citep{udel}.
%
Figure \ref{fig:obs_T_P} displays University of Delaware observations of summer
average surface temperature (2-meter above the surface) and mean precipitation
rate over land for the 30-year period starting in December 1969.  Both panels
show summer average results defined as:

\begin{equation}
	\hskip-1em\text{Summer}\;=\;
	\nicecases{\text{June, July, August (JJA) }}{if}{\vartheta > 0^\circ}
						{\text{December, January, February (DJF)}}{\vartheta \le 0^\circ}
	\label{eq:summer}
  \medskip
\end{equation}

\noindent%
%
where $\vartheta$ is the latitude.
%
The above definition of summer corresponds to the rainy season in most tropical
locations.
%
Since a soil moisture climatology does not exist, we use mean precipitation as a
surrogate for the soil moisture content. Mean precipitation gives an approximate
representation of the moisture content in soils in summer over most continental
locations. 
%
However, mean precipitation systematically underestimates soil moisture content
in the high latitudes where melting snow and frozen soil water are important
sources of moisture. Discrepancies between mean precipitation and soil moisture
are also expected in mountainous regions where runoff is important. 
%
A comparison of soil moisture content in two GCMs and two reanalysis products is
presented in section \ref{sec:data_results}. 

With these considerations in mind, we partition the globe into the following
five \emph{climates zones}:

\begin{enumerate}

\item Tropical:
%
the Amazon, the Congo, India, Southeast Asia, Indonesia, Southern China;

\item Dryland:
%
central United States (US hereafter), Eastern Europe, the Sahel and Australia;

\item Mid-latitude wet:
%
Eastern North America, Danube basin in Europe, Northeast China;

\item High-latitude:
%
Canada, Russia and Alaska above 60$^\circ$N;

\item Desert:
%
the Sahara, Middle East, southwest US.

\end{enumerate}

\noindent%
%
Here, the expression \emph{climate zones} is used instead of \emph{climate
regimes} to distinguish the former from the evapotranspiration regimes discussed
in chapter \ref{chp:intro} and elsewhere in this thesis. The climate zones are
defined by glancing at figure \ref{fig:obs_T_P} without any specific criteria on
the soil moisture regime.  They are meant as an aid in our discussion of results
throughout the thesis.

The tropical, mid-latitude wet and high-latitude climate zones are expected to
have sufficient supply of soil moisture available so that evapotranspiration at
all times is not limited --- or affected by --- soil moisture content.  In the
Koster diagram, they are expected to display characteristics of energy-limited
$E$ regime (\ie $m > m\subt{crit}$).  
%
By contrast, the dryland and desert climate zones are expected to fall into the
moisture-limited $E$ regime (\ie $m < m\subt{crit}$).
%
The aforementioned claims are assessed in section \ref{sec:data_results}.

\vfill
\newpage

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[scale=0.6]{chap2/obs_Tbar}}
\vskip-1em
\hskip0.9em
\sidesubfloat[]{%
  \includegraphics[scale=0.6]{chap2/obs_Pbar}}

\caption[University of Delaware 1969-1999 summer average surface temperature and
precipitation rate.]%
%
{University of Delaware 1969-1999 summer average surface temperature (panel a)
and precipitation rate (panel b).}

\label{fig:obs_T_P}
\end{center}
\end{figure}

%\newpage

