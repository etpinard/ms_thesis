%
% Chapter 6 (last)
%
% -------------------------------------------------------------------------------
\chapter{Summary and unanswered questions}
\label{chp:conclu}

In this study, we have developed a simplified theoretical framework for
understanding summertime surface temperature variability over land. 
% 
This framework, called the toy model, was built on physical relationships
quantified using four datasets: two CMIP3 GCMs (\ccsm and \hadgem) and two
reanalysis products (\era and \ncep). 
%
Our approach allowed \emph{(i)} the illumination of the processes governing
temperature variability without the use of GCM simulations, \emph{(ii)}
attribution of GCM errors, and \emph{(iii)} insight on the veracity of the
projections of temperature variability in the late \twentyfirst century.

%% chapter 2

In chapter \ref{chp:land_atm}, we found that correlations between
evapotranspiration ($E$) and soil moisture as well as between $E$ and radiation
forcing delineate two regimes of land-atmosphere interactions.
%
Our results are consistent with the two-evapotranspiration regime (moisture and
energy-limited) behavior previously described in the Koster diagram
\citep{koster09_droughts}. 
%
Our analysis revealed that the critical moisture value, that approximately
separates the two evapotranspiration regimes in GCMs was significantly smaller
than in the reanalysis products.
%
Moreover, we found that the moisture-limited evapotranspiration regime is of
greater areal extent in the GCMs than in the reanalysis products.
%
%Additionally, we found that the GCMs' surface temperature variability errors
%correlated better with errors in evapotranspiration regime locations than with
%errors in summer mean surface-layer soil moisture.

% -------------------------------------------------------------------------------

%% chapter 3

In chapter \ref{chp:toy_model}, we derived simplified equations for the monthly
mean surface temperature ($T$) and surface-layer soil moisture ($m$) anomalies.
%
%The resulting expressions were diagnostics of fluctuations about the monthly
%climatological means in the surface energy and soil moisture budgets.
% 
We parametrized six land-surface processes present in the surface energy and
soil moisture budgets using only state variables ($T$, $m$) and forcing
functions ($F$, $P$). 
%
In this way, $T$ as well as $m$ anomalies are represented as linear combinations
of the forcings $F$ and $P$ weighted by a collection of (toy model)
coefficients.
%
We implemented a moisture-limited and energy-limited (or dry/wet)
parameterization algorithm for evapotranspiration. Interestingly, sensible heat
flux anomalies were best represented by soil moisture.
%
The corresponding toy model expression for surface temperature variance
underestimates the temperature variance in the four datasets by 25\% to 50\% on
average --- mainly due to inaccuracies in the parameterization of the sensible
heat flux.

%-- comment on Koster LE/Fnet vs toy model's nuE and lambda in chapter 4 (4.3.2)
%   Koster has E/F ~ m , we have E ~ m' (more compact, one can show that LE/F
%   anomalies are mostly driven by E' is soil-moisture limited regimes).

% -------------------------------------------------------------------------------

%% chapter 4

Chapter \ref{chp:interpret} revealed that the toy model coefficients contain
valuable information about surface temperature variability.
%
We were able to extend Koster's description of two evapotranspiration regimes to
surface temperature variability regimes: the land-surface amplified (where the
variability in the $T$ response is greater than the input forcing variability)
and damped (where the variability in $T$ is less than the input forcing
variability) regimes.
%
The land-surface amplified regime is associated with (but not perfectly
equivalent to): \emph{(i)} the moisture-limited $E$ regime, \emph{(ii)} soils
that have mean soil moisture less than a critical value $m\subt{crit}$,
\emph{(iii)} soil moisture anomalies that amplify $T$ variability, and
\emph{(iv)} weak $E$ efficiency given a radiation forcing anomaly.
%
In contrast, the land-surface damped regime is associated with: \emph{(i)} the
energy-limited $E$ regime, \emph{(ii)} soils that have mean soil moisture
greater than $m\subt{crit}$, \emph{(iii)} soil moisture anomalies that damp $T$
variability, and \emph{(iv)} large $E$ efficiency given a radiation forcing
anomaly.
%
The two regime behavior is best described by contrasting the land-surface
response to precipitation anomalies in the two $E$ regime. This is perhaps the
main result of this study.
%
In brief, radiative effects of precipitation induce temperature anomalies of the
opposite sign (almost) always (\eg rainy days are cloudy and thus cooler than
normal).
%
Then, in the moisture-limited $E$ regime, the positive soil moisture anomalies
that are generated provoke large positive $E$ anomalies and that amplify the
cooling associated with the initial negative radiation anomaly and hence amplify
the temperature variance.
%
By contrast, in the energy-limited regime, $E$ is independent of $m$ and the
radiative effects of $P$ (associated with a positive precipitation anomaly) are
partially compensated for by reduced $E$ anomalies associated with reduced
radiation forcing and hence the $E$ anomalies damp the response.

% -------------------------------------------------------------------------------

%% chapter 5

In chapter \ref{chp:apps}, our results suggest that surface temperature
variability errors in the \ccsm and \hadgem GCM can be mostly traced back to a
misrepresentation of mean-state soil moisture in the GCMs.
%
We also argued that regions that are projected to change evapotranspiration
regime from the energy-limited regime to moisture-limited regime by the end of
the \twentyfirst century will experience a large increase in temperature
variability.

% -------------------------------------------------------------------------------

%% Unanswered questions:

This study made use of two --- relatively old --- CMIP3 GCMs: the \ccsm and the
\hadgem. Figures \ref{fig:ccsm4_Var_T_bias_4panel} and
\ref{fig:hadgem2_Var_T_bias_4panel} presents surface temperature variability
errors in their latest CMIP5 versions of these two models (\ccsmfour and
\hadgemtwo).  The overestimations of surface temperature variability in dryland
climate is less pronounced in the CMIP5 version of these two models (see figure
\ref{fig:comp_Var_T_bias}). 
%
That said, surface temperature variability still increases significantly from
June to July in regions (\eg the central US and Southern Europe), a feature not
seen in the \twentyth century observations (see figure
\ref{fig:compobs_ssn_Var_T}).

Finally, we leave a few unanswered questions for future work. These include:

\begin{enumerate}

\item Why is the monthly variability in the sensible heat flux negatively
correlated with (and best predicted by) surface-layer soil moisture?
%
\label{enu:Q_Hs}

\newpage

\item In this study, we were able to link GCM temperature variability errors in
two GCMs to errors in mean-state soil moisture. Why are (those two) GCMs too dry
in the first place? 
%
In section \ref{sec:data_results}, we showed that both the \ccsm and the \hadgem
are characterized by errors in summertime mean surface temperature and
precipitation. Are those errors enough the explain the GCMs' erroneous soil
moisture content?
%
\label{enu:Q_dry}

\item The toy model is a diagnostic model that expresses surface temperature and
soil moisture variability about a climatology. 
%
The toy model is applicable for understanding summertime surface temperature and
soil moisture variability on time scales that range from a few weeks to decades.
%
Can our method for approximating processes at the land-surface interface be
extended to shorter time scales?
%
\label{enu:Q_time_scales}

\item Does bottom-layer soil moisture provide an appreciable source of
variability for surface temperature as suggested by the pronounced 1-month
lagged correlations between bottom and surface-layer soil moisture? 
%
\label{enu:Q_bottom}

\item Are the effects of non-linear land-atmosphere feedbacks (\eg soil-moisture
recycling) relevant for summertime surface temperature variability?
%
If so, under what conditions are surface temperatures more susceptible to the
effects of these feedbacks?
%
\label{enu:Q_feedback}

\item%
%
%Can our description of surface temperature variability which presents the
%land-surface as either an amplifier or a damper of forcing variability be
%diagnosed only by observational variables available large spatial and temporal
%scales (\eg temperature, precipitation but not soil moisture nor surface
%fluxes)? 
%
Can we use the results from the toy model to identify locations in the
land-surface amplified and land-surface damped temperature variability regimes
in nature without resorting to GCMs and/or reanalysis products?
%
\label{enu:Q_descp}

\end{enumerate}

\newpage

%- list other potential controlling mechanism (variable) other than
%  (surface-layer) soil moisture 
%  (Tbar --> gamma ??)
%
%--> The toy model gave us an interpretation for surface temperature variability
%behavior. However, the toy model is not perfect ...

% -------------------------------------------------------------------------------

\newpage

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap6/ccsm4_Var_T_bias_4panel}

\caption[\ccsmfour error ratio of surface temperature variance, summer average
and three summer month separately]%
%
{Error ratio (equation \ref{eq:Err}, 1969-1999) of surface temperature variance,
for summertime and for each of the three summer month separately. [\ccsmfour
  data]}

\label{fig:ccsm4_Var_T_bias_4panel}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap6/hadgem2_Var_T_bias_4panel}

\caption[As in figure \ref{fig:ccsm4_Var_T_bias_4panel} but for the
\hadgemtwo.]%
%
{As in figure \ref{fig:ccsm4_Var_T_bias_4panel} but with \hadgemtwo data.}

\label{fig:hadgem2_Var_T_bias_4panel}
\end{center}
\end{figure}

\pagebreak
