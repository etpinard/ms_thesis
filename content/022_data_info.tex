%
% Chapter 2
%
% -------------------------------------------------------------------------------
\section{Datasets background}
\label{sec:data_info}
%
% - 'appreciably well' in era40 section - what does that mean?
%
% - add line about how the field capacity is important to GCM results 
%   \citet{sene06_memory}. no!
%
% -------------------------------------------------------------------------------

%% Introduction to section

%As mentioned previously in chapter \ref{chp:intro}, numerical modeling in the
%study of land-atmosphere interactions on climatic time scales is omnipresent.
%%
%Land surface models (LSMs herein) compute land surface variables using
%theoretical and empirical relationships between land and atmospheric variables.
%%
%LSMs are vital components to both GCMs and reanalysis products as they provide
%boundary conditions to atmospheric models. 
%%
%In GCM-based studies, the effects of particular land surface variables on
%climate can be readily isolated. For instance, \citet{sene06_coupling} isolated
%the effects of soil moisture on surface temperature variability tendencies by
%comparing simulations with freely-evolving LSMs to simulations with constant
%land-surface conditions.
%%
%In this case, the use of reanalysis products, would drive the solution back
%toward a predetermined state and not allow interactions between the surface and
%atmosphere \citep{oglesby02}.
%%
%That said, as discussed in chapter \ref{chp:intro}, GCMs do not offer a perfect
%representation of the climate system.
%%
%As a result, reanalysis products are deemed advantageous as some LSM-derived
%variables in reanalysis products are constrained by observations in order to
%give the most accurate description of the climate system possible. 

We use both GCM and reanalysis datasets in this study because previous studies
have shown that there are large difference between different models and
different reanalysis products when it comes to the state of the land surface
\citep{jimenez11,sene06_memory,vidale07}.
%
%. Comparisons between data of freely-evolving and constrained LSMs prove useful
%in understanding land-atmosphere interactions and the sources of GCM errors. 
%%
%However, we caution that large model differences between different LSMs have
%been detected in past studies.
%
%For example \citet{jimenez11} found in an inter-comparison of twelve LSMs that
%estimates of latent and sensible heat fluxes diverge significantly; a
%inter-model spread of 20 \Wm[-2] in the annual mean was noticed.
%%
%Analysis conducted in \citet{sene06_memory} and \citet{vidale07} also
%highlights the need of several datasets to attain generalizable conclusions on
%the land surface.
%
In an effort to identify model-dependent results, we analyse four datasets ---
two GCMs (\ccsm, \hadgem) and two reanalysis products (\era and \ncep) --- which
use four different land surface models (LSMs herein).
%
In this document, the noun \emph{dataset(s)} refers to the model outputs from
the two GCMs and the two reanalysis products excluding University of Delaware
\emph{observations}.
%
The datasets are briefly described in sections \ref{ssec:ccsm3} through
\ref{ssec:ncep_doe}.  But first, some valuable information on LSMs is provided.

\subsection{Land Surface Models (LSMs)}
\label{ssec:LSMs}

LSMs compute outward (\ie from the surface) fluxes of energy, water, carbon
dioxide and trace gases as functions of both the state of the atmosphere and the
land surface. 
%
Turbulent heat fluxes are computed using bulk formulae. For the sensible heat
flux $H_s$, we have:

\begin{equation}
  H_s \;=\; \frac{1}{r\subt{aero}}\, 
  \big( \pt \Tsk - T \pt \big)
  \enskip,
  \label{eq:Hs_bulk}
\end{equation}

\noindent%
%
where $r\subt{aero}$ is aerodynamic resistance, $\Tsk$ is the surface skin
temperature and $T$ is the air temperature above the surface friction layer
(typically at 2 meters above ground/canopy).
%
The aerodynamic resistance represents the role of turbulence: decreasing wind
speed, increasing atmospheric stability and decreasing surface roughness all
increase the aerodynamic resistance.
% 

The surface evapotranspiration flux, $E$, is driven by a gradient of humidity at
the land-surface interface: 

\begin{equation}
  E \;=\; \frac{1}{r\subt{aero}}\, 
  \big( \pt q\subt{skin} - q \pt \big) 
  \;=\; \frac{1}{r\subt{aero}+r\subt{soil}}\, 
  \big( \pt q\subt{sat}(T\subt{skin}) - q \pt \big) 
  \enskip,
  \label{eq:E_bulk}
\end{equation}

\noindent%
%
where $q$ is the specific humidity at 2$\m$ ($\kg$ of water vapor by $\kg$ of
air), $q\subt{sat}(T\subt{skin})$ is the saturation specific humidity at the
surface skin temperature determined by the Clausius-Clapeyron relation. 
%
The soil and vegetation (and canopy) controls on evapotranspiration are
represented in $r\subt{soil}$. 
%
Over lakes and the ocean, $r\subt{soil}{=}0$ and $E$ is not limited by
availability of moisture at the land-surface interface. In this case, $E$ is
equal to the potential evapotranspiration.
%
Over vegetated areas, moisture availability for evapotranspiration is controlled
by biophysical processes, namely via stomata.
%
Stomata are pores found in leaves used to control gas exchange including water
vapor.
%
The measure of the rate of passage through stomata is called the \emph{stomatal
conductance} and is equal to the reciprocal of $r\subt{soil}$ in vegetated
areas.

The LSMs used in the GCMs and reanalysis products analysed in this study are
classified as \emph{third-generation land surface models}.  They include an
explicit representation of key physiological processes impacting vegetation.
%
For example, the stomatal conductance is a function of the photosynthetic
assimilation, \COtwo concentration at leaf surface, relative humidity at leaf
surface and atmospheric pressure.
%
Relative contributions of each factor are strongly dependent on the plants'
physiology classified under \emph{plant functional types} (or PFTs).
%
Although third-generation LSMs perform better than their predecessors, the
increase in complexity also has a down side: inclusion of new processes
introduces more model parameters, which are often poorly constrained
\citep{sene10}.
%
As well, LSMs are still not fully mature in particular with respect to
biogeochemical processes and vegetation dynamics \citep{moorcroft06}.

\newpage

%\tcomment{Emphasize again the need of several datasets?}

%\comment{Maybe: a few words on how infiltration is computed? Darcy flow. It is
%less important for my work though.}

\subsection{NCAR's \ccsm}
\label{ssec:ccsm3}

The Community Climate System Model version 3 (referred to as the \ccsm herein)
is a coupled climate model developed by NCAR. This study makes use of the
\ccsm's IPCC AR4\footnote{%
%
The Intergovernmental Panel on Climate Change Fourth Assessment Report.}
%
simulation archives, run at T85 resolution (approximatively 1.4$^\circ$
resolution in longitude and latitude) for the atmosphere and the land surface
\citep{collins06}.
%
The land-surface scheme in the \ccsm is the Community Land Model version 3.0
(CLM3.0 herein). The CLM3.0 is a complex LSM. For example, from its technical
documentation \citep{clm3} we note that radiative transfer is computed at the
land-surface interface and within the vegetated canopy.  Diffused and direct
beam solar radiation are accounted for. Aerodynamic resistances ($r\subt{aero}$
in equations \ref{eq:Hs_bulk} and \ref{eq:E_bulk}) over vegetated areas are
separated into surface to canopy and canopy to atmosphere components.
%
Subgrid-scale processes are included: individual grid boxes can be comprised of
up to 4 our of the 15 available plant functional types (PFTs). Each PFT features
a unique set of optical, emission and aerodynamic properties.
%
\citet{qian06} remarked that, when forced by a reanalysis dataset, the CLM 3.0
reproduces many aspects of the long-term mean, annual cycle, interannual and
decadal variations and trends of stream flow for many large rivers.  Moreover,
observed soil moisture variations over Illinois and parts of Eurasia are
generally well simulated. 

% LSM is CLM 3.0  (4.0 in the CCSM4.0)
% talk about wilting point, like in proposal ? cite sene_mem

%\citet{sene_mem} Community Land Model version 3 (CLM3). Complete water
%and energy budget. Impact of vegetation is explicitly
%parameterized. Subgrid-scale processes are accounted.

\subsection{Hadley Centre's \hadgem}
\label{ssec:hadgem1}

This study also makes use of the Hadley Centre Global Environment Model version
1 (referred to as the \hadgem herein) IPCC AR4 simulation archives.
%
The resolution of theses runs is 1.25$^\circ$ $\times$ 1.875$^\circ$ in latitude
and longitude \citep{johns06}.
%
The \hadgem is comprised of the MOSES-II LSM. A technical description of the
MOSES-II can be found in \citet{moses2}. MOSES-II is a LSMs of moderate
complexity. It does not have the \ccsm's detail in the representation of
canopy-scale processes. 
%
Subgrid-scale processes are accounted for using up to 4 PFTs per grid box out of
the eight PFTs available.
%
The plant wilting point is not represented in MOSES-II. Hence, the
moisture-limited regime of the Koster diagram passes through the origin.

%\citet{sene_mem} MOSES2. Complete water and energy budget. Impact of vegetation
%is explicitly parameterized. Subgrid-scale processes are accounted.

\subsection{ECMWF \era}
\label{ssec:era40}

The \era (as referred to herein) is a global reanalysis product produced by
European Centre for Medium-Range Weather Forecasts (ECMWF). The \era's archives
start in September 1957 and end in August 2002. They are output on a T159
spectral grid (2.5$^\circ$ latitude and longitude resolution) \citep{uppala05}.
%
The LSM used in the \era reanalysis is the Tiled ECMWF Surface Scheme for
Exchange over Land (TESSEL as referred to herein). Subgrid scale processes are
emphasized in the TESSEL and 20 PFTs are used \citep{vandenhurk00}.
%
%Soil moisture and soil temperature in the \era are corrected using air
%temperature and relative humidity observations from surface (SYNOP) stations
%\citep{douville00}.
%
Several studies evaluated the surface turbulent fluxes in the \era against
observations from flux towers data in various locations.
%
\citet{betts03,betts06} found the turbulent fluxes compare well against
observations.
%
\citet{decker12} noted that of the four reanalysis products analysed in their
study, the \era has lowest overall bias in terms of 6-hourly latent and sensible
heat fluxes as well as in precipitation.
%
Hydrological variables were evaluated by  \citet{su06}. Here again, \era matched
observations well.
%
\citet{li05} evaluated the performance of three reanalysis datasets (including
the \ncep, described next in section \ref{ssec:ncep_doe}) using soil moisture
observations at 10 sites in China over a 19-year interval.  They found that the
variability in soil moisture on monthly and interannual time scales in the \era
reanalysis was closest those observed among the three reanalysis datasets.

%as well as soil moisture anomalies' temporal scale. 

%% more details:
%
%\citet{betts03} \era \vs Mackenzie Global energy and water cycle experiment
%study.  The \era represents well variability in monthly precipitation. Annual
%evaporation is 30\% greater in the \era. The annual runoff is comparable to the
%annual stream flow but the interannual variability is poor correlated. 
%% should I talk about P and T ... nah leave it 
%
%\citet{betts06} \era Boreal Ecosystem Monitoring Study in central Saskatchewan
%small errors of T and q but high bias in E, high bias in surface net surface.
%In summer, \era has too little clouds. Errors could be because of land-surface
%models or cloud parameterization. But, (important for us), the statistical
%relationships between surface parameters are similar in model and data.
%
%\citet{su06} \era is quite consistent with observations in terms of
%interannual, spatial and latitudinal variations. Arctic river basins compared
%to off-line simulation of the Variable Infiltration Capacity macro scale
%hydrology model.
%% do I need this?
%
%\citet{decker12} Using FLUXNET network of flux towers (33 locations in NH) \era
%has lowest overall bias of the four reanalysis products analysed in this study
%in LH and SH (6-hourly), lowest P bias.  (ERA-interim is best for T and Var(P)
%though ...)

%\citet{jiminez11}
%They use the ERA interim (not the era40) but it has the TESSEL LSM
%ERA LH close to ensemble mean. of (how many models)


\subsection{NCEP-DOE Reanalysis II}
\label{ssec:ncep_doe}

The NCEP-DOE Reanalysis II (referred to as \ncep herein) is a reanalysis product
produced by the National Center for Environmental Prediction (NCEP). The \ncep
is on-going: its temporal coverage is currently from January 1979 to December
2012.  The \ncep products are output on a T62 gaussian grid (approximatively
1.9$^\circ$ resolution in longitude and latitude).
%
It makes use of Oregon State University Noah LSM \citep{kanamitsu02}. 
%
The \ncep utilizes pentad mean observed precipitation to correct model
precipitation in driving the soil model, which makes the evolution of soil
moisture more realistic \citep{lu05}.
%
\citet{lu05} warn that variables such as heat fluxes, humidity and surface
temperature are not directly influenced by observed quantities and therefore
they should be interpreted with caution.
%
Correspondingly, \citet{jimenez11}, in evaluation an inter-model of 12 LSMs,
found that surface fluxes in the \ncep product were outliers compared to the 11
other products examined.
%
That said, \citet{li05} found that soil moisture in the \ncep is in good
agreement with the limited observational data (better than the \era) in terms of
seasonal mean, yearly mean and amplitude of the seasonal cycle.

%\citet{sene06_mem}
%Oregon State University (OSU) LSM. Complete water and energy
%budget. Impact of vegetation is explicitly parameterized.
%- Field capacity is a function of the soil type (unlike GCMs')

%Latent heat is big in NCE (outlyer on every plot it looks like)
%
%\citet{li04} \ncep has a better agreement in terms of mean, amplitude of the
%seasonal cycle and interannual variability. 
%(They use 19 yr soil moisture data in China)
%
%\ncep has higher correlations with observations, and a good representation of
%the mean state.

\subsection{Data information}
\label{ssec:vars_list}

This study focuses on temperature variability on monthly time scale, over
continental regions. All oceanic grid points and grid points over lakes and
glaciers (\ie of constant soil moisture) have been removed from the four
datasets (as in figure \ref{fig:obs_T_P}).
%
We make use of data from the 30 summers from December 1969 to August 1999 (20,
1979-1999 for the \ncep) where \emph{summer} is defined as in section
\ref{sec:la_obs} (see equation \ref{eq:summer}).
%
The GCM outputs are taken the \twentyth century simulations (the \texttt{20c3m}
code name, which include all natural and anthropogenic forcings, which were used
in the IPCC AR4 report).
%
Refer to table \ref{tab:vars} in appendix \ref{app:list_of_vars} for a list of
variables used in this study.

In each LSM, the surface energy fluxes are computed at 2 meters above the canopy
height (2\m above ground in non-vegetated areas).
%
Moreover, the surface runoff flux is positive definite: it only includes the
transport of water from land grid points directly into oceans. 
%
Evapotranspiration ($E$) is computed using the surface latent heat flux output.
In this study, we consider a constant latent heat of vaporization (its value at
20\degC) for the globe:

\begin{equation}
  E \;=\; \frac{( \pt \text{surface latent heat flux} \pt )}{L} 
  \quad,\quad
  \text{with }\; L \;\equiv\; 2.45\; \J\kg[-1]
  \enskip.
  \label{eq:E_from_LH}
\end{equation}

\noindent%
%
The latent heat of vaporization is a weak function of temperature: $L$ increases
by 4\% from 0\degC to 40\degC. As a result, note that $E$ is slightly
overestimated if $T>20\degC$ and underestimated if $T<20\degC$.
%
The latent heat of sublimation is roughly 15\% greater than $L$. So, in regions
where sublimation makes an important contribution to the surface latent heat
flux, $E$, computed with equation \ref{eq:E_from_LH} is overestimated.  That
said, the induced errors are small: sublimation is a small contribution to the
$E$ and limited to high latitude locations. 

Soil moisture variables used in this study have units of \mm of water (or
equivalently $\kg\m[-2]$).
%%
%as outputted\footnote{%
%%
%The \ccsm and the \hadgem have very similar lists of archived variables
%(especially land-surface variables). This is the main reason for selecting
%these two particular GCMs in this study.}
%
%in both GCMs. 
%
The \ncep's surface-layer soil moisture content is converted from the volumetric
content.
%
For the \era, we perform a weighted average of the two top-most soil layers: the
archived variables \texttt{swvl1} ($m_1$ herein) and \texttt{swvl2} ($m_2$).
The depths from the surface of soil layer 1 and soil layer 2 are $d_1{=}7\cm$
and $d_2{=}21\cm$ respectively. In units of \cm, we compute the surface-layer
soil moisture content ($m$) from the \era outputs as:

\begin{equation}
  m \;=\; m_1 \;+\; 
  \left( \frac{10 - d_1}{d_2-d_1} \right)\, m_2
  \enskip.
  \label{eq:era40_m}
\end{equation}

\noindent%
%
That is, we assume that soil moisture is uniformly distributed within soil layer
2 to compute the moisture content from $d_1$ to the 10 \cm depth.  The symbol
$m$ refers to surface-layer (top 10 \cm) soil moisture content herein.  
%
Since the total soil depth varies between the four datasets, using the
surface-layer soil moisture allows for a head-to-head comparisons of the soil
moisture content between datasets. Alternatively, normalized metrics could have
used, such as volumetric soil moisture content.  But, with regards to the toy
model (more in chapter \ref{chp:toy_model}), the surface-layer soil moisture
content proves to be more central to this study.
%
For guidance, in the \era the saturation moisture content in the top 7\cm of
soil is 47\mm or 67\mm when extrapolated to the top 10\cm of soil
\citep{vandenhurk00}.
%
In turn, the field capacity, defined as the maximum amount of water that a
particular soil can hold against gravity, in the top 7\cm of soil is 0.32\mm (or
46\mm in the top 10\cm).
% 
However, note that in nature and in the other three datasets theses values are
not constant \citep{sene10}. 

%\vfill
%\newpage

% For reference, what is the saturation level in 10 cm of soil?
%max 47 mm in top 7 cm
%field cap 0.32 in "
%melting 0.17 in "
% -------------------

%% figures and tables
%\afterpage{\clearpage}
%\newpage
