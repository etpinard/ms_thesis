%
% Chapter 3
%
% -------------------------------------------------------------------------------
\section{Preliminaries}
\label{sec:tm_prelim}
%
% - end of intro to section (David wants more specific), good enough now ?
%
% - revise sections 3.1.1 and 3.1.2 
%
% -------------------------------------------------------------------------------

%% Section intro

To better understand the impacts of land-atmosphere interactions on summertime
surface temperature variability over land, we develop a simple, accurate and
physically-consistent \emph{toy model} of 
%
the land-atmosphere system.
%
In developing the toy model, we attempt to reproduce summertime surface
temperature inter-annual variability in two GCMs (\ccsm and \hadgem) and two
reanalysis products (\ncep and \era). As suggested by the results of chapter
\ref{chp:land_atm}, soil moisture is a decisive control of land-atmosphere
interactions. 

We first consider the surface energy and soil moisture budgets.  Neglecting the
effects of snow/ frozen water melt and ground transport of water, these are:

\begin{subequations}
\begin{align}
  \derv{U}{t} \;&=\; F \;-\; L\pt E \;-\; H_s \;-\; \Flu \;-\; G
 \label{eq:budgets_U} \\
  \derv{m}{t} \;&=\; P \;-\; E \;-\; R \;-\; I
 \label{eq:budgets_m} \enskip,
\end{align}
  \label{eq:budgets}
\end{subequations}

\vspace*{-1em}%
%
\noindent%
%
where $U$ is the surface energy which is proportional to the surface
temperature\footnote{%
%
Strictly speaking, $U$ represents the surface air enthalpy if considering the
surface air while $U$ represents the land surface's internal energy if
considering the land surface.}. 
%
The soil moisture content is $m$ (for now, in a given layer that extends
downward from the land surface; see section \ref{ssec:soil_layer} for further
discussion).
%
Recall that $F$ is the radiation forcing, defined as the sum of surface net
shortwave radiative flux ($\Fsw\down{-}\Fsw\up$) and surface downwelling
longwave radiative flux ($\Flw\down$). 
% 
The remaining symbols in \eqref{eq:budgets} are: $L$ the latent heat of
vaporization for water, $E$ the (surface) evapotranspiration flux, $H_s$ the
(surface) sensible heat flux, $\Flu$ (surface) upwelling longwave radiative
flux, $G$ the ground heat flux, $P$ the (surface) precipitation flux, $R$ the
surface runoff flux and $I$ the vertical transport of soil moisture. 

From the perspective of the land surface, both budgets can be partitioned into a
storage term ($\dervside{U}{t}$, $\dervside{m}{t}$), external (\ie atmospheric)
\emph{forcing} ($F$, $P$) and the sum of several \emph{land-surface processes}.
Surface temperature and soil moisture content are referred to as \emph{state
variables}.
%
We make a distinction between \emph{land-atmosphere interactions} and
\emph{land-surface processes}. The former is a generic term used to describe one
and two-way dependence relations between land-surface and atmosphere variables.
%
The latter refers specifically to the \emph{processes} that flux of energy and
moisture at the land-atmosphere interface: $E$, $H_s$, $\Flu$, $G$, $R$ and $I$,
in equation \ref{eq:budgets}. This terminology is used throughout in this
document.
%
The surface energy and soil moisture budgets are explicitly coupled through
evapotranspiration $E$ which is both a sink of surface energy and soil moisture.
%
%However, the equations of \eqref{eq:budgets} are far from closed.  In
%particular, the budget equations make no reference to the land-surface
%processes' control(s). Unfortunately, the land surface's governing equations
%found in Land Surface Models are not analytically solvable and theoretical
%deductions can only be made from numerical simulations. 

In this study, we seek a physically-consistent method for expressing the
variability in the land-surface processes in the budget equations
\eqref{eq:budgets}.  Closure is attained by implementing parameterizations
schemes that approximate land-surface processes as function of forcings
($F$,$P$) or/and the state variables ($T$,$m$). 
%
The resulting toy model yields an analytical expression for the variability of
both state variables as functions of the forcings only.  The toy model's
simplified version of the land surface gives a framework for understanding,
quantifying and isolating the effects of land-atmosphere interactions in surface
temperature variability. 

As in the analysis of chapter \ref{chp:land_atm}, we focus on monthly to decadal
variability in summertime; monthly average variables and a sample of 30-year
(20-year for the \ncep) are used. 
% 
Additionally, as in the previous chapter, we use the \emph{surface air}
temperature (measured at 2 meters above the ground) for toy-model state variable
$T$.  In doing so, the toy model expression for surface temperature variability
is consistent with the University of Delaware observations, at the expense of
making the state variables inconsistent with the surface energy budget which
relates fluxes at the land-surface interface.
%
Note that, when unambiguous, surface temperature is sometimes shortened to
simply \emph{temperature} in this document.

The development of the parameterization schemes, the toy model expressions for
surface temperature and soil moisture variability and the toy model's
performance are presented in sections \ref{sec:tm_method} through
\ref{sec:tm_perf}.  Section \ref{sec:anom_bud_eqs} presents important scalings
for the terms in the budget equations \eqref{eq:budgets_U} and
  \eqref{eq:budgets_m}.
%
But first, in this section, we investigate the autocorrelation properties of
both state variable and 
%
determine the depth over which soil moisture affects surface energy fluxes on
time scales of months to decades.
%
%layer that is best suited for the purposes of the toy model.  
 
\subsection{Time scales in summertime temperature variability}
\label{ssec:temp_time_scales}
%
% compobs_Corlag_T
%
% -------------------------------------------------------------------------------

We first investigate the autolag correlation properties of surface temperature
anomalies in summer.  The 1-month autolag correlation, shown in figure
\ref{fig:compobs_Corlag_T}, is computed as the arithmetic mean of the autolag
correlation of the first and second summer months and autolag correlation of the
second and third summer months. That is, with $j$ as the month index, $i$ the
year index and $\Deltat$ of 1 month, we compute:

\begin{equation}
  \R_X(\Deltat{=}1\,\text{month}) \;=\;
  \frac{1}{2(N\subt{year}{-}1)}\, 
  \Bigg[ 
  \frac{ \innerp{X_{i,1}'}{X_{i,2}'} }
       { \Var_1(X)^\half \Var_2(X)^\half } 
       \;+\;
  \frac{ \innerp{X_{i,2}'}{X_{i,3}'} }
       { \Var_2(X)^\half \Var_3(X)^\half } 
  \Bigg]
  \enskip,
  \label{eq:auto_corr}
\end{equation}

\noindent%
%
where $\Var_j(X)$ is the variance of $X$ at month $j$,
$\innerp{X_{i,j}}{X_{i,j}}$ is the inner-product of anomalies in $X$ at month
$j$ (where $j=1,2,3$ \ie JJA in Northern Hemisphere and DJF in the Southern
Hemisphere) and $N\subt{year}$ is the number of years.
%
The metric \eqref{eq:auto_corr} gives a summer-wide description of the 1-month
autolag correlation --- or \emph{memory} --- in a given variable. For reference
a red noise process with an $e$-folding time of 1 month has an autolag
correlation of $\R_X(\Deltat{=}1\,\text{month}){=}e\inv{\simeq}0.37$.  
%
From the top panel of figure \ref{fig:compobs_Corlag_T}, the observed surface
temperatures feature small but non-negligible memory. We find that surface
temperature memory is largest in the drylands, tropical climate zones and in
northern Canada, where autolag correlation values correspond to $e$-folding
times of greater than 1 month. Oddly, there are a few locations where
$R_T(\Delta{=}1\,\text{month})$ is negative but small (\eg a part of Siberia in
the observations).

To continue our description of memory in surface temperature anomalies, we
investigate the impacts of secular trends in the four datasets and in the
observations.
%
Figure \ref{fig:compobs_Corlag_T_detrend} presents the 1-month autolag
correlation in summer (as in equation \ref{eq:auto_corr}) of linearly detrended
surface temperature, $\R_{T\subt{detrend}}(\Deltat{=}1\,\text{month})$. 
%
We quantify the effects of the linear trend on the 1-month autolag correlation
by computing:

\begin{equation}
  \big| \R_T(\Deltat{=}1\,\text{month}) \big| 
  \;-\;
  \big| \R_{T\subt{detrend}}(\Deltat{=}1\,\text{month}) \big| 
  \enskip,
  \label{eq:diff_Corlag_T}
\end{equation}

\noindent%
%
where $|\,\cdot\,|$ are absolute\footnote{%
%
Since the 1-month autolag correlation of $T$ is positive for most of the globe
(see figures \ref{fig:compobs_Corlag_T} and \ref{fig:compobs_Corlag_T_detrend}),
note that the result of equation \ref{eq:diff_Corlag_T} is same with or without
the absolute values.}
%
values. The results are shown in figure \ref{fig:compobs_diff_Corlag_T}.
%
We note that detrending decreases the autolag correlation significantly (by 0.2)
in parts of tropical and desert climates in all four datasets. This increase is
especially pronounced in the \hadgem.
%
That said, 1-month autolag correlations of surface temperature in summer are
still pronounced (greater than 0.4) even after detrending surface temperature
anomalies in all four datasets as well as in the observations (see figure
\ref{fig:compobs_Corlag_T_detrend}).
%
In this regard, soil moisture memory is often cited as the main contributer to
surface temperature memory on sub-seasonal time scales \citep{sene06_memory}.
For example, in moisture-limited evapotranspiration regimes, intra-seasonal soil
moisture storage partly controls surface temperature fluctuations through its
control on evapotranspiration \citep{koster09_droughts}. 
%
Alternatively, temperature memory could be related to memory in the forcing
($F$) due to anthropogenic effects (more in appendix \ref{app:detrend}).

With regards to our hypothesis linking errors in surface temperature variability
to errors in a misrepresentation of soil moisture in GCMs, notice that both the
\ccsm and the \hadgem overestimate surface temperature memory in the central US
(compared to observations). Recall that both GCMs also feature a large
overestimation of the surface temperature variability in this region (see figure
\ref{fig:comp_Var_T_bias}).
%
So, the results of figure \ref{fig:compobs_Corlag_T} suggest a possible link
between surface temperature memory and variability errors in which the
erroneously large memory would be a source of temperature variability.
%
Tropical climates are characterized by pronounced temperature memory in
observations and consistently across the four datasets --- even after detrending
with respect to a linear fit.
%
Could this behavior also be linked to the soil moisture memory?  A more thorough
analysis of the sources of surface temperature memory and the impacts of memory
in the land-atmosphere system is given in section \ref{sec:tm_limits}.  
%
For now, the autocorrelation properties of surface temperature on monthly time
scales give us insights on selecting the appropriate soil layer(s) for the toy
model.  The argument is presented next.

%\vfill
%\newpage

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/compobs_Corlag_T}

\caption[Surface temperature 1-month autolag correlation in summer.]%
%
{Surface temperature 1-month autolag correlation in summer (see equation
\ref{eq:auto_corr}).}

\label{fig:compobs_Corlag_T}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.85\linewidth]{chap3/compobs_Corlag_T_detrend}

\caption[Surface temperature 1-month autolag correlation in summer after
detrending surface temperature anomalies.]%
%
{Surface temperature 1-month autolag correlation in summer (see equation
\ref{eq:auto_corr}) after removing linear trends in the surface temperature
anomalies.}

\label{fig:compobs_Corlag_T_detrend}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.85\linewidth]{chap3/compobs_Corlag_T_detrend}

\caption[Difference between the absolute value of the 1-month autolag
correlation of (non-detrended) surface temperature and the absolute value of the
1-month autolag correlation of detrended surface temperature.]%
%
{Difference between the absolute value of the 1-month autolag correlation of
(non-detrended) surface temperature in summer and the absolute value of the
1-month autolag correlation of detrended surface temperature in summer (equation
\ref{eq:diff_Corlag_T}).}

\label{fig:compobs_diff_Corlag_T}
\end{center}
\end{figure}

%\newpage

\subsection{Which soil layer(s) to use for the toy model?}
\label{ssec:soil_layer}
%
% gcms_Cor_Em_Cor_Emb_Cor_Emf
% gcms_Corlab_mb_Corlag_mf 
% comp_Corlag_m
%
% -------------------------------------------------------------------------------

%The toy model intends to approximate land-surface processes using state
%variables and forcings only. Selecting the appropriate soil moisture variable is
%then critical in providing an accurate representation of the surface energy and
%soil moisture budgets. 

In the moisture-limited evapotranspiration regime, soil moisture is a dominant
control of $E$ fluctuations.
%
Hence, our toy model for must account for variations in the fluxes of water in
the layer that actively participates in the month-to-month variability in $E$.

Figure \ref{fig:gcms_Cor_Em_Cor_Emb_Cor_Emf} compares the summertime correlation
coefficients of evapotranspiration to surface-layer, bottom-layer and
full-column soil moisture in the two GCMs. The bottom-layer soil moisture
($m_b$) is defined as full-column soil moisture content minus the surface-layer
soil moisture content ($m_f{-}m$). As before, the surface layer refers to the
top 10\cm of soil measured from the land surface; its moisture content has
symbol $m$.  Unequivocally, $E$ features more pronounced correlations with
surface-layer soil moisture than with bottom-layer or full-column soil moisture,
consistently across both GCMs.  That is, a significant fraction of the
full-column soil moisture fluctuations does not covary with $E$ fluctuations.

Next, we investigate memory properties of the soil moisture.  Figure
\ref{fig:gcms_Corlag_mb_Corlag_mf} compares 1-month autolag correlations (see
equation \ref{eq:auto_corr}, non-detrended) of soil moisture in the bottom layer
and full-column in the \ccsm and \hadgem.
%
Figure \ref{fig:comp_Corlag_m} presents the 1-month autolag correlation
(equation \ref{eq:auto_corr}) of surface-layer soil moisture in the four
analyzed datasets.  Bottom moisture anomalies in a given summer month persist
well into the subsequent month and govern, to a large degree, the memory in
full-column soil moisture content.  We notice that the surface-layer moisture is
significantly more volatile than soil moisture in the bottom and full-column
layers.
%
Importantly, we note that surface-layer soil moisture and surface temperature
have comparable autolag correlations globally (see figure
\ref{fig:comp_Corlag_m} and \ref{fig:compobs_Corlag_T}).

As result of the surface-layer soil moisture's correlation properties with $E$
and its memory properties, we postulate that summertime surface temperature
fluctuations --- over monthly to decadal time scales --- can be modeled
adequately by coupling the surface energy budget to the \emph{surface-layer}
soil moisture budget.
%
Note that using the surface-layer soil moisture is also advantageous with
regards to the datasets that are in the CMIP3 (and CMIP5) databases. We develop
the toy model using an archived variable in all four datasets (except for a
slight correction in the \era, see equation \ref{eq:era40_m}).
%
%Comparing figures \ref{fig:compobs_Corlag_T} and \ref{fig:comp_Corlag_m},
%notice that regions of relatively strong top layer memory do not correspond to
%regions of relatively strong surface temperature memory. We then expect the toy
%model to underestimates surface temperature variability in regions of strong
%temperature memory.  % Nevertheless, we proceed by considering exclusively
%top-layer soil moisture in following analysis in developing the toy model. 
%
Hereafter, to lighten the text, the term \emph{soil moisture} without an
adjective designates the surface-layer soil moisture.

%\vfill
\newpage

%% More stuff about top-layer only implications
%That is, we assume that surface-layer (top 10\cm from the land-surface) moisture
%content 
%is an archived output for three of the four datasets and
%retrieved for the other.
%%% Say something , or refer to, output soil layers.
% Why only 1 layer ...
% As seen, in 2.2, the two GCMs used in this study output, without interpolating
% ... or say something about linear combinaison 

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/gcms_Cor_Em_Cor_Emb_Cor_Emf}

\caption[Summertime correlation coefficients of evapotranspiration and \newline
surface-layer, bottom-layer and full-column soil moisture.]%
%
{Summertime correlation coefficients of evapotranspiration and (top panels)
surface-layer soil moisture, (middle panels) bottom-layer and (bottom panels)
full-column soil moisture. [\ccsm data (left panels) and \hadgem data (right
panels)].}

\label{fig:gcms_Cor_Em_Cor_Emb_Cor_Emf}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/gcms_Corlag_mb_Corlag_mf}

\caption[1-month autolag correlation of summer soil moisture in the bottom soil
layer and full soil column.]%
%
{1-month autolag correlation of summer soil moisture in the (left panels) bottom
soil layer and the (right panels) full soil column.  [\ccsm data (top panels)
and \hadgem data (bottom panels)].}

\label{fig:gcms_Corlag_mb_Corlag_mf}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_Corlag_m}

\caption{1-month autolag correlation of summertime surface-layer soil moisture.}

\label{fig:comp_Corlag_m}
\end{center}
\end{figure}

%\newpage
