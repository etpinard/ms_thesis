%
% Chapter 4
%
% -------------------------------------------------------------------------------
\section{Two regimes of summertime surface temperature variability}
\label{sec:two_regimes}
%
%
%
% -------------------------------------------------------------------------------

\subsection{Relative contribution of forcing to the state variables' variance}
\label{ssec:rel_contr}
%
% comp_fact_F0 
% comp_fact_P 
% ccsm3_cor_TP_sign_check
% comp_scale_F0_P
% comp_scale_F0_P_m 
%
% -------------------------------------------------------------------------------

The toy model expresses surface temperature and soil moisture anomalies as
linear combinations non-precipitating radiation forcing and precipitation.
% 
Here, we analyze the relative contributions of the orthogonal forcing functions
($F_0'$ and $P'$) to the variance in the state variables.
%
Using equation \ref{eq:tm_T_factors}, the toy model expression for surface
temperature anomalies can be written more compactly as:

\begin{equation}
  T'\;=\; 
  \frac{1}{\;\gamma\;}\, \big[\pt c_{F_0}F_0' \,-\, L\pt c_PP' \pt\big] 
  \enskip,
  \label{eq:tm_T_w_factors}
\end{equation}

\noindent%
%
where

\begin{equation}
 c_{F_0} \;\equiv\; 1 - \lambda (1 - \chi)
 \quad,\quad
 c_{P} \;\equiv\; \alpha (1 - \lambda) + \chi (1 + \alpha\lambda - \beta) 
 \enskip.
 \label{eq:tm_T_factors}
\end{equation}

\noindent%
%
Both $c_{F_0}$ and $c_P$ are unitless.
%
Figure \ref{fig:comp_fact_F0} presents the summer average $c_{F_0}$ in the four
datasets. The factor $c_{F_0}$ is near unity in dryland climates consistently
across the four datasets. That is, non-precipitating radiation forcing is
unattenuated by land-surface processes in dryland climates as a result of a
small evapotranspiration efficiency $\lambda$ (see figure \ref{fig:tm_coeffs1}).
%
Elsewhere, the land-surface damps the effects of $F_0'$ on $T'$ through
evapotranspiration.  In the previous section \ref{sec:tm_mean_state}, we remark
that the coupling coefficient $\chi$ is vanishingly small in the tropical
climate zone due large soil moisture seeping rates (see figure
\ref{fig:tm_coeffs2}); hence, $F_0'$ is modulated by evapotranspiration
efficiently only.
%
Recall that $\chi$ is negative in the high-latitude and mid-latitude wet
climates: energy-limited locations (see section \ref{sec:param}). 
%
Here, $F_0'$ generates soil moisture anomalies that, in turn, cause sensible
heat flux anomalies that dampen the temperature response to $F_0'$.
%
That said, the sensible heat flux response is quantified by the product of two
less-than-one coefficients (figure \ref{fig:tm_coeffs1} and
\ref{fig:tm_coeffs2}); hence, it is a small term in $c_{F_0}$.

The factor $c_P$ is presented in figure \ref{fig:comp_fact_P}. Largest values of
$c_P$ are found in dryland climates. Wet soil locations in the \era and the
\hadgem feature $c_P$ values near zero; precipitation anomalies do not
contribute to surface temperature variability.
%
In tropical climates, where $\chi$ is small, the precipitating component of the
radiation forcing ($\alpha$) is negated by the large evapotranspiration
efficiency ($\lambda$). That is, radiation anomalies associated with
precipitating clouds are almost-entirely converted into evapotranspiration
anomalies of the opposite sign, and thus $c_p \approx \alpha(1-\lambda) \approx
0$.
%
In high-latitude and mid-latitude wet regions, the sensible heat flux response
(through $\chi$) also contributes to off-setting the radiative effects of
precipitation on surface temperature.

The \ccsm features negative $c_P$ in Central Africa and in much of Central South
America. In these places, the toy model describes a positive correlation between
precipitation and surface temperature. 
%
Is this a proper description of the \ccsm outputs? Figure
\ref{fig:ccsm3_cor_TP_sign_check} presents the product of the (summer average)
$T$-$P$ correlation using the archived $T$ and the toy model $T$.  In figure
\ref{fig:ccsm3_cor_TP_sign_check} positive (negative) values imply that the toy
model properly (improperly) represents the sign the of $T$-$P$ correlation.
%
We remark that most regions of negative $c_P$ are associated with toy model
$T$-$P$ correlations of the wrong sign. 
%
Correspondingly, referring back to figure \ref{fig:comp_tm_Var_T_bias}, regions
of improper $T$-$P$ correlation sign in the \ccsm have relatively large
calibration errors.

Figure \ref{fig:comp_fact_P} demonstrates why the toy model fit to \ncep data
underestimates surface temperature variability even when forced by the
erroneously large precipitation variance of the \ncep's archives (section
\ref{ssec:P_and_F}).
%
The factor $c_P$ is near zero everywhere; precipitation anomalies only
marginally contribute to temperature variability in the \ncep.
%
Toy model calibration errors are largest in the \ncep dataset, especially in the
drylands where the toy model underestimates the \ncep's temperature variance by
a factor of two or more (see figure \ref{fig:comp_tm_Var_T_bias} in section
\ref{sec:tm_perf}).
%
Compared to the other three datasets, the \ncep, characterized by small $c_P$,
is missing an important component of variability, which explains the calibration
error pattern shown in section \ref{sec:tm_perf}.
%
From the results of section \ref{ssec:F_decomp}, we note that the small
magnitude of $c_P$ can be partly linked to the decomposition coefficient
$\alpha$.

Next, we assess whether the $F_0'$ or $P'$ component contributes more to the
temperature variance at a given location. Figure \ref{fig:comp_scale_F0_P}
presents the ratio of the non-precipitating radiation forcing component,
$c_{F_0}^2\Var(F_0)$, to the precipitation component, $L^2 c_P^2\Var(P)$, (see
definitions in equations \ref{eq:tm_T_factors} and \ref{eq:tm_T_w_factors}).
%
The non-precipitating radiation forcing component dominates for most of the
globe. A typical high-latitude or tropical location has an $F_0'$ component more
than ten times greater than its $P'$ component.
%
Overall, the $P'$ component of temperature variance is more important in dryland
locations. In some cases, the $P'$ component is more than four times larger than
the $F_0'$ component --- for example in the Kalahari desert and central
Australia. 
%
Moreover, we notice that there are more areas where the $P'$ component dominates
in the GCMs than in the reanalysis products. These patterns can be traced back
to the GCMs' soil moisture deficit through the coupling coefficient $\chi$ (part
of $c_P$, see equation \ref{eq:tm_T_factors}) which displays a strong dependence
on mean-state soil moisture.

Finally, we investigate the relative contribution of $F_0'$ and $P'$ in the toy
model expression for the soil moisture variance (equation \ref{eq:tm_Var_m}).
%
Analogous to the previous figure, figure \ref{fig:comp_scale_F0_P_m} presents
the ratio of the $F_0'$ component, $(\lambda/L)^2\Var(F_0)$, to the $P'$
component, $(1+\alpha\lambda-\beta)^2\Var(P)$.
%
The $P'$ component dominates the $F_0'$ component in the soil moisture
expression for $\Var(m)$ everywhere except in some high-latitude locations and
in the northern Sahara in the reanalysis products.
%
Across the four datasets, the $F_0'$ component contributes 10\% or less to the
toy model $\Var(m)$ expression (with the exception of Spain and wet soil regions
in Europe in the \era dataset).
%
That is, the toy model describes soil moisture variability as being governed
almost entirely by precipitation.

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_fact_F0}

\caption[Summertime modulating factor for non-precipitating radiation forcing
anomalies in the toy model expression for temperature anomalies.]%
%
{Summertime $c_{F_0}$, the factor modulating the contribution of $F_0'$ in the
toy model expression for $T'$, see equation \ref{eq:tm_T_factors}.}

\label{fig:comp_fact_F0}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_fact_P}

\caption[Summertime modulating factor for precipitating anomalies in the toy
model expression for temperature anomalies.]%
%
{Summertime $c_{P}$, the factor modulating the contribution of $P'$ in the toy
model expression for $T'$, see equation \ref{eq:tm_T_factors}.}

\label{fig:comp_fact_P}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.7\linewidth]{chap4/ccsm3_cor_TP_sign_check}

\caption[\ccsm surface temperature-precipitation correlation check.]%
%
{Product of the summertime \ccsm $T$ / \ccsm $P$ correlation coefficient and
summertime toy model $T$ / \ccsm $P$ correlation coefficient.}

\label{fig:ccsm3_cor_TP_sign_check}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_scale_F0_P}

\caption[Summertime ratio of toy model surface temperature variance
components.]%
%
{Summertime ratio of toy model surface temperature variance components:
$c_{F_0}^2\Var(F_0) \big/ L^2 c_P^2\Var(P)$.}

\label{fig:comp_scale_F0_P}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_scale_F0_P_m}

\caption[Summertime ratio of toy model soil moisture variance components.]%
%
{Summertime ratio of toy model soil moisture variance components:
$(\lambda/L)^2\Var(F_0) \big/ (1+\alpha\lambda-\beta)^2\Var(P)$. Refer to
equation \ref{eq:tm_Var_m}.}

\label{fig:comp_scale_F0_P_m}
\end{center}
\end{figure}

\subsection{Land-surface amplified and land-surface damped}
\label{ssec:amplified_damped}
%
% comp_scale_gammaT_OvGM
% hadgem1_scale_engy_OvGM
%
% -------------------------------------------------------------------------------

In developing the toy model, we derived an expression for the surface
temperature variance by parameterizing six land-surface processes contributing
to the surface energy and soil moisture budgets. 
%
In section \ref{sec:tm_mean_state}, we found that some land-surface processes
behave differently over dry soils (\ie in moisture-limited locations) and wet
soils (\ie energy-limited).
%
Moreover, in section \ref{ssec:rel_contr}, we found that the net land-surface
response to given forcing anomalies (\ie $c_{F_0} F_0'$ and $L c_P P'$) are
different over dry and wet soils.
%
Here, we investigate whether the surface temperature response (yet to be
defined) to given forcing anomalies differs across the dry/wet soils boundary. 
%
To do so, we substitute the toy model parameterizations involving surface
temperature anomalies $T'$ (see section \ref{ssec:param_summ}) into the toy
model equation for surface energy (equation \ref{eq:tm_bud_engy}) to obtain:

\begin{equation}
  F' \;=\; \gamma T' \;+\; LE \;+\; \Hso
  \enskip,
  \label{eq:T_regimes}
\end{equation}

\noindent%
%
where $F'$ is the radiation forcing and $\gamma$ is the surface temperature
resistance ($\equiv\gammalw{+}\gammaHs{+}\gammaG$ with units of $\Wm[-2]\K[-1]$)
as before.
%
The symbol $\Hso$ refers to sensible heat flux projection residual after a
(first) projection onto $T'$.  Note that $\Hso$ is not the toy model
parameterization residual: the toy model algorithm makes use of sequential
projections onto $T'$ and $m'$.
%
For simplicity, we drop the upwelling longwave radiation ($\Fluuo$) and the
ground heat flux ($G'_0$) parameterization residuals, which have been shown to
be small in magnitude in sections \ref{ssec:param_Flu} and \ref{ssec:bud_res}
respectively.
%
Next, we square both sides of \eqref{eq:T_regimes} and average across the sample
space to form:

\begin{align}
  \Var(F) 
  \;&=\;
  \gamma^2 \pt \Var(T) \;+\; L^2 \pt \Var(E) \;+\; \Var(\HsO) \nonumber \\
  &\; \quad+\; 2L\gamma \pt \Cov(T,E) \;+\; 2L \pt \Cov(E,\HsO) 
  \enskip.
  \label{eq:Var_T_regimes}
\end{align}

\noindent%
%
In equations \ref{eq:T_regimes} and \ref{eq:Var_T_regimes}, the forcing appears
on the left-hand side (LHS) and the land-surface response on the right-hand side
(RHS).
%
That is, equation \ref{eq:Var_T_regimes} expresses the sample-mean response to
variability in radiation forcing in terms of land-surface processes ($\gamma$,
$E$ and $\Hso$) and one state variable ($T$).
%
Note that the LHS includes both components of $F'$: $\Var(F) =
\Var(F_0){+}L^2\alpha^2\Var(P)$.
%
Equation \ref{eq:Var_T_regimes} is accurate up to\footnote{%
%
Except along coastlines in the \ccsm and \hadgem. See figure
\ref{fig:comp_scale_xiU_F}.}
%
10\% of $\Var(F)$ where errors arise from inexact parameterizations of $\Fluu$
and $G'$.
% 
Additionally, note that by definition $\Cov(T,\HsO){=}0$ (see section
\ref{ssec:Proj}).

Dividing equation \ref{eq:Var_T_regimes} by $\Var(F)$ gives a scaling for the
surface temperature response given a typical radiation forcing anomaly,
$\gamma^2\Var(T)$ to $\Var(F)$. This quantity is referred to as the
\emph{surface temperature response} or temperature response hereafter. Figure
\ref{fig:comp_scale_gammaT_OvGM} presents the summer average (surface)
temperature response.
%
Interestingly, we observe that there exists locations where the surface
temperature response is greater than the forcing variability and vice-versa.
That is, in some locations land-surface processes \emph{amplify} the temperature
variability inputted into the land-surface system by $F'$. Conversely, some
locations are characterized by land-surface process that \emph{damp} $F'$.
%
More compactly, we define two\footnote{%
%
With a complementary \emph{uncoupled} regime where $\gamma^2\Var(T)=\Var(F)$.
\ie land-surface processes have no (net) effect on surface temperature
variability.}
%
regimes of surface temperature variability regimes: the \emph{land-surface
amplified regime} where $\gamma^2\Var(T) > \Var(F)$ and the \emph{land-surface
damped regime} where $\gamma^2\Var(T) < \Var(F)$.

As in our analysis of land-surface correlation patterns in section
\ref{ssec:data_corr}, we superpose the global soil moisture contour (see table
\ref{tab:m_GM}) on figure \ref{fig:comp_scale_gammaT_OvGM} and
\ref{fig:hadgem1_scale_engy_OvGM}.
%
Consistently across the four datasets, dry soils are associated with the
land-surface amplified regime and wet soils are associated land-surface damped
regime.
%
Equivalently, the moisture-limited evapotranspiration regime translates to the
land-surface amplified temperature variability regime and the energy-limited $E$
regime translates to a land-surface damped temperature variability regime. 

We proceed by normalizing each of the five terms on the RHS of
\eqref{eq:Var_T_regimes} by radiation forcing variance $\Var(F)$. The results
are displayed in figure \ref{fig:hadgem1_scale_engy_OvGM} where \hadgem data is
used. 
%
Conclusions presented henceforth in the section are not dataset dependent.
Figures generated with \ccsm, \era and \ncep data can be found in \FigFolder.
Take note that the top panel of figure \ref{fig:hadgem1_scale_engy_OvGM} is the
same the as bottom-left panel of figure \ref{fig:comp_scale_gammaT_OvGM}.
%
We notice that all terms in the equation \ref{eq:Var_T_regimes} show a strong
change of behavior at the dry/wet boundary. 
%
Most notably, latent heat flux variability, $L^2\Var(E)$, is as larger or
greater than the radiation forcing input in dry soils regions. Typical $E$
anomaly magnitudes are more pronounced in the moisture-limited $E$ regime (\ie
land-surface controlled) than in the energy-limited $E$ regime.
% 
Moreover, the $2L\gamma\Cov(T,E)$ term demonstrates that surface temperature
fluctuations are strongly affected by evapotranspiration over dry soils and less
so over wet soils.
%
We note that, overall, the land-surface response terms on the RHS of equation
\ref{eq:Var_T_regimes} are large in magnitude over dry soils and small in
magnitude over wet soils. For reference, typical magnitudes are listed in table
\ref{tab:ampl_damp}.

Using the toy model formulation we found that the dry/wet soil boundary
separates regimes of temperature variability in an analogous manner, as
$m\subt{crit}$ delineates evapotranspiration regimes in the Koster diagram.
%
In essence, the toy model extends Koster's framework.  The Koster diagram links
$E$ to soil moisture and energy inputs at land-surface and
%
although the Koster diagram does not explicitly link forcing variance to
temperature variance, the latter can be roughly deduced if one assumes
evapotranspiration anomalies scale by $(1{-}\lambda)F'$ and are the dominant
land-surface process. 
%
The toy model incorporates six land-surface processes, including $E$, in an
effort to quantify surface temperature variability.
%
Note that our results disagree with Koster's framework in desert climates: these
regions do not feature the largest values of temperature variability (quantified
by $\gamma^2\Var(T)/\Var(F)$, see figure \ref{fig:comp_scale_gammaT_OvGM}) as
one might expect in regions lacking an evapotranspiration buffer on the
radiation forcing.
%
Instead, the toy model presents surface temperature variability (given a
forcing) as dependent on the variability in the processes that flux energy and
moisture at the land-surface interface.
%
We then ask: why are surface temperatures more variable in moisture-limited
regions than in energy-limited regions? Next, we investigate the potential role
of soil moisture in controlling surface temperature variability.

\newpage

%% figures and tables
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_scale_gammaT_OvGM}

\caption[Summertime surface temperature response.]%
%
{Summertime surface temperature response $\gamma^2\Var(T)\big/\Var(F)$ (colored
contours). Superposed is the line where summertime soil moisture is equal to the
global mean soil moisture (solid red line).}

\label{fig:comp_scale_gammaT_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/hadgem1_scale_engy_OvGM}

\caption[Summertime scalings of the anomaly surface energy budget in fractions
of the radiation forcing variance.]%
%
{Scaling of each term on the RHS of equation \ref{eq:Var_T_regimes} (\pt
$\gamma^2\Var(T)$, $L^2\Var(E)$, $\Var(\HsO)$, $2L\gamma\Cov(T,E)$ and
$2L\Cov(E,\HsO)$ \pt) by $\Var(F)$ (colored). Superposed is the line
where soil moisture is equal to the  global mean soil moisture (solid red
line). [\hadgem data].}

\label{fig:hadgem1_scale_engy_OvGM}
\end{center}
\end{figure}

\begin{table}[H]
\begin{center}

\begin{tabular}[t]{c||c|c}
	Fraction of \pt$\Var(F)$ 
  & Land-surface amplified & Land-surface damped 
  \\[1ex] \hline
  $\gamma^2\Var(T)$   &  $>1$      & $<1$            \\[1ex] 
    \hline\hline
  $L^2\Var(E)$        & $>2$       & $[0,0.5]$       \\
  $\Var(\HsO)$        & $\simeq 1$ & $[0,0.25]$      \\
  $2L\gamma\Cov(T,E)$ & $<-2$      & $[-0.25,0.25]$  \\
  $2L\Cov(E,\HsO)$    & $<-2$      & $[-0.5,-0.25]$  \\

\end{tabular}

\end{center}

\caption[Complementary table for figure \ref{fig:hadgem1_scale_engy_OvGM}.]%
%
{Typical values for the land-surface process response with respect to the
radiation forcing variance $\Var(F)$. The two topmost rows are definitions of
the two regimes of surface temperature variability. Values in this table are
approximative and represent typical locations in each of the two regimes.}

\label{tab:ampl_damp}
\end{table}

\subsection{Role of soil moisture}
\label{ssec:role_of_m}
%
% comp_tm_T_no_m_OvGM         (show it, just to quantify what we know already)
% - map of m'=0 version to Var(T) , including residuals 
%
% hist_dist_T_no_m_full       (dropped)
% - T' frequency distributions T' with m'=0 vs dataset T'
%
% comp_rel_diff_scale_gammaT_OvGM
% - map of relative error gamma^2 Var(T) / Var(F) with and without m'
%
% compsc_Lchisig_P_scale_gammaT_Cor_Em
% - scatter plot of Corr(E.m) in (gamma^2Var(T)/Var(F)-L*chi*sig_P) space
% compsc_mbar_scale_gammaT_Cor_Em
% - scatter plot of Corr(E.m) in (gamma^2Var(T)/Var(F)-mbar) space
% compsc_lambda_scale_gammaT_Cor_Em
% - scatter plot of Corr(E.m) in (gamma^2Var(T)/Var(F)-lambda) space
%
% comp_scale_gammaT_tilde_OvGM
% - map of gamma_tilde^2 Var(T) / Var(F)
%
% tab:regimes   , summary table of our associations
%
% -------------------------------------------------------------------------------

In the toy model, the coupling coefficient ($\chi$) modulates the effects of
soil moisture on surface temperature.  Its sign determines whether soil moisture
anomalies are positively or negatively correlated with surface temperature. In
section \ref{sec:tm_expr}, we remarked that soil moisture anomalies amplify
(damp) both the $F_0'$ and $P'$ components of the radiation forcing if $\chi$ is
positive (negative).
%
To assess the role of $m'$ on $T'$, we modify the toy model expression for $T'$
(equation \ref{eq:tm_T}). Consider the exact surface energy budget and set
$\chi{=}0$:

\begin{equation}
 T'_{m'=0}
 \;=\; \frac{1}{\;\gamma\;}\, 
 \Big[ \, 
 ( 1 - \lambda )\, F_0' 
 \;-\; L\, \alpha (1 - \lambda) \, P'
 \;-\; LE'_{00} - \Hsoo - \Fluuo - G_0'
 \, \Big]
 \enskip,
 \label{eq:tm_T_no_m}
\end{equation}

\noindent%
%
where we have included all toy model parameterization residuals of surface
energy budget: $E_{00}'$, $\Hsoo$, $\Fluuo$ and $G'_0$.
% 
With the above expression, we isolate the effects of soil moisture variability
on $T'$: $T'_{m'=0}$ differs from the dataset $T'$ only because of the impact of
$m'$ on the energy fluxes is set to zero.
% 
The corresponding expression for the temperature variance $\Var(T_{m'=0})$
isolates the effects of $\Var(m)$ on $\Var(T)$.
%
Figure \ref{fig:comp_tm_T_no_m_OvGM} presents the calibration ratio
$\Var(T'_{m'=0})\big/\Var(T\subt{data})$, where $T\subt{data}$ is the
temperature from each dataset.
%
Calibration ratios less (greater) than one indicate that soil moisture has an
amplifying (damping) effect of surface temperature anomalies.
%
As in our analysis of the coupling coefficient, figure
\ref{fig:comp_tm_T_no_m_OvGM} shows that the dry/wet soil boundary delineates
regions of amplifying and damping effects of soil moisture variability on
$\Var(T)$.
%
The effects of $\Var(m)$ on $\Var(T)$ are quantified in figure
\ref{fig:comp_tm_T_no_m_OvGM}: in both dry and wet soil locations, $\Var(m)$ is
responsible for 25-50\% of $\Var(T)$.

%\comment{I also made $T'$ frequency distributions for the dry and wet box as in
%the toy model evaluation (figure \ref{fig:hist_tm_T_dist_full}). Not much
%additional information came out of it though.}
%%% --- text for dropped figure ---
%Figure \ref{fig:hist_tm_T_dist_no_m_full} compares temperature anomalies
%frequency distributions of the datasets' outputs and of equation
%\ref{eq:tm_T_no_m} in two box regions (one dry box and one wet box as in figure
%\ref{fig:hist_tm_T_dist_full}).
%%
%Figure \ref{fig:hist_tm_T_dist_no_m_full} suggests that the effects of soil
%moisture variability are more pronounced over dry soils than over wet soils,
%especially in the two GCMs.
%%
%Additionally we remark that $m'$ makes the distributions skewed about the mean
%in the \ccsm in the dry box (central US); soil moisture makes extreme warm
%temperature anomalies more frequent in this dataset.

%In brief, $\Var(m)$ amplifies $\Var(T)$ in the land-surface amplified regime
%and damps $\Var(T)$ in the land-surface damped regime; hence, soil moisture
%variability is party responsible for the surface temperature variability
%patterns observed earlier in figure \ref{fig:comp_scale_gammaT_OvGM}.
%
Next, we investigate to what degree can the $\gamma^2\Var(T)\big/\Var(F)$
patterns be attributed to soil moisture variability. Using equation
\ref{eq:tm_T_no_m}, we compute $\gamma^2\Var(T_{m'=0})\big/\Var(F)$, the
temperature response in the absence of soil moisture.
%
Figure \ref{fig:comp_rel_diff_scale_gammaT_OvGM} shows the relative difference
between $\gamma^2\Var(T)$ and $\gamma^2\Var(T_{m'=0})$, both normalized by
$\gamma^2\Var(T)$. Positive (negative) values imply that soil moisture
variability amplifies (damps) the temperature response.
%
In the land-surface amplified regime, 40\% to 60\% of $\gamma^2\Var(T)$ can be
attributed to $\Var(m)$. Overall, the relative contributions of $\Var(m)$ to
$\Var(T)$ in the GCMs are greater than in the reanalysis products, suggesting a
strong $\chi$ control on the temperature response (see figure
\ref{fig:compsc_lambda_chi}).
% 
In the land-surface damped regime, relative contributions of $\Var(m)$ to the
temperature response range from 20\% to almost 100\% in some tropical regions.
%
Therefore, we conclude that soil moisture introduces additional temperature
variance in the moisture-limited regime via $E$.  
%
In contrast, soil moisture acts as a damping on $F'$ where $E$ in
energy-limited.
%

We next ask: what processes control the temperature response within each
temperature variability regime?
%
Figure \ref{fig:compsc_Lchisig_P_scale_gammaT_Cor_Em} present a scatter plot of
$L\chi\Var(P)^\half$ against $\gamma^2\Var(T)\big/\Var(F)$.
%
The $x$-axis variable $L\chi\Var(P)^\half$ represents the typical net energy
flux (in $\Wm[-2]$) in the presence of a precipitation anomaly, a component of
the forcing variability $\Var(F)$.
%
The scatter is color-coded by the $E$-$m$ correlation coefficient. Recall that
the sign of the $E$-$m$ correlations delimits regions of the dry and wet toy
model $E$ parameterization algorithms. It is also a proxy for moisture available
for $E$.
%
We first notice that almost every point of positive $\corr(E,m)$ lies above the
$\gamma^2\Var(T)\big/\Var(F)=1$ line, confirming the result shown earlier in
figure \ref{fig:comp_scale_gammaT_OvGM}.
%
Within the amplified regime, $L\chi\Var(P)^{\half}$ is well-correlated with the
temperature response across the four datasets and especially in the \era and
\hadgem.
%
That is, a larger temperature response is expected in regions of larger
precipitation variability and strong amplifying effects of $m'$ on $T'$. 
%
Note the plotting the temperature response against $L\chi\Var(P)^\half$, as in
figure \ref{fig:compsc_Lchisig_P_scale_gammaT_Cor_Em}, yields better results
than using $\chi$ and $\Var(P)^\half$ separately. These figures are available in
\FigFolder.

We next investigate the role of (surface-layer) mean-state soil moisture on the
temperature response. 
%
In figure \ref{fig:compsc_mbar_scale_gammaT_Cor_Em} the $x$-axis is $\mbar$ but
otherwise the same as figure \ref{fig:compsc_Lchisig_P_scale_gammaT_Cor_Em}.
%
Here, we can associate small temperature responses with large mean-state soil
moisture. 
%
%However, note that the results are not unanimous. But, the $\mbar$
%dependence is more conclusive within the damped regime than in the amplify
%regime especially in the \era dataset.
%
Previously, we remarked that the evapotranspiration efficiency ($\lambda$) shows
a strong dependence on $\mbar$. 
%
Correspondingly, substituting $\lambda$ for $\mbar$ in figure
\ref{fig:compsc_lambda_scale_gammaT_Cor_Em} shows that $\lambda$ is negatively
correlated with the temperature response in the damped regime. 

In summary, to asses temperature variability in a given location, one must first
identify the temperature variability regime. The land-surface amplified regime
is associated with positive $E$-$m$ correlations or mean-state soil moisture
content below the global mean (\ie below $m\subt{crit}$). Regions in the
land-surface damped regime have negative $E$-$m$ correlations and
above-$m\subt{crit}$ mean-state soil moisture.
%
Within the amplified regime, the magnitude of the temperature response is
positively correlated with the variable $L\chi\Var(P)^\half$ representing the
typical net energy flux in the presence of a precipitation anomaly.
%
Within the damped regime, the magnitude of the temperature response is
negatively correlated with the evapotranspiration efficiency $\lambda$.
%
That is, soil moisture variability controls the magnitude of the temperature
response in the amplified regime wheres mean-state soil moisture controls
$\gamma^2\Var(T)\big/\Var(F)$ in the damped regime.
%
These finding are summarize in table \ref{tab:regimes}.

We close this section with a discussion of an important caveat regarding our
analysis of the surface temperature response.
%
In section \ref{ssec:coeffs_alt}, we noted that the toy model formulation
describes an upper bound on the effect of soil moisture on temperature
variability: parameterizing sensible heat flux anomalies with $T'$ then $m'$
underestimates the soil moisture control on $H_s'$ and inflates the magnitude of
$\chi$ in regions where $\corr(E,m)$ (\ie dry soils).
%
In turn, the surface temperature resistance ($\gamma$) contains information
about soil moisture when $\corr(T',m'){\neq}0$.  
%
Figure \ref{fig:comp_scale_gammaT_tilde_OvGM} presents the temperature response
using the \emph{alternative} surface temperature resistance (\ie
$\gammaalt^2\Var(T)\big/\Var(F)$).
%
Compared to $\gamma^2\Var(T)\big/\Var(F)$ in figure
\ref{fig:comp_scale_gammaT_OvGM}, the temperature response is more uniform: the
effects of the land-surface are smaller in magnitude than with $\gamma$ (recall
that $\gammaalt$ is almost constant globally, see figure
\ref{fig:tm_coeffs_alt}).
%
In other words, $\gamma^2\Var(T)\big/\Var(F)$ should be interpreted as the upper
bound of the temperature response and $\gammaalt^2\Var(T)\big/\Var(F)$ as its
lower bound.
% 
In addition, these results imply that the effects of soil moisture on the
temperature response are underestimated in figure
\ref{fig:comp_rel_diff_scale_gammaT_OvGM}
%
since the spatial structure in the surface temperature resistance $\gamma$ also
contributes to the $\gamma^2\Var(T)\big/\Var(F)$ patterns observed in section
\ref{ssec:amplified_damped}.
%
Regardless, even in the alternative formulation shown in figure
\ref{fig:comp_scale_gammaT_OvGM}, there exists well-defined regions where the
land surface amplifies temperature variability and where the land surface damps
temperature variability.

\newpage

%% figures and tables
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_tm_T_no_m_OvGM}

\caption[Summertime surface calibration ratio of the temperature variance
without soil moisture variability.]%
%
{Summertime surface calibration ratio of the temperature variance without soil
moisture variability $\Var(T'_{m'=0})\big/\Var(T\subt{data})$ (see equation
\ref{eq:tm_T_no_m}, colored contours). Superposed is the line where soil
moisture is equal to global mean soil moisture (solid black line).}

\label{fig:comp_tm_T_no_m_OvGM}
\end{center}
\end{figure}

%\begin{figure}[H]
%\begin{center}
%
%\includegraphics[width=\linewidth]{chap4/hist_tm_T_dist_no_m_full}
%
%\caption{Monthly temperature anomalies frequency distribution over box regions
%(see figure \ref{fig:hist_tm_T_dist_full} for their locations). 
%
%Bins size: 0.1 K, grid points used (\ccsm: 224,345 , \hadgem: 195,315 , \era:
%80,119 , \ncep: 138,208 ).}
%
%\label{fig:hist_tm_T_dist_no_m_full}
%\end{center}
%\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_rel_diff_scale_gammaT_OvGM}

\caption[Summertime relative difference between surface temperature response
with and \newline without soil moisture variability.]%
%
{Summertime relative difference $\gamma^2\Var(T)$ minus $\gamma^2\Var(T_{m'=0})$
normalized by $\gamma^2\Var(T)$ (colored contours).  Superposed is the line
where soil moisture is equal to the global mean soil moisture (solid green
line).}

\label{fig:comp_rel_diff_scale_gammaT_OvGM}
\end{center}
\end{figure}

\pagebreak
\vspace*{3cm}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/compsc_Lchisig_P_scale_gammaT_Cor_Em}

\caption[Summertime surface temperature response as a function of the net energy
flux in the presence of a precipitation anomaly ]%
%
{Summertime $E$-$m$ correlation coefficient in
$\gamma^2\Var(T)\big/\Var(F)$-$L\chi\Var(P)^\half$ space. [1 in 4 grid points
are plotted].}

\label{fig:compsc_Lchisig_P_scale_gammaT_Cor_Em}
\end{center}
\end{figure}

\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.83\linewidth]{chap4/compsc_mbar_scale_gammaT_Cor_Em}

\caption[Summertime surface temperature response as a function of summertime
mean soil moisture.]%
%
{Summertime $E$-$m$ correlation coefficient in
$\gamma^2\Var(T)\big/\Var(F)$-$\mbar$ space. [1 in 4 grid points are plotted].}

\label{fig:compsc_mbar_scale_gammaT_Cor_Em}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.83\linewidth]{chap4/compsc_lambda_scale_gammaT_Cor_Em}

\caption[Summertime surface temperature response as a function of the summertime
evapotranspiration efficiency.]%
%
{Summertime $E$-$m$ correlation coefficient in
$\gamma^2\Var(T)\big/\Var(F)$-$\lambda$ space. [1 in 4 grid points are
plotted].}

\label{fig:compsc_lambda_scale_gammaT_Cor_Em}
\end{center}
\end{figure}

\begin{table}[H]
\begin{center}

\def\arraystretch{1.5}

\begin{tabular}[t]{|c||c|c|}
% 
  \hline
  %
  & Land-surface amplified & Land-surface damped \\[1ex] \hline
  %
  Definition%
  & $\displaystyle \frac{\gamma^2\Var(T)}{\Var(F)} >1$  
  & $\displaystyle \frac{\gamma^2\Var(T)}{\Var(F)} <1$  \\[1ex] \hline\hline
  %
  is associated with%
  & $\sgn(\mbar-m\subt{crit}) < 0$ 
  & $\sgn(\mbar-m\subt{crit}) > 0$ \\[-2.5ex]
  %
  & $\sgn(\corr(E,m)) > 0$ 
  & $\sgn(\corr(E,m)) < 0$ \\[-2.5ex]
  %
  & $\sgn(\corr(E,F)) < 0$ 
  & $\sgn(\corr(E,F)) > 0$ \\[-2.5ex]
  %
  & $\sgn(\chi) > 0$
  & $\sgn(\chi) < 0$ \\[1ex] \hline
  %
  magnitude is governed by%
  & $\chi\;$ and $\;\Var(P)$ 
  & $\lambda\;$ or $\;\mbar$ \\[1ex] \hline
%
\end{tabular}

\end{center}

\caption[Surface temperature variability regime summary table.]
%
{Surface temperature variability regime summary table. The first row has the
definition of both regimes. Associations encountered in this study are listed in
the second row. The final row list the variables governing the magnitude of the
surface temperature response $\,\gamma^2\Var(T)\big/\Var(F)$.
%
Recall that, for the four dataset analyzed, $m\subt{crit}$ was found to equal
the datasets' respective global mean soil moisture value.}

\label{tab:regimes}
\end{table}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_scale_gammaT_tilde_OvGM}

\caption[Summertime surface temperature response in the alternative toy model
formulation.]%
%
{Summertime surface temperature response in the alternative toy model
formulation $\gammaalt^2\Var(T)$ (colored contoured).  Superposed is the line
where soil moisture is equal to the global mean soil moisture (solid red
line).}

\label{fig:comp_scale_gammaT_tilde_OvGM}
\end{center}
\end{figure}



