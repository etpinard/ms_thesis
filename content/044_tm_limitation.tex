%
% Chapter 4
%
% -------------------------------------------------------------------------------
\section{Toy model limitations}
\label{sec:tm_limits}
%
%   
%
% -------------------------------------------------------------------------------

%% Intro to section 

In developing the toy model, we made several assumptions to arrive at a
simplified representation of the surface temperature and soil moisture
variability over land.
%
Almost everywhere, the toy model underestimates both the (surface) temperature
variance and (surface-layer) soil moisture variance.
%
In this section, we investigate why such calibration errors are found in the toy
model and discuss limitation of the simplified framework employed in this study.

\subsection{Calibration errors by individual terms}
%
% comp_tm_T_no_E00
% comp_tm_T_no_Hs00
%
% -------------------------------------------------------------------------------

We first investigate the calibration errors due to parameterizing individual
terms in the surface energy anomaly budget.  We focus on evapotranspiration and
sensible heat flux because upwelling longwave radiation and the ground heat flux
where found to be accurately parameterizable and small in magnitude,
respectively.
%
We start by adding the parameterization residual of $H_s'$, $\Fluu$ and $G'$
(but not $E'$) into the toy model expression for $T'$ (equation \ref{eq:tm_T}):

\begin{equation}
 T'_{E_{00}'=0} \;=\; 
 \frac{1}{\;\gamma\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda (1 - \chi) \pt \big)\, F_0' 
 \;-\; L\, \big( \pt 
 \alpha (1 - \lambda) + \chi (1 + \alpha\lambda - \beta) 
 \pt \big)\, P' 
 \;-\; \Fluuo - \Hsoo - G_0'
 \, \Big]
 \enskip. \label{eq:tm_T_no_E00}
\end{equation}

\noindent%
%
So that the errors due to by an improper parameterization of $E$ are isolated.
%
Figure \ref{fig:comp_tm_T_no_E00} shows the $\Var(T_{E_{00}'=0})$ calibration
ratio (\ie $\Var(T_{E_{00}'=0})\big/\Var(T\subt{data})$).
%
The $E$ parameterization causes minimal errors in drylands. The high latitudes
have mostly positive $\Var(T)$ errors and the tropics mostly negative $\Var(T)$.
%
That is, the toy model $E$ parameterization over-damps temperature variability
in the tropics and under-damps it in the high latitudes.
%
Note that $E'$ was found to have only minimal impacts on the soil moisture
anomaly budget (see figure \ref{fig:comp_scale_F0_P_m}); correspondingly,
parameterization errors do not affect $\Var(m)$ significantly.

We proceed with a similar analysis of the sensible heat flux parameterization.
This time, we add the parameterization residual of $E_{00}'$, $\Fluu$ and $G'$
into the toy model expression for $T'$, isolating errors induced by an improper
parameterization of $H_s$. We write:

\begin{equation}
 T'_{\Hsoo=0} \;=\; 
 \frac{1}{\;\gamma\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda (1 - \chi) \pt \big)\, F_0' 
 \;-\; L\, \big( \pt 
 \alpha (1 - \lambda) + \chi (1 + \alpha\lambda - \beta) 
 \pt \big)\, P' 
 \;-\; \Fluuo - E_{00}' - G_0'
 \, \Big]
 \enskip. \label{eq:tm_T_no_Hs00}
\end{equation}

\noindent%
%
Figure \ref{fig:comp_tm_T_no_Hs00} shows the $\Var(T_{\Hsoo=0})$ calibration
ratio. Almost everywhere, the $H_s'$ parameterization errors are responsible for
an underestimation of the temperature variance. 
%
Again, the \ncep dataset is an outlier.
%
Compared to the toy model variance calibration ratio (removing all
parameterization residuals, see figure \ref{fig:comp_tm_Var_T_bias}), the
patterns observed in figure \ref{fig:comp_tm_T_no_Hs00} are quite similar.
%
Together, these results suggest that the underestimation of $\Var(T)$ by the toy
model is partly due to by an improper representation of the sensible heat flux
anomalies.
%
%More clearly, $H_s'$ in the toy model over-damps temperature variability.
%%
%Note that the alternative $H_s'$ parameterization gives the opposite result:
%$T'$ is overestimated when setting $\Hsoo=0$. 

%\newpage

%Other variable governing $E'$ and $H_s'$ \ldots
%% comment on error compensation ...

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_tm_T_no_E00}

\caption[Summertime calibration ratio of temperature variance, including only
errors due to the evapotranspiration parameterization.]%
%
{Summertime calibration ratio of temperature variance from equation
\ref{eq:tm_T_no_E00} isolating $E$ parameterization errors.}

\label{fig:comp_tm_T_no_E00}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_tm_T_no_Hs00}

\caption[Summertime calibration ratio of temperature variance, including only
errors due to the sensible heat flux parameterization.]%
%
{Summertime calibration ratio of temperature variance from equation
\ref{eq:tm_T_no_Hs00} isolating $H_s$ parameterization errors.}

\label{fig:comp_tm_T_no_Hs00}
\end{center}
\end{figure}

\subsection{Memory and non-linear processes}
\label{sec:memory_limits}
%
% comp_Corlag_F
% comp_Corlag_P
% gcms_Corlag_mbm
%
% -------------------------------------------------------------------------------

In section \ref{sec:tm_prelim}, we analyzed autocorrelation properties of
surface temperature and soil moisture. We noticed that some locations, such the
central US and the tropics, are characterized by autocorrelation values greater
than one month.
%
Figure \ref{fig:comp_Corlag_F} and \ref{fig:comp_Corlag_P} present the 1-month
autocorrelation (equation \ref{eq:auto_corr}) of radiation forcing ($F$) and
precipitation ($P$) respectively.
%
Interestingly, locations of pronounced surface temperature memory collocate with
locations of pronounced radiation forcing anomalies. 
%
Note that the radiation forcing memory is overestimated (\eg in Central Africa
in the \era) to due to fact that $F'$ contains a secular trend in its time
series (see appendix \ref{app:detrend}). That said, even with a detrended $F'$,
pronounced 1-month autolag correlations\footnote{%
%
Figure is available in \FigFolder.}
%
are observed, similar to our results for $T'$ in section
\ref{sec:tm_prelim}.
%

Recall that previously we found the surface-layer soil moisture had little
memory on monthly time scales (figure \ref{fig:comp_Corlag_m}). Then, how can
$T$ and $F$ attain memory on monthly time scales? 
%
%For hints, we turn back to figure \ref{fig:hist_tm_T_dist_full} where we found
%that extreme warm temperature anomalies are more frequent in the GCM datasets
%than in the toy model fit and weak $T'>0$ are less frequent.
%
Perhaps bottom-layer soil moisture, which features pronounced intra-seasonal
memory (see figure \ref{fig:gcms_Corlag_mb_Corlag_mf}), contributes to $T'$ on
some occasions.
%
In turn, bottom-layer soil moisture would provide an additional source of
variability for surface temperatures and surface-layer soil moisture (which the
toy model also underestimates, see figure \ref{fig:comp_tm_Var_m_bias}).
%
Figure \ref{fig:gcms_Corlag_mbm} presents the 1-month lagged correlation between
bottom-layer soil moisture content ($m_b$, the full-column soil moisture content
minus the surface-layer soil moisture) and the surface-layer soil moisture, with
$m_b$ leading $m$. 
%
Correlations are positive for most of the globe and they are, overall, more
pronounced than the precipitation 1-month auto-lagged correlation (see figure
\ref{fig:comp_Corlag_P}. This suggest the bottom-layer soil moisture anomalies
can propagate upwards to the surface soil layer on time scales on the order of
one month; this would add variability into the land-surface system system.

%
Alternatively, perhaps the autolag correlation in $F$ (and therefore $T$) is due
to a finite time-delay in soil moisture recycling.
%
On this topic, \citet{eltahir96} estimated that 25\% of all rain falling in the
Amazon basin is contributed by evapotranspiration within the basin.  Moreover,
\citet{dominguez06} noted that around 25\% of precipitated water originated from
soil moisture in 2.5$^\circ$ $\times$ 2.5$^\circ$ grid boxes over the central
US. For 1.5$^\circ$ $\times$ 1.5$^\circ$ grid boxes, \citet{vanderent10} found
that at most 10\% soil water is recycled globally.
%
%While these number appear small, note that they only take into account
%precipitated water; hence, exclude soil water affecting cloudiness (\ie
%$F_0'$).
%
While these number appear small, recycled soil moisture also affects cloudiness,
and hence it contributes to $F_0'$.
%
In short, soil moisture recycling is a non-linear process neglected in the toy
model which is perhaps at the root the toy model's underestimation of the
datasets' surface temperature variance.

%% more stuff on Var(m) ?
%Neglecting low frequency variability in soil moisture makes the toy model 
%$\Var(m)$ smaller than the datasets'. Most memory comes bottom layer moisture.
%Propose that it becomes significant only in extreme dry years, explaining the
%skewed $T'$ frequency distributions of figure \ref{fig:hist_tm_T_dist_full}.
%
%Caveat: the toy model $\Var(m)$ is almost perfect in the \ccsm's drylands.
%Propose significant error compensation making the toy model appear to perform
%better than it does.

\vfill

%\newpage

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_Corlag_F}

\caption[1-month autolag correlation of radiation forcing in summer.]%
%
{1-month autolag correlation of radiation forcing ($F$) in summer, see equation
\ref{eq:auto_corr}.}

\label{fig:comp_Corlag_F}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_Corlag_P}

\caption[1-month autolag correlation of precipitation in summer.]%
%
{1-month autolag correlation of precipitation ($P$) in summer, see equation
\ref{eq:auto_corr}.}

\label{fig:comp_Corlag_P}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.6\linewidth]{chap4/gcms_Corlag_mbm}

\caption[1-month lag correlation between bottom layer soil moisture and
surface-layer soil moisture.]%
%
{1-month lag correlation between bottom layer soil moisture and
surface-layer soil moisture, with the bottom-layer soil moisture leading by
1-month, see \ref{eq:auto_corr}. [\ccsm and \hadgem data].}

\label{fig:gcms_Corlag_mbm}
\end{center}
\end{figure}
