%
% Chapter 5
% 
% -------------------------------------------------------------------------------
\section{Analysis of temperature variability errors in GCMs}
\label{sec:correct_gcms}
%
% - change the 'correct' terminology.
% - change 'purple' in line plots
% - refer to T' as T_TM' ???
%
% - comment on 'all-coeffs' results in 5.1.2 and 5.1.3 ?? NO!
% - show tm_coeffs_exchange or *_all ?? NO!
%
% -------------------------------------------------------------------------------

%% Intro to section

The \ccsm and \hadgem have too much variance in summertime surface temperature
in dryland climates (see figure \ref{fig:comp_Var_T_bias}).
%
In section \ref{sec:la_summary}, we proposed that this erroneous amplification
is due to an incorrect geographical placement of the boundary between
energy-limited and moisture-limited evapotranspiration regimes. 
%
Using the toy model, we showed that theses evapotranspiration regimes were
delimited by the sign of the coupling coefficient ($\chi$).
% 
So, in section \ref{ssec:correct_att}, we examine whether the surface
temperature variance errors in the GCMs are due to errors in the coupling
coefficient, the forcings or the other land-surface processes.
%
%In section \ref{ssec:correct_att}, we use the toy model as a tool for
%attributing the GCMs' temperature variability errors to specific processes. 
%
Then, in sections \ref{ssec:correct_ssn} and \ref{ssec:correct_rean}, we attempt
to \emph{correct} the GCMs by adjusting the toy model expressions for surface
temperature variance.
%
That is, we illustrate how a misrepresentation of the forcings and the
land-surface processes affects temperature variability. However, we do not
attempt to correct the GCMs' physical parameterizations. 

\subsection{Attribution of temperature variability errors in GCMs}
\label{ssec:correct_att}
%
% hadgem1_box_regions_dry1-2
% line_mbar_sig_T_all
% line_forc_coeffs_all
%
% -------------------------------------------------------------------------------

We pose the attribution problem as follows. The toy model expresses the
(surface) temperature variance as a function of the non-precipitating radiation
forcing and precipitation variances, one decomposition coefficient and four toy
model coefficients (see equation \ref{eq:tm_Var_T}). More compactly, we write:

\begin{equation}
  \Var(T) \;=\; 
  \mcal{F}\big(\Var(F_0),\Var(P),\alpha\,;\,
  \gamma,\lambda,\beta,\chi\big)
  \enskip,
  \label{eq:att_Var_T}
\end{equation}

\noindent%
%
where $\mcal{F}$ is given by equation \ref{eq:tm_Var_T}. The contributions of
the forcing terms and the land-surface terms are separated by a semi-colon.
%
For simplicity, in this chapter, we assume that the toy model perfectly
represents $\Var(T)$ so that errors in $\Var(T)$ can be exclusively traced back
to errors in one or multiple terms on the RHS of equation \ref{eq:att_Var_T}.
%
In this section, the GCM forcings and toy model coefficients are evaluated
against \era reanalysis data. Hence, GCM disagreements with \era dataset are
considered to be \emph{errors} in this section.
%
We also use the \ncep dataset to evaluate mean-state soil moisture in GCMs (in
figure \ref{fig:line_mbar_sig_T_all}) since the \ncep's mean-state soil moisture
outputs performed well against observations in \citet{li05}. But otherwise the
\ncep is not-considered in the attribution analysis as the toy model expression
for $\Var(T)$ shows large calibration errors when fit to \ncep data (see section
  \ref{sec:tm_perf}).
%
Moreover, surface temperature and precipitation in the \era reanalysis product
and are closer to the observed quantities than are the \ncep products.  (see
section \ref{sec:data_results}).
%

We focus on two regions where $\Var(T)$ in both GCMs is much greater than the
observed: the central US and Eastern Europe. We form two box regions: they are
delimited in figure \ref{fig:hadgem1_box_regions_dry1-2}. 
%
We proceed by averaging the parameters and variances over all grid points lying
inside the two boxes for each of the datasets. For instance, the box-averaged
($[\,\cdot\,]$) standard deviation of temperature is:

\begin{equation}
  [ \Var(T)^\half ] \;\equiv\; 
  \frac{1}{N\subt{pts}} \, \sum_{k=1}^{N\subt{pts}} \Var(T)^\half_k \
  \enskip,
  \label{eq:box_avg}
\end{equation}

\noindent%
%
where $N\subt{pts}$ is the number of points lying inside the box region in
question and $k$ is the grid-point index.
%
Figure \ref{fig:line_mbar_sig_T_all} presents monthly climatological mean
(surface-layer) soil moisture and the standard deviation of temperature
$[\Var(T)^\half]$ for the months of May, June, July, August and September.
%
More clearly than in the figures of section \ref{sec:data_results}, we see that
the summer average $\mbar$ is far smaller in the GCMs than in the reanalysis
products. 
%
In addition, there is a pronounced decrease in $\mbar$ as summer proceeds in the
GCMs that is not seen in either reanalysis products.
%
Not shown previously, note that $\Var(T)$ decreases from July to August in both
boxes and for both GCMs.

We next examine the toy model expression for temperature variance.  Figure
\ref{fig:line_forc_coeffs_all} presents the non-precipitating $[\Var(F_0]$ and
precipitating $[L^2\alpha^2\Var(P)]$ components of the radiation forcing
variance (plotted as standard deviations) along with toy model
evapotranspiration efficiency $\lambda$ and the coupling coefficient $\chi$ for
all three summer months.

In box 1, both GCMs have erroneously large coupling coefficients ($\chi \simeq$
0.5) compared to the \era ($\chi \simeq$ 0.1) . That is, the amplifying
effects of soil moisture anomalies (as $\chi>$0) on $\Var(T)$ are grossly
overestimated in the GCMs.
%
Moreover, the evapotranspiration efficiency $\lambda$ is slightly underestimated
in both GCMs which also contributes to the excessive $\Var(T)$ in the GCMs.
%
Both components of the \hadgem's radiation forcing agree well the \era at summer
every month. The \ccsm features errors in the precipitating component of
$\Var(F)$: it is erroneously large in June and erroneously small in August.
%
These errors contribute to the excessive $\Var(T)$ June and to the decrease in
$\Var(T)$ from July to August in the \ccsm shown in figure
\ref{fig:line_mbar_sig_T_all}.

In box 2, both GCMs display positive coupling coefficients in all summer months
whereas the \era has $\chi<$0 for each summer month. Hence, the GCMs erroneously
place Eastern Europe in the land-surface amplified $\Var(T)$ regime.
%
The precipitating component of $\Var(F)$ ($L\alpha\Var(P)^\half\simeq$
3$\Wm[-2]$) is overestimated in both GCMs at every month:  in the summer
average.
%
In addition, the \ccsm has $\Var(F_0)^\half\simeq$ 4$\Wm[-2]$ larger than in the
\era and its evapotranspiration efficiency $\lambda$ is underestimated.
%
In brief, GCM $\Var(T)$ errors in Eastern Europe are due to errors in the
forcings and to errors in land-surface processes, whereas for the central US,
figure \ref{fig:line_forc_coeffs_all} suggest that $\Var(T)$ errors are, to
first order, due to errors in the coupling coefficient (more in section
\ref{ssec:correct_rean}).

To test our first hypothesis (of chapter \ref{chp:intro}) which proposes a link
between GCM temperature variability errors and soil moisture, we focus on the
impacts of erroneous $\lambda$ and $\chi$. That is, we investigate GCM errors
originating from errors in mean-state moisture leading to errors in $\lambda$
(see section \ref{sec:tm_mean_state}) and misrepresentation of the coupling
between soil moisture and temperature variability (\ie $\chi$).
%
In this regard, we propose two methods for \emph{correcting} temperature
variance in the GCMs.  They are presented next.

\vfill

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap5/hadgem1_box_regions_dry1-2}

\caption[Location of box regions 1 and 2.]%
%
{Location of box 1 (central US) and box 2 (Eastern Europe).
%
The number of grid points inside each of the boxes varies from one dataset to
the other as the four datasets do not have the same resolution.
%
In box 1: the \ccsm has 224 grid points, the \hadgem has 195, the \era has 80
and the \ncep has 143. 
%
In box 2: the \ccsm has 176 grid points, the \hadgem has 156, the \era has 70
and \ncep has 108.}

\label{fig:hadgem1_box_regions_dry1-2}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.8\linewidth]{chap5/line_mbar_sig_T_all}

\caption[Box-averaged mean soil moisture and standard deviation of surface
temperature in summer in box regions 1 and 2.]%
%
{Box-averaged mean soil moisture and standard deviation of surface temperature
in summer in box regions 1 and 2 (see figure
\ref{fig:hadgem1_box_regions_dry1-2}).
%
Each dataset is plotted in a different color.
%
The different line styles and symbols correspond to different quantities.}

\label{fig:line_mbar_sig_T_all}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.88\linewidth]{chap5/line_forc_coeffs_all}

\caption[Box-averaged non-precipitating radiation forcing variance,
precipitating radiation \newline forcing variance, toy model evapotranspiration
efficiency and toy model coupling coefficient in box regions 1 and 2.]%
%
{Boxed-averaged non-precipitating and precipitating radiation forcing standard
deviation $[\Var(F_0)^{1/2}]$ and $[L\alpha\Var(P)^{1/2}]$ and toy model
coefficients $[\lambda]$, $[\chi]$ in box regions 1 and 2 (see figure
\ref{fig:hadgem1_box_regions_dry1-2}).
%
The right-most (Sum. avg.) column is the average of the June, July and August
values.
%
Each dataset is plotted in a different color.
%
The different line styles and symbols correspond to different quantities.}

\label{fig:line_forc_coeffs_all}
\end{center}
\end{figure}

\subsection{The relative contribution of errors in the seasonal changes in the
land-surface processes to errors in temperature variance}
\label{ssec:correct_ssn}
%
% hadgem1_tm_coeffs_all_june ccsm3_tm_coeffs_all_june  ( or *_no_mask )
%
% -------------------------------------------------------------------------------

Here, we investigate to what degree the temperature variance errors in the GCMs
are due to errors in the coupling coefficient ($\chi$) and the
evapotranspiration efficiency ($\lambda$) that are in turn due to errors in
mean-state soil moisture in summer. 
%
%Specifically, we adjust the toy model expression for $\Var(T)$ by removing the
%effects of the summer soil moisture drying in the GCMs, which is not evident in
%the two reanalysis products used in this study (see figure
%\ref{fig:line_mbar_sig_T_all} and section \ref{ssec:T_var_err_and_m}).
First, in figure \ref{fig:line_forc_coeffs_all}, we noticed that both $\chi$ and
$\lambda$ show erroneous seasonal changes (compared to the \era) that act to
increase temperature variance over the course of summer, consistent with a link
between $\chi$, $\lambda$ and mean-state soil moisture as from analysis of the
section \ref{sec:tm_mean_state}.
%
Recall that toy model coefficients are computed separately for the three summer
months.  So, in a first attempt to \emph{correct} the  $\Var(T)$ errors in the
GCMs, we set $\chi$ and $\lambda$ to their June value for all three summer
months.  Substituting into into the toy model expression for $\Var(T)$ (as in
equation \ref{eq:att_Var_T}) gives:

\begin{equation}
  \Var(T) \;=\; 
  \mcal{F}\big( \Var(F_0),\Var(P),\alpha;
  \gamma,\lambda\subt{June},\beta,\chi\subt{June} \big)
  \enskip,
  \label{eq:june_Var_T}
\end{equation}

\noindent%
%
where the effects of the GCMs' erroneous seasonal changes in $\lambda$ and
$\chi$ in summer are removed from the toy model's $\Var(T)$ expression.
%
Figures \ref{fig:hadgem1_tm_coeffs_all_june} and
\ref{fig:ccsm3_tm_coeffs_all_june} show the results for the \hadgem and \ccsm
respectively. For presentation purposes, all panels show summer averages of the
temperature variance normalized by the observed temperature variance.
%
Since the toy model expression for $\Var(T)$ underestimates the \hadgem and
\ccsm outputs (see sections \ref{sec:tm_perf} and \ref{sec:tm_limits}), the
normalized toy model $\Var(T)$ (shown in the middle panels, tm-obs) is closer to
unity than the normalized $\Var(T)$ from the GCMs (shown in the top panels,
data-obs).
%
The bottom panels show the normalized the toy model $\Var(T)$ with $\lambda$ and
$\chi$ fixed to June values for all three summer months, as in equation
\ref{eq:june_Var_T}. 
%
Hence, by comparing the bottom panels (tm.june-obs) to the middle panels
(tm-obs) in figures \ref{fig:hadgem1_tm_coeffs_all_june} and
\ref{fig:ccsm3_tm_coeffs_all_june}, we can quantify the effects of the GCMs'
erroneous seasonal changes in $\lambda$ and $\chi$ in summer on temperature
variance.
%
If the GCMs' $\Var(T)$ errors are independent of the GCMs' mean-state soil
moisture drying in summer, panels (tm-obs) and (tm.june-obs) would be identical.

Figures \ref{fig:hadgem1_tm_coeffs_all_june} and
\ref{fig:ccsm3_tm_coeffs_all_june} show that $\Var(T)$ in panel (tm.june-obs) is
reduced compared to panel (tm-obs). 
%
Hence, we conclude that $\Var(T)$ errors in the GCMs are partly due the GCMs'
erroneous seasonal change in $\lambda$ and $\chi$ in summer (linked to a soil
moisture drying from June to August) where increasing $\chi$ and decreasing
$\lambda$ act to increase $\Var(T)$.
%
The largest temperature variance increase due to the GCM's erroneous seasonal
changes in $\lambda$ and $\chi$ in summer occurs in the central US in the \ccsm
where $\Var(T)$ increases by up to 100\%.

\newpage

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.65\linewidth]{chap5/hadgem1_tm_coeffs_all_june}

\caption[Summertime surface temperature variances from the \hadgem archives, the
toy model fit to \hadgem data and the toy model to \hadgem data using only June
toy model coefficients.]%
%
{Panel (data-obs): Summertime ratio of the \hadgem $\Var(T)$ to the observed
(University of Delaware) $\Var(T)$. %
%
Panel (tm-obs): Summertime ratio of the toy model $\Var(T)$ fit to \hadgem data
with the observed $\Var(T)$. %
%
Panel (tm.june-obs): Summertime ratio of the toy model $\Var(T)$ fit to \hadgem
data with June $\chi$ and $\lambda$ set for all three summer months (see
equation \ref{eq:june_Var_T}). %
%
The results are cropped around box regions 1 and 2 (see figure
\ref{fig:hadgem1_box_regions_dry1-2}).}

\label{fig:hadgem1_tm_coeffs_all_june}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.65\linewidth]{chap5/ccsm3_tm_coeffs_all_june}

\caption{As in figure \ref{fig:hadgem1_tm_coeffs_all_june} but with \ccsm
data instead of \hadgem data.}
%\caption{All show summer average ratios with U. Delaware's $\Var(T)$ as
%denominator. Panel (data-obs) $\Var(T)$ from \ccsm outputs. Panel (tm-obs) toy
%model $\Var(T)$ fit to \ccsm data. Panel (tm.june-obs) toy model $\Var(T)$ fit
%\ccsm data with June toy model coefficients set for all three summer months.}

\label{fig:ccsm3_tm_coeffs_all_june}
\end{center}
\end{figure}

\subsection{The relative contribution of errors in the forcings and errors in
the land-surface processes to the errors in temperature variance}
\label{ssec:correct_rean}
%
% hadgem1_era40_tm_coeffs_exchange ccsm3_era40_tm_coeffs_exchange
% 
% ( or *_all , *_no_mask )
%
% -------------------------------------------------------------------------------

In this section, we again attempt to \emph{correct} the $\Var(T)$ errors in the
GCMs by adjusting the toy model coefficients $\chi$ and $\lambda$.
%
Here, we exchange the GCM and \era forcings and toy model coefficients to
illuminate the relative contribution of the errors in the forcings and the
errors in the land-surface processes to the errors in temperature variance
simulated by the GCMs.
%
To do so, we first compute the temperature variance using GCM forcings and \era
coefficients:

\begin{equation}
  \Var(T) \;=\; 
  \mcal{F}\big( \Var(F_0)\subt{GCM},\Var(P)\subt{GCM},\alpha\subt{GCM};
  \gamma\subt{GCM},\lambda\subt{ERA40},\beta\subt{GCM},\chi\subt{ERA40} \big)
  \enskip,
  \label{eq:gcm_rean_Var_T}
\end{equation}

\noindent%
%
where the forcings as well as the toy model coefficients $\gamma$ and $\beta$
are taken from the GCMs (\hadgem or \ccsm) and the toy model coefficients
$\lambda$ and $\chi$ are of the \era reanalysis. Expression
\ref{eq:gcm_rean_Var_T} is referred to as (gcm.forc / rean.coef).
%
Now, note that if the $\Var(T)$ errors in the GCMs are entirely due to errors in
$\chi$ and/or $\lambda$, then expression \ref{eq:gcm_rean_Var_T} would perform
as well as the toy model with \era forcings and \era coefficients.
%
In turn, to quantify effects of errors on $\lambda$ and $\chi$ on $\Var(T)$, we
compute:

\begin{equation}
  \Var(T) \;=\; 
  \mcal{F}\big( \Var(F_0)\subt{ERA40},\Var(P)\subt{ERA40},\alpha\subt{ERA40};
  \gamma\subt{ERA40},\lambda\subt{GCM},\beta\subt{ERA40},\chi\subt{GCM} \big)
  \enskip,
  \label{eq:rean_gcm_Var_T}
\end{equation}

\noindent%
%
which we refer to as (rean.forc / gcm.coef).

Figures \ref{fig:hadgem1_era40_tm_coeffs_exchange} and the
\ref{fig:ccsm3_era40_tm_coeffs_exchange} show the results of expression
\ref{eq:gcm_rean_Var_T} and \ref{eq:rean_gcm_Var_T} for the \hadgem and \ccsm
respectively.  
%
As in section \ref{ssec:correct_ssn}, we choose to normalized the results with
University of Delaware observations. 
%
In both figures, the top panel (rean.forc / rean.coef) is the toy model fit to
\era data (forcings and toy model coefficients).  We first notice that the toy
model fit to \era data does well at reproducing the observed $\Var(T)$: it
underestimates the observed $\Var(T)$ by approximatively 30\% in both box
regions.
%
Moreover, by comparing the middle panels (gcm.forc / rean.coef) and the top
panels (rean.forc / rean.coef), we conclude that errors (relative to the \era)
in the forcings, $\gamma$ and $\beta$ in the GCMs act to damp $\Var(T)$ by
approximatively 25\% in both box regions.
%
Further analysis reveals that the surface temperature resistance ($\gamma$) is
larger in both GCMs than in the \era, partly explaining the results\footnote{%
%
The surface temperature resistance ($\gamma$) is excluded from the analysis of
this chapter in effort to assess the effects of soil moisture on $\Var(T)$.
%
Recall that no conclusive link between $\gamma$ and $\mbar$ were identified at
the global scale (see section \ref{sec:tm_mean_state}).}
%
of figures \ref{fig:hadgem1_era40_tm_coeffs_exchange} and
\ref{fig:ccsm3_era40_tm_coeffs_exchange}.
%
Importantly, by comparing the bottom panels (gcm.coef / rean.forc) and the top
panels (rean.forc / rean.coef), we conclude that errors in $\lambda$ and $\chi$
act to increase $\Var(T)$ by approximatively 100\% in both GCMs.

In summary, it appears that errors in the toy model coefficients $\lambda$ and
$\chi$ in the GCMs are responsible for the excessive summertime temperature
variance in both box regions.
%
We note also that, while having smaller net effects on $\Var(T)$, errors in the
forcings in the GCMs partly compensate the effects of the $\lambda$ and $\chi$
errors on $\Var(T)$.
%
%

%\tcomment{I made plots similar plots showing exchanges of
%$\lambda,\chi,\beta,\gamma$.}

\vfill
%\pagebreak

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.65\linewidth]{chap5/hadgem1_era40_tm_coeffs_exchange}

\caption[Summertime surface temperature variance from the toy model fit to \era
data, using \hadgem toy model coefficients and \era forcings and using \era toy
model coefficients and \hadgem forcings.]%
%
{Panel (rean.forc / rean.coef): Summertime ratio of the toy model $\Var(T)$ fit
to \era data to the observed (University of Delaware) $\Var(T)$. %
%
Panel (gcm.forc / rean.coef): Summertime ratio of the toy model $\Var(T)$ fit
using \hadgem $\Var(F_0)$, $\Var(P)$, $\alpha$, $\gamma$, $\beta$ and \era
$\lambda$, $\chi$ (see equation \ref{eq:gcm_rean_Var_T}) to the observed
$\Var(T)$. %
%
Panel (gcm.coef / rean.forc): Summertime ratio of the toy model $\Var(T)$ fit
using \hadgem $\lambda$, $\chi$ and \era $\Var(F_0)$, $\Var(P)$, $\alpha$,
$\gamma$, $\beta$ (see equation \ref{eq:rean_gcm_Var_T}) to the observed
$\Var(T)$.
%
The results are cropped around box regions 1 and 2 (see figure
\ref{fig:hadgem1_box_regions_dry1-2}). %
%
\hadgem quantities are interpolated to the resolution of the \era.}

\label{fig:hadgem1_era40_tm_coeffs_exchange}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.65\linewidth]{chap5/ccsm3_era40_tm_coeffs_exchange}

\caption{As in figure \ref{fig:hadgem1_era40_tm_coeffs_exchange} but with \ccsm
data instead of \hadgem data.}

\label{fig:ccsm3_era40_tm_coeffs_exchange}
\end{center}
\end{figure}
