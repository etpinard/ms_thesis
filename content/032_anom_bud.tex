%
% Chapter 3
%
% -------------------------------------------------------------------------------
\section{The anomaly budget equations}
\label{sec:anom_bud_eqs}
%
% - update downloaded variable table reference in 3.2.1
%
% - 3.2.2: The last conclusions, stemming from structure of toy model, were all
% qualitative. You  will approximate their contributions to the overall
% uncertainties in T and m quantitatively; lay this out now.
%
% - citation for dryland evapotranspiration (light vegetation)?
% - figure out if snow analysis should be here or in appendix!
% - similarly for mean budget residuals ?
%
% -------------------------------------------------------------------------------

\subsection{Decomposing the budget equations}
\label{ssec:bud_decomp}
%
% no figs.
% 
% -------------------------------------------------------------------------------

In developing the toy model, we seek simplified expressions for the surface
temperature and surface-layer soil moisture fluctuations.  To do so, we first
separate the budget equations of \eqref{eq:budgets} into \emph{climatological
mean budget} and \emph{anomaly budget} equations by decomposing each budget
variable month-by-month into a 30-year (20-year for the \ncep) monthly mean and
anomalies about the 30-year mean, as in equation \ref{eq:anom}, focusing on
summertime monthly and longer-time-scale variability.  
%
At this point, we only include variables that can be directly retrieved from the
GCM and reanalysis archives (see section \ref{ssec:vars_list} and table
\ref{tab:vars}). We write the land-surface climatological mean budget equations
as:

\begin{subequations}
\begin{align}
  0 \;&=\; \Fbar \;-\; L\pt \Ebar \;-\; \Hsbar \;-\; \Flubar \;-\; \xiUbar
%  \enskip,
  \label{eq:bud_mean_engy} \\
  0 \;&=\; \Pbar \;-\; \Ebar \;-\; \Rbar \;-\; \ximbar
  \enskip,
  \label{eq:bud_mean_mois}
\end{align}
  \label{eq:bud_mean}
\end{subequations}

\noindent%
%
where $\xiUbar$ and $\ximbar$ are the surface energy and (surface-layer) soil
moisture \emph{budget residuals} respectively.  They are defined as to make the
climatological mean budget equations \eqref{eq:bud_mean_engy} and
\eqref{eq:bud_mean_mois} balance for each summer month using only archived
variables. 
%
The surface energy budget residual includes the surface energy storage term
($\dervside{U}{t}$), ground heat flux through the land surface ($G$) and
temperature advection. 
%
The soil moisture budget residual is comprised of the soil moisture storage in
surface soil layer ($\dervside{m}{t}$), the vertical flux of moisture at the 10
\cm soil depth ($I$), the horizontal flux of moisture inside the surface soil
layer and the melting snow source term.
%
Scalings of these budget residuals, in particular the importance of the storage
terms ($\dervside{U}{t}$ and $\dervside{m}{t}$), are presented in section
\ref{ssec:bud_res}.  Correspondingly, the land-surface anomaly budget equations
are:

\begin{subequations}
\begin{align}
  0 \;&=\; F' \;-\; L\pt E' \;-\; H_s' \;-\; \Fluu \;-\; \xiU'
%  \enskip,
  \label{eq:bud_anom_engy} \\
  0 \;&=\; P' \;-\; E' \;-\; R' \;-\; \xim'
  \enskip.
  \label{eq:bud_anom_mois}
\end{align}
  \label{eq:bud_anom}
\end{subequations}

\noindent%
%
By including $\xiU'$ and $\xim'$, the surface energy and soil moisture anomaly
budget residuals respectively, we make \eqref{eq:bud_anom_engy} and
\eqref{eq:bud_anom_mois} balance at every time entry in the sample.
%
The above equations are the backbone of the toy model.  They relate fluctuations
in the land-surface processes that toy model attempts to trace back to
fluctuations in the two state variables.
%
Notice that the anomaly budget equations, as written in \eqref{eq:bud_anom},
make no explicit reference to the state variables; these are hidden inside the
land-surface processes and budget residuals.

Recall that both of the GCMs analyzed do not archive the surface
evapotranspiration flux (see section \ref{ssec:vars_list}).  Evapotranspiration
is computed by dividing the surface latent heat flux by a constant latent heat
of vaporization (so that $E$ has units of $\mm\s[-1]$, see section
\ref{ssec:vars_list}). 
%
In doing so, we make no distinction on the origin of the liquid (or frozen)
water transfered from soils to atmosphere during evapotranspiration, introducing
another component to the soil moisture budget residuals $\ximbar$ and $\xim'$.
%
Note that the surface-layer soil moisture budget, as written in equations
\eqref{eq:bud_mean_mois} and \eqref{eq:bud_anom_mois}, is inconsistent: only a
fraction of the total evapotranspiration transports soil moisture out of the
surface soil layer; the remaining moisture is transported from the bottom soil
layer.
%
That said, scale analysis presented in section \ref{ssec:scales} reveals that
the this artificial sink of surface-layer soil moisture has only minor impacts
on our interpretation of the anomaly budget equation.

\subsection{Budget residuals}
\label{ssec:bud_res}
%
% comp_scale_xiU_F 
% comp_scale_ddt_xiU    
% comp_corr_xiUF
%
% comp_scale_xim_P                          
% comp_scale_ddt_xim   
% comp_corr_ximP
%
% -------------------------------------------------------------------------------

We first assess the importance of the surface energy anomaly budget residual
$\xiU'$ in the anomaly budget equation \eqref{eq:bud_anom_engy}. Figure
\ref{fig:comp_scale_xiU_F} presents the summertime ratio of $\xiU$ variance to
radiation forcing variance (\ie $\innerp{\xiU'}{\xiU'}\big/\innerp{F'}{F'}$).  
%
In the \hadgem and \era, the variance in $\xiU'$ is almost everywhere less than
20\% of the variance in the radiation forcing. The highest $\xiU'$ magnitudes
are in the high northern latitudes, where variability in snow melt is  expected
to be significant factor controlling surface energy.  The \ncep and \ccsm
feature larger magnitudes of $\xiU'$ in high latitude climates suggesting more
important snow melt in these datasets.  The variance in $\xiU'$ in the \ccsm is
greater than in the three other datasets in the mid-latitude and especially in
the tropical climates. 

We next ask: how important is the surface energy storage in $\xiU'$? To do so,
we must define an \emph{effective} heat capacity for the surface.  This
effective heat capacity is meant to be multiplied by the surface temperature $T$
to yield the surface air enthalpy. Using the specific heat capacity of air at
constant pressure and assuming an hydrostatic atmosphere, we have that:

\begin{equation}
  C\subt{atm} \;=\;
  c_p \frac{p\subt{sfc}}{g} 
  \;\simeq\;
  \frac{1004 \J \K[-1] \kg[-1] \,\times\, 101325 \Pa}
  {9.81 \m\s[-2]} 
  \;\simeq\; 1.37{\times}10^7 \J\K[-1]\m[-2]
  \enskip.
  \label{eq:Cp}
\end{equation}

\noindent%
%
The above represents the heat capacity of the entire atmospheric column above
mean sea-level pressure. But, only a fraction of the atmosphere is affected by
land-surface processes. To give a reasonable upper bound on magnitude of surface
energy storage, we choose:

\begin{equation}
  C\subt{eff}\;\equiv\;\tfrac{1}{10}\,C\subt{atm}
  \;\simeq\; 1.37{\times}10^6 \J\K[-1]\m[-2]
  \enskip,
  \label{eq:Ceff}
\end{equation}

\noindent%
%
which is roughly the mass of air contained in the planetary boundary layer
(PBL).
%
%For reference, one tenth of the atmospheric column in hydrostatic balance
%corresponds to a height of approximatively 850\m from sea level or equivalently
%the height of the planetary boundary layer. 
%
Alternatively, the effective heat capacity of the surface could be defined using
the specific heat capacity and density of the land surface ranging from
$\simeq$800-1480 $\J\kg[-1]\K[-1]$ and $\simeq$1000-1600 $\kg\m[-3]$
respectively \citep{soils}.
%
Hence, the effective heat capacity associated with the surface soil layer is
approximately equal to 0.8-2.4$\times10^{5}\J\K[-1]\m[-2]$, which is a small
fraction of the PBL heat capacity.
%
%The effective capacity value defined using atmospheric quantities in
%\eqref{eq:Ceff} is equivalent to a land-based heat capacity for 4 to 13 meters
%of soils.

We next scale the magnitude of the surface energy storage anomalies in summer by
computing a finite difference approximation of the time derivative:

\begin{equation}
  \Bigg| \derv{X'}{t} \Bigg|^2
  \;=\; 
  \frac{1}{2N\subt{year}}\,
  \Bigg[\pt 
  \sum_{i=1}^{N\subt{year}} 
  \Bigg( \frac{X'_{i,2} - X'_{i,1}}{\Deltat} \Bigg)^2 
  \;+\;
  \sum_{i=1}^{N\subt{year}}
  \Bigg( \frac{X'_{i,3} - X'_{i,2}}{\Deltat} \Bigg)^2 
  \pt\Big]
  \enskip.
  \label{eq:scale_ddt}
\end{equation}

\noindent%
%
Using $X'_{i,j}{=}C\subt{eff}T'_{i,j}$\pt, where $\Deltat$ is 1 month, $i$ and
$j$ are the year and month indices respectively. In figure
\ref{fig:comp_scale_ddt_xiU}, $|C\subt{eff}\pt\dervside{T'}{t}|^2$ (with
$C\subt{eff}$ as computed in equation \ref{eq:Ceff}) is presented in fractions
of summertime $\xiU'$ variance . Across all datasets, the summertime surface
energy storage constitutes less than 40\% of the budget residual. 
% 
In fact, surface energy storage is less than 10\% of $\xiU'$ globally in the
\ccsm and \era.  
%
Together with the results of figure \ref{fig:comp_scale_xiU_F}, we conclude the
surface energy storage makes an insignificant contribution to the surface energy
anomaly budget equation. By elimination, the dominant component of surface
energy anomaly budget residual, except in high latitude climate, is the heat
transfer through the land-surface interface in the form of a ground heat flux.
%
Figure \ref{fig:comp_Cor_xiUF} presents the summertime instantaneous correlation
between $\xiU$ and the radiation forcing. Apart from the tropical climate
regions in the \ccsm, $\xiU$ behaves as expected from ground heat flux in
relation to the radiation forcing.

The variance in the soil moisture anomaly budget residual $\Var(\xim')$ is shown
in figure \ref{fig:comp_scale_xim_P}, normalized by the summertime precipitation
variance.  For most the globe, the variance in $\xim'$ is comparable to the
variance in precipitation. Hence, $\xim'$ is a significant contributor to the
surface-layer anomaly soil moisture budget.  Appropriately approximating $\xim'$
is crucial for developing the toy model. We then ask: to what degree are
fluctuations in soil moisture storage contributing to $\xim'$?

As shown in figure \ref{fig:comp_scale_ddt_xim}, the soil moisture storage
(computed using equation \ref{eq:scale_ddt} with $X'{=}m'$) is a negligible part
of the soil moisture anomaly budget residual and is (together with figure
\ref{fig:comp_scale_xim_P}) a small component of the surface-layer soil moisture
budget.
% 
Therefore, in selecting the surface-layer soil moisture in developing our toy
model, we made soil moisture memory --- an often cited mechanism linking soil
moisture and temperature variability --- negligible.
%
By elimination, the most important component of the surface-layer soil moisture
anomaly budget is the vertical transport of soil moisture, called infiltration
(with symbol $I$).
%
Figure \ref{fig:comp_Cor_ximP} shows the summertime instantaneous correlation
between $\xim'$ and precipitation. The soil moisture anomaly budget residual is
positively correlated with precipitation, except in a few isolated locations.
%
That is, $\xim'$ behaves as we would expect from soil water infiltration (more
in section \ref{ssec:param_xim} and \ref{sec:tm_mean_state}).

%% \ref{fig:comp_Pxim}
%\comment{Should I show a map of $\xim',P'$ correlation to argue that it is in
%fact infiltration anomalies?} 

%Overall, both anomaly budget residual's scaling with respect forcing terms are
%of comparable magnitude across all four datasets. In particular, the \hadgem and
%the \era show very similar results in figures \ref{fig:comp_scale_xiU_F} and
%\ref{fig:comp_scale_xim_P}. 
%%
%Additionally, recall from section \ref{sec:data_results}, that these two
%datasets have similar forcing anomaly magnitude, but different soil moisture
%content and surface temperature variability magnitudes. This suggests that
%comparing the toy model \emph{fits} to \hadgem and \era data would isolate the
%effects of a soil moisture misrepresentation with respect to errors in
%temperature variability. A full discussion is presented in chapter
%\ref{chp:apps}.

The climatological mean budget residuals do not contribute to our understanding
of surface temperature and soil moisture variability; hence, they are of little
interest for our toy model.
% 
That said, for completeness, figures showing scalings of $\xiUbar$ and $\ximbar$
in comparison to the summertime mean radiation forcing and mean snow melt in
summer are available in \FigFolder.

\vfill

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_xiU_F}

\caption[Summertime ratio of the surface energy anomaly budget residual variance
to the radiation forcing variance.]%
%
{Summertime ratio of the variance in surface energy anomaly budget residual to
the variance in radiation forcing, $\innerp{\xiU'}{\xiU'}\big/\innerp{F'}{F'}$.}

\label{fig:comp_scale_xiU_F}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_ddt_xiU}

\caption[Summertime ratio of the surface energy storage anomaly magnitude to the
surface energy anomaly budget residual variance.]%
%
{Summertime ratio of the surface energy storage anomaly magnitude (see equation
\ref{eq:scale_ddt}) to the variance in the surface energy anomaly budget
residual, $|C\subt{eff}\dervside{U'}{t}|^2\big/\Var(\xiU)$.}

\label{fig:comp_scale_ddt_xiU}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_Cor_xiUF}

\caption[Summertime correlation coefficient between the surface energy budget
residual and radiation forcing.]%
%
{Summertime correlation coefficient between the surface energy budget residual
and radiation forcing, $\corr(\xiU,F)$.}

\label{fig:comp_Cor_xiUF}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_xim_P}

\caption[Summertime ratio of the soil moisture anomaly budget residual variance
to the precipitation variance.]%
%
{Summertime ratio of the variance in soil moisture anomaly budget residual to
the variance in precipitation, $\innerp{\xim'}{\xim'}\big/\innerp{P'}{P'}$.}

\label{fig:comp_scale_xim_P}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_ddt_xim}

\caption[Summertime ratio of the soil moisture storage anomaly magnitude to the
soil moisture anomaly budget residual variance.]
%
{Summertime ratio of the (surface-layer) soil moisture storage anomaly magnitude
(see equation \ref{eq:scale_ddt}) to the variance in soil moisture anomaly
budget residual, $|\dervside{m'}{t}|^2\big/\Var(\xim)$.}

\label{fig:comp_scale_ddt_xim}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_Cor_ximP}

\caption[Summertime correlation coefficient between the soil moisture budget
residual and precipitation.]%
%
{Summertime correlation coefficient between the soil moisture budget residual
and precipitation, $\corr(\xim,P)$.}

\label{fig:comp_Cor_ximP}
\end{center}
\end{figure}

%\newpage

\subsection{Some important scalings}
\label{ssec:scales}
%
% comp_bowen_mean
% comp_bowen_anom 
% comp_scale_E_P_mean
% comp_scale_E_P_anom
%
% -------------------------------------------------------------------------------

Before discussing the best method for parameterizing the land-surface processes,
we first investigate some important scalings in the anomaly budget equations. 
%
Figure \ref{fig:comp_bowen_mean} presents the climatological mean Bowen ratio,
defined as $\Hsbar\big/L\Ebar$ and figure \ref{fig:comp_bowen_anom} shows the
\emph{anomaly Bowen ratio}, defined as \\
$\innerp{H_s'}{H_s'}\big/\innerp{LE'}{LE'}$.
%
The anomaly Bowen ratio is a non-standard metric which compares the
characteristic fluctuation magnitudes of the two turbulent heat fluxes.
%
Interestingly, the turbulent heat flux fluctuations scale differently than their
mean values (see figure \ref{fig:comp_bowen_mean} and
\ref{fig:comp_bowen_anom}). In drylands, latent heat flux anomalies are
typically larger than sensible heat flux anomalies whereas, in the mean,
$L\Ebar$ is smaller than $\Hsbar$.
%
In the high latitude climates, we have $L\Ebar$ greater than $\Hsbar$ whereas
$\innerp{LE'}{LE'}$ is typically smaller than $\innerp{H_s'}{H_s'}$.  Overall,
notice also that the anomaly Bowen ratios are closer to unity than the mean
Bowen ratios, consistently across the four datasets.
%
That is, for a given location, the turbulent heat fluxes of both moisture and
temperature are significant terms in the surface energy anomaly budget and, thus
both are potentially important contributers to surface temperature variability.

Next, we compare the precipitation flux to the evapotranspiration flux using
ratios of means $\Ebar\big/\Pbar$ and variances
$\innerp{E'}{E'}\big/\innerp{P'}{P'}$. 
%
From figure \ref{fig:comp_scale_E_P_mean}, mean evapotranspiration is found to
be greater than mean precipitation in some drylands locations. This suggest that
seasonal moisture storage is an important source of summertime mean-state
evapotranspiration.
%
In section \ref{ssec:bud_res}, however we showed that soil moisture memory is a
small term in the anomaly budget, a telling sign of the vastly different
behavior encountered in the mean and anomaly budget soil moisture equations.

Interestingly, from figure \ref{fig:comp_scale_E_P_anom}, we see that the
variance in evapotranspiration is less than a quarter of the variance in
precipitation in the mid-latitude wet, tropical and high-latitude climates.  
%
That is, evapotranspiration is a small term in the soil moisture anomaly budget
in forested regions, where trees can transfer water from bottom soil layers to
atmosphere through transpiration.  Recall that the budget residual is defined as
$\xim' \equiv P'{-}E'{-}R'$. Hence, the inconsistent definition of $E$ in the
surface-layer soil moisture budget only slightly affects the magnitude of
$\xim'$ as well as its interpretation.  
%
In drylands climates $\innerp{E'}{E'}$ is smaller than but comparable to
$\innerp{P'}{P'}$.  Transpiration originating from the bottom (lower than 10\cm)
soil layer is expected to be small in the lightly-vegetated dryland climate
zone.  Therefore, to good approximation, $E'$ can be consider as originating
from the surface soil layer in dryland climates.

\vfill

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_bowen_mean}

\caption[Summertime climatological mean Bowen ratio.]%
%
{Summertime ratio of the mean sensible heat flux to the mean latent heat flux,
(the climatological mean Bowen ratio) $\Hsbar/L\Ebar$.}

\label{fig:comp_bowen_mean}
\end{center}
\end{figure}
  
\begin{figure}[H]
\begin{center}


\includegraphics[width=\linewidth]{chap3/comp_bowen_anom}

\caption[Summertime anomaly Bowen ratio.]%
%
{Summertime ratio of the variance in sensible heat flux to the variance in
latent heat flux, (the anomaly Bowen ratio)
$\innerp{H_s'}{H_s'}\big/\innerp{LE'}{LE'}$.}

\label{fig:comp_bowen_anom}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_E_P_mean}

\caption[Summertime ratio of mean evapotranspiration to mean precipitation.]%
%
{Summertime ratio of mean evapotranspiration to mean precipitation rate,
$\Ebar/\Pbar$.}

\label{fig:comp_scale_E_P_mean}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap3/comp_scale_E_P_anom}

\caption[Summertime ratio of the evapotranspiration variance to the
precipitation variance.]
%
{Summertime ratio of the evapotranspiration variance to the precipitation rate
variance, $\innerp{E'}{E'}\big/\innerp{P'}{P'}$.}

\label{fig:comp_scale_E_P_anom}
\end{center}
\end{figure}

%\newpage

\subsection{The toy model equations}
\label{ssec:tm_assump}
%
% land_sfc_sys
%
% -------------------------------------------------------------------------------

%In section \ref{sec:tm_prelim}, we found that the surface-layer soil moisture
%content is characterized by pronounced correlations with evapotranspiration and
%autocorrelation properties comparable to surface temperature.
%%
%Therefore, we postulate that surface temperature variability can be reproduce by
%coupling the surface energy budget to the surface-layer soil moisture budget. 

In section \ref{ssec:bud_res}, we found that, on monthly time scales, the
storage terms ($\dervside{U'}{t}$ and $\dervside{m'}{t}$) in the anomaly budget
equations are small relative to the forcing ($F'$, $P'$) and budget residual
terms ($\xiU$, $\xim$).
% 
Hence, in developing the toy model, we neglect the storage terms resulting in
\emph{diagnostic} expressions for surface temperature and soil moisture
variability.

%assume that land-surface processes
%fluctuations can be adequately approximated using only forcing and state
%variable of the same sample (instantaneous) monthly average time entry. That is,
%the toy model expressions for surface temperature and soil moisture variability
%are \emph{diagnostics} on monthly time scales.

%As a further simplification of the set of land-atmosphere interactions
%potentially controlling surface temperature and soil moisture variability, 

We also assume that the surface energy and surface-layer moisture anomaly
budgets are \emph{forced by} the radiation forcing and precipitation ($F'$,
$P'$).
%
We assume that the latter are not affected by the variations in soil moisture
and surface temperature; that is, we neglect all potential feedbacks from land
to atmosphere.  In particular, soil moisture recycling and the impacts of soil
moisture on cloud cover are excluded from the toy model. 
%
Consequences of this assumption are revisited in section \ref{sec:tm_limits}.
%
Additionally, the albedo of the land-surface is function of the soil moisture.
That said, it can be shown that fluctuations in albedo, regardless of cause,
only slightly affect the magnitude of the radiation forcing in the four analyzed
datasets. Refer to appendix \ref{app:albedo} for a complete discussion.
%
The essential assumptions that comprise the toy model are as follows:

\begin{enumerate}

\item The fluxes of energy and moisture at the land-surface interface can be
adequately parameterized by the forcing functions and/or state variable
anomalies $F'$, $P'$, $T'$ and $m'$.
%
%Land-surface processes' anomalies can be adequately reproduce (\ie
%parameterized) as functions of forcing functions anomalies ($F'$,$P'$) and
%state variables anomalies ($T'$,$m'$) only.
%
\label{enu:assp_param}

\item Moisture below the 10\cm-deep surface soil layer does not affect the
monthly anomalies in surface temperature ($T'$) and surface-layer soil moisture
($m'$).
%
%Land-surface processes' anomalies are independent of soil moisture content
%below the surface soil layer (top 10\cm from land surface).
%
\label{enu:assp_sfc}

\item The energy and moisture storage terms are negligible compared to the
dominant terms in the respective anomaly budget residual ($\xiU$ and $\xim$).
%
%Surface temperature and surface-layer soil moisture anomalies on monthly
%time scales are governed exclusively by instantaneous processes.
%
\label{enu:assp_inst}

\item The external forcing functions ($F'$ and $P'$) are independent of
land-surface processes.
%
\label{enu:assp_forc}

\item Finally, following the analysis of section \ref{ssec:bud_res}, we set
$\xiU' \equiv G'$ and $\xim' \equiv I'$.
%
That is, both budget residuals are now considered land-surface processes: the
ground heat flux and vertical transport of moisture (infiltration) respectively. 

These five assumptions result in the \emph{toy model equations}:
%
\label{enu:tm_eqs}

\begin{subequations}
\begin{align}
  0 \;&=\; F' \;-\; L\pt E' \;-\; H_s' \;-\; \Fluu \;-\; G'
%  \enskip,
  \label{eq:tm_bud_engy} \\
  0 \;&=\; P' \;-\; E' \;-\; R' \;-\; I'
  \enskip.
  \label{eq:tm_bud_mois}
\end{align}
  \label{eq:tm_bud}
\end{subequations}

The geometry of the toy model is displayed schematically in
\ref{fig:land_sfc_sys}.

\end{enumerate}

%As a results of assumptions \ref{enu:assp_param}, \ref{enu:assp_inst} and
%\ref{enu:assp_forc} imply that, for a given location, the state variables'
%\emph{response} to the forcings is intended to be entirely determined by the
%forcings themselves. It is then useful to define a \emph{land-surface system} of
%vertical extent delimited by the top 10\cm (assumption \ref{enu:assp_sfc}) and
%the bottommost 2-meter of atmosphere.
%% 
%For reference, fluxes in and out of the land-surface system are illustrated in
%figure \ref{fig:land_sfc_sys}.

\vfill

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.9\linewidth]{land_sfc_sys}

\caption[The land-surface system for our toy model.]%
%
{The land-surface system. The state variables are the 2-meter air temperature
and the surface-layer (top 10\cm) soil moisture content, $T'$ and $m'$ (in
black). 
%
The forcing (in red) of the energy budget is $F'$ ($\equiv
\Fsw\down{-}\Fsw\up{+}\Flw\down$) and the forcing of the soil moisture budget is
precipitation $P'$. The fluxes of energy and moisture in and out of the
land-surface system are pictured in blue. They are evapotranspiration $E'$,
sensible heat $H_s'$, upwelling longwave radiation $\Fluu$, ground heat flux
$G'$, surface runoff $R'$ and infiltration of soil moisture $I'$.} 


\label{fig:land_sfc_sys}
\end{center}
\end{figure}

%\newpage
