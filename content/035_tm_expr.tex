%
% Chapter 3
%
% -------------------------------------------------------------------------------
\section{Toy model coefficients and expressions}
\label{sec:tm_expr}
%
%
%
% -------------------------------------------------------------------------------

\subsection{Toy model coefficients}
\label{ssec:tm_coeffs}
%
% no figs
%
% -------------------------------------------------------------------------------

To make the toy model expressions for surface temperature and soil moisture
variability more elegant, we combine selected projection coefficients to define
a set of \emph{toy model coefficients}\footnote{%
%
Not to be confused with the decomposition coefficient $\alpha$ (section
\ref{ssec:F_decomp}).}.
%
These same toy model coefficients prove useful in the interpretation of the toy
model of chapter \ref{chp:interpret}.  They are:

\begin{itemize}

\item $\nuE$\; [\s[-1]] : rate at which soil moisture anomalies induce
evapotranspiration anomalies.
%
The coefficient $\nuE$ is positive definite (see section \ref{ssec:param_E}) and
positive soil moisture anomalies are associated with positive evapotranspiration
anomalies.

\item $\nuHs$\; [\s[-1]] : rate at which soil moisture anomalies induce sensible
heat flux anomalies. 
%
The coefficient $\nuHs$ is positive definite (see section \ref{ssec:param_Hs})
and positive soil moisture anomalies are associated with negative sensible heat
flux anomalies (\ie surface warming \emph{ceteris paribus}).

The physical interpretation for $\nuHs$ is different over dry soils (where
$\innerp{E'}{m'}>0$) and wet soils (where $\innerp{E'}{m'}{<0}$). In both case,
however, $m'$ is the best predictor for $H_s'$ everywhere around the globe as
seen in section \ref{ssec:param_Hs}.  A full discussion on the interpretation of
sensible heat flux is presented in section \ref{ssec:Hs_interp}.

\item $\nus\equiv \big(\nuI + \nuE\big)$\; [\s[-1]]: rate at which soil moisture
anomalies seep away from the surface soil layer as a result of infiltration (if
$\nuI>0$) and evapotranspiration (recall $\nuE>0$ by definition, see section
\ref{ssec:param_E}). 

However, $\nus$ can potentially be negative. In this case, it represents the net
rate of moisture input into the surface soil layer when upward moisture
transports are greater than the evapotranspiration sink. That said, steady state
solutions where $\nuE{=}-|\nuI|$ must be discarded as they would induce
unbounded toy model expression for $m'$.

\item $\gamma \equiv \gammalw + \gammaHs + \gammaG$\; [$\W\m[-2]\K[-1]$] : net
surface temperature resistance to energy fluxes (via $\Fluu$, $H_s'$ and $G'$).
In other words, the inverse of the sensitivity of the land-surface.  We can
write $\gamma$ as $\gamma=C\subt{eff}\big/\tau$: an effective heat capacity
for the land-surface [$\J\K[-1]\m[-2]$] divided by a characteristic temperature
  anomaly time scale $\tau$. Spatial patterns and magnitudes of $\gamma$ are
  investigated in section \ref{ssec:coeffs_state}.

\item $\lambda$\; [unitless] : efficiency at which radiation forcing anomalies
induce evapotranspiration \newline anomalies (intact from equations
\ref{eq:param_E_dry} and \ref{eq:param_E_wet}). 

\item $ \beta\equiv \beta_R + \beta_I$\; [unitless] : fraction of precipitated
water anomaly that is evacuated to runoff and infiltration 
%
(and in the high latitudes, the fraction of precipitation that is stored in
snow).

\item $\chi\equiv (\nuE-\nuHs)\pt\big/\nus$\; [unitless] : impact of surface
soil moisture fluctuations on surface temperature fluctuations,
%
hereafter defined as the \emph{coupling coefficient} with symbol $\chi$.

Of the land-surface processes involved in the surface energy budget,
evapotranspiration and sensible heat flux are parameterized by soil moisture.
So, the coupling coefficient links soil moisture and turbulent fluxes to surface
temperature (more clearly seen in the following section \ref{ssec:tm_expr}).  
%
It is important to note that the coupling coefficient can be positive or
negative depending on the relative magnitude of the rate which soil moisture
induces cooling by evapotranspiration and sensible heat flux anomalies. In
particular, over wet soils (where $\nuE$ is set to 0) $\chi$ is negative (its
numerator is $-\nuHs$).

\end{itemize}

\noindent%
%
From the definition of $\chi$, the faster soil moisture seeps away from the
surface soil layer (\ie the larger $\nus$), the lesser its impact on $T'$. To
avoid avoid unphysical values, the coupling coefficient is set to zero over
soils characterized by  $|\nus| < (\text{30 days})\inv$.
% 
The restriction criterion is based on $\nus$ since its interpretation is more
physically meaningful than for $\chi$.
% 
In the datasets analyzed in this study, only a few grid points in the Sahara
desert\footnote{A few grid points in Alaska in the \ccsm were also affected with
minor consequence on the toy model.} were affected by the restriction criterion. 
%
Intuitively, in the Sahara, soil moisture is not expected to affect surface
temperature variability.
%
So, by setting $\chi$ to zero, soil moisture anomalies do not affect surface
temperature in the toy model.  In essence, we assume that surface-layer soil
moisture anomalies of characteristic time scales ($\sim\nus\inv$) greater than a
month are independent of surface temperatures.
%
We found that this assumption considerably reduces the toy model calibration
errors in the Sahara. 

\subsection{Toy model expressions}
\label{ssec:tm_expr}

After implementing the parameterization algorithms for the land-surface
processes in the anomaly budget equations,
%
%and collecting the projection coefficients into toy model coefficients
%
the toy model expressions for surface temperature and surface-layer soil
moisture anomalies are:

\begin{subequations}
\begin{align}
 T' \;&=\; 
 \frac{1}{\;\gamma\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda \pt \big)\, F_0' 
 \;-\; L\, \big( \pt \alpha (1 - \lambda) \pt \big)\, P'
 \;-\; L\, \big( \pt \nuE - \nuHs \pt \big)\, m'
 \, \Big]
\label{eq:tm_T_w_m} \\
%
 m' \;&=\; 
 \frac{1}{\nus}\,
 \Big[ \, 
 \big( \pt 1 + \alpha \lambda - \beta \pt \big)\, P'
 \;-\; \big( \pt \tfrac{\lambda}{L} \pt \big)\, F_0'
 \, \Big] 
 \enskip. \label{eq:tm_m} 
\end{align}
\end{subequations}

\noindent%
%
The toy model expression for $m'$ does not depend on surface air temperature
anomalies. Its interpretation is quite simple. Monthly soil moisture anomalies
are diagnosed as functions of the land-atmosphere forcing functions, three toy
model coefficients ($\nus$, $\beta$, $\lambda$) and one decomposition parameter
($\alpha$).
%
Positive precipitation anomaly ($P'>0$) increases the surface-layer soil
moisture content. 
%
A fraction $\beta$ of the precipitation runs off and/or is transported to the
soil layers below instantaneously.
%
%$(1+\alpha\lambda-\beta)$ represents the net effect on the surface-layer soil
%moisture content: \emph{(i)} a fraction $\beta P'$ runoffs and/or infiltrates
%instantaneously, \emph{(ii)} evapotranspiration is partly reduced in
%$\alpha\lambda P'$ through decreased radiation reaching the land-surface.  
%
Positive non-precipitation radiation anomalies ($F_0'>0$) increase the
evapotranspiration rate leading to reduced soil moisture content; similarly,
reduced radiation in precipitating clouds represented by $\alpha\lambda P'$
reduces evapotranspiration and thus increases soil moisture.
%
Soil moisture anomalies are damped/restored at a $\nus$ as moisture seeping away
from the surface layer by $E'$ and $I'$.

On the other hand, surface temperature anomalies do depend partly depend on soil
moisture anomalies. Soil moisture regulates $T'$ through 
%
its effect on evapotranspiration (cooling) and the sensible heat flux anomalies
(described by the coupling coefficient).  After substituting for $m'$ from
\eqref{eq:tm_m} into \eqref{eq:tm_T_w_m}, we obtain:

\begin{equation}
 T' \;=\; 
 \frac{1}{\;\gamma\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda (1 - \chi) \pt \big)\, F_0' 
 \;-\; L\, \big( \pt 
 \alpha (1 - \lambda) + \chi (1 + \alpha\lambda - \beta) 
 \pt \big)\, P'
 \, \Big]
 \enskip. \label{eq:tm_T}
\end{equation}

Monthly surface air temperature anomalies are diagnosed as functions of two
land-atmosphere forcing functions, four toy model coefficients and one
decomposition parameter.
%
From equation \eqref{eq:tm_T}, given positive forcing anomalies $F_0'>0$ and
$P'>0$, term-by-term surface air temperatures are subject to:

\begin{itemize}

\item $F_0'$\;: Warming caused by non-precipitating forcing anomalies (\eg
decreased cloudiness),

\item $-\lambda F_0'$\;: Cooling caused by increased evapotranspiration in
energy-limited situations.

\item $-L\alpha P'$\;: Cooling caused by reduced shortwave radiation at the
surface due to precipitating clouds.

\item $L\alpha\lambda P'$\;: Warming caused by reduced evapotranspiration in
energy-limited situations.

\item $-L\chi(1+\alpha\lambda-\beta)P'$\;: Cooling (if $\chi{>}0$) or warming
(if $\chi{<}0$) caused by turbulent flux ($E'{+}H_s'$) adjustments in the
presence of a positive soil moisture anomaly initiated by a precipitation
anomaly.

\item $\lambda\chi F_0'$\;: Warming (if $\chi{>}0$) or cooling (if
$\chi{<}0$) caused by an adjustment of the turbulent fluxes ($E'{+}H_s'$) in the
presence of a positive soil moisture anomaly initiated by a non-precipitating
radiation anomaly.

\end{itemize}

In the absence of land-surface processes, positive non-precipitating radiation
forcing anomalies would warm the surface whereas the increased cloudiness during
precipitating anomalies would cool it.
%
We then remark that depending on the sign of the coupling coefficient $\chi$,
soil moisture anomalies can either amplify or damp a given radiation forcing
anomaly.
%
This behavior will be investigated in more details in chapter
\ref{chp:interpret}.

Squaring both sides of \eqref{eq:tm_T} and \eqref{eq:tm_m} and averaging across
the sample space yields the toy model expression for surface air temperature and
surface-layer soil moisture variance:

\begin{subequations}
\begin{align}
 \Var(T) \;&=\; 
 \frac{1}{\;\gamma^2\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda (1 - \chi) \pt \big)^2\, \Var(F_0)
 \;+\; L^2\, \big( \pt 
 \alpha (1 - \lambda) + \chi (1 + \alpha\lambda - \beta)
 \pt \big)^2\, \Var(P)
 \, \Big]
\label{eq:tm_Var_T} \\
%
 \Var(m) \;&=\; 
 \frac{1}{\nus^2}\,
 \Big[ \, 
 \big( \pt 1 + \alpha \lambda - \beta \pt \big)^2\, \Var(P)
 \;+\; \big( \pt \tfrac{\lambda}{L} \pt \big)^2\, \Var(F_0)
 \, \Big] 
 \enskip. \label{eq:tm_Var_m} 
\end{align}
\end{subequations}


