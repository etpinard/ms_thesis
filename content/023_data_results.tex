%
% Chapter 2
%
% -------------------------------------------------------------------------------
\section{Dataset analysis}
\label{sec:data_results}
%
% - add line to introduce 2.3
% - mention appendix A at end of 2.3.3 ?
%
% - make table / display equation about Corr(E,{F,m}) ??
% - elaborate more on decomposition (koster09 wrote some stuff) 
% - define the standard deviation ? Var(X)^1/2 ?
%
% -------------------------------------------------------------------------------

\subsection{Inter-annual variability, important metrics and the error ratio}
\label{ssec:metrics}
%
% (no figs)
%
% -------------------------------------------------------------------------------

We define \emph{anomalies} or as \emph{fluctuations} about monthly climatological
means. Note that the terms \emph{anomaly} and \emph{fluctuation} are used
interchangeably in this document.  For a given monthly average variable $X$, we
set:

\begin{equation}
  X_{i,j}' \;\equiv\; X_{i,j} \;-\; \Xbar_j
  \enskip,
  \label{eq:anom}
\end{equation}

\noindent%
%
where $j{=}1,2,3$ is the summer month index (JJA in the Northern Hemisphere and
DJF in the Southern Hemisphere), $i{=}1,2,\ldots,30$ is the year index,
$\Xbar_j$ are the monthly climatological means and $X_{i,j}'$ are the monthly
anomalies (or equivalently monthly fluctuations).  By decomposing with respect
to monthly means instead of seasonal means, where $\Xbar$ would be constant
throughout the sample, we remove variations associated with the climatological
annual cycle.
%
In addition, we choose to not detrend the variables to simplify the analysis.
In doing so, note that the inter-annual variability is slightly overestimated in
the variables showing trends through the sample (\eg surface temperature in the
tropics). In appendix \ref{app:detrend}, we assess the impacts of the secular
trends on our quantification of variability.

We choose the (multi-year) monthly variance to describe the monthly anomaly
magnitude. It is defined as:

\begin{equation}
  \Var_j(X) \;\equiv\; 
  \frac{\innerp{X_{i,j}'}{X_{i,j}'}_j}{N\subt{year}{-}1}
  \;=\;
  \frac{1}{N\subt{year}{-}1} \sum_{i=1}^{N\subt{year}}\, X_{i,j}'\pt X_{i,j}' 
  \enskip,
  \label{eq:Var}
\end{equation}

\noindent%
%
where $N\subt{year}{=}30$ is the number of years in the sample
($N\subt{year}{=}20$ for \ncep data).  The sample variance is an unbiased
estimator of the true or population variance.
%
The monthly variance in equation \ref{eq:Var} is defined for each of the three
summer months.  To present a summer-wide description of the inter-annual
variability in a given variable, we compute the summer average monthly variance
which is simply defined as the arithmetic mean of $\Var_j(X)$ of the three
summer months ($j{=}1,2,3$). One can show that the summer average monthly
variance is always less than the variance about a lone three-month seasonal
mean.
%
Concerned with the appropriate metric to use for inter-annual variability,
\citet{vidale07} remarked that non-parametric based estimators, such as the
inter-quartile range, are more robust than moment-based estimator such as the
variance. That said, due to its higher statistical efficiency in small samples,
\ie of 20 to 30 years, the variance is more reliable measure of inter-annual
variability and is used exclusively in this study.

The inner-product in equation \eqref{eq:Var} can be generalized to a pair of
variables, $X$ and $Y$ to define the (multi-year) monthly covariance:

\begin{equation}
  \Cov_j(X,Y) \;\equiv\;
  \frac{\innerp{X_{i,j}'}{Y_{i,j}'}_j}{N\subt{year}{-}1}   
  \;=\;
  \frac{1}{N\subt{year}{-}1} \sum_{i=1}^{N\subt{year}}\, X_{i,j}'\pt Y_{i,j}' 
  \enskip.
  \label{eq:Covar}
\end{equation}

\noindent%
%
Similarly, the (multi-year) monthly correlation coefficient is:

\begin{equation}
  \corr_j(X,Y) \;\equiv\;
  \frac{\Cov_j(X,Y)}{\Var_j(X)^\half \pt \Var_j(Y)^\half}
  \enskip.
  \label{eq:corr}
\end{equation}

\noindent%
%
Throughout this document, anomalies, variances and covariances are computed with
respect to monthly climatological means (see equation \ref{eq:anom}). 
%
So, take note that to lighten the text, we drop the adjective \emph{monthly} and
the subscripts $j$. 
%
In addition, the word \emph{summertime}  as an adjective designates that a
summer average was performed on a monthly field.
%
For example, \emph{summertime variance} designates the average of the variances
for the three summer months:

\begin{equation}
  \Var(X) \;=\; \frac{1}{3}\, \sum_{j=1}^3 \Var_j(X)
  \enskip.
  \label{eq:summer_avg}
\end{equation}

\noindent%
%
Refer to the glossary, for a complete list of expressions and abbreviations.

To quantify errors in a given positive metric of a variable in a given GCM or
reanalysis dataset ($X\subt{data}$), we use the \emph{summertime error ratio}
with respect to University of Delaware observations ($X\subt{obs}$). 
%
For example, for the variance in $X\subt{data}$, we write:

\begin{equation}
 \Err\big(\Var(X\subt{data})\big) \;\equiv\; 
 \frac{1}{3}\, \sum_{j=1}^3
 \frac{\Var_j(X\subt{data})}{\Var_j(X\subt{obs})}
 \enskip,
 \label{eq:Err}
\end{equation}

\noindent%
%
%where $\Err\big(\Var(X\subt{data})\big)$ is computed month-by-month.
%
An error ratio of unity designates that the dataset is in perfect accordance
with observations. If $\Err(X\subt{data})$ is less than one, the dataset
underestimates observations; if the error ratio is greater than one, the dataset
overestimates observations.

\subsection{Temperature variability errors and climatological soil moisture}
\label{ssec:T_var_err_and_m}
%
% comp_Var_T_bias
% comp_mbar
% compobs_ssn_Var_T
% comp_ssn_nrm_mbar
% comp_sig_m 
% comp_Tbar_bias
%
% -------------------------------------------------------------------------------

We first evaluate summertime surface temperature variability in the four
datasets.  Figure \ref{fig:comp_Var_T_bias} \\ presents the summer average
error ratio of surface temperature variance over continental regions. As in the
CMIP5 ensemble (figure \ref{fig:cmip5_Var_T_bias} in chapter \ref{chp:intro})
surface temperature variability suffers from large errors in the two GCMs
datasets.
%
Errors are found to be largest over drylands, where surface temperature
variability is greatly overestimated in both GCMs.  The \ccsm features
wide-spread areas where the summer surface temperature variance is four times
greater than observed (\ie $\Err(\Var(T))>$ 4). The \hadgem performs better than
the \ccsm overall, especially in regions characterized as the mid-latitudes wet
climates. Nevertheless, large positive biases are also present over drylands in
the \hadgem.  
% 
Large errors are also present over some tropical climates such as the Central
Africa, where $\Var(T)$ is underestimated in both GCMs.  While there exist
locations of erroneous $\Var(T)$ in the both reanalysis products, to no
surprise, the reanalysis products are better than the two GCMs overall,
especially the \era.  

%\comment{Should I elaborate on these wet-climate errors? They have not been
%linked to $m$ previously? Some of these location are wetter in GCMs than in
%reanalyses.}
% Note that these errors have not been linked to soil moisture in previous 
% studies. (maybe) This comes down to effect of m' on T' (when setting m'=0
% in chap 4)!

As a first test of our hypothesis linking errors in summertime GCM surface
temperature variability to a misrepresentation of land-atmosphere interactions
through soil moisture, we examine the summer average soil moisture content in
the surface-layer (the top 10 \cm of soil from the surface) in figure
\ref{fig:comp_mbar}.  We first notice that the \ccsm is the driest of the
datasets analyzed through most the globe. 
%
In this particular GCM, wet mid-latitudes climates, which are well-simulated in
\hadgem, are depicted as drylands. 
%
As seen in figure \ref{fig:comp_Var_T_bias}, these dry-biased regions are also
the regions with greatest positive biases in surface surface temperature
variance in the \ccsm, consistent with our hypothesis.
%
Moreover, in dryland regions, both GCMs have smaller surface-layer soil moisture
contents than seen in the reanalysis products and positive bias in summer
temperature variance suggesting a link between GCM errors in surface temperature
variance and errors in summer mean soil moisture.

Many mid-latitude regions experience a large seasonal cycle in
\emph{full-column} soil moisture \citep{sene10}.  Moisture accumulates in soils
during the winter, appreciable melting occurs in spring followed by moisture
lost in summer through runoff and evaporation.
%
Maximum mid-latitude soil moisture content is commonly reached in late winter or
early springtime. Evapotranspiration peaks in the summertime leading to a soil
moisture minimum in late summer or early autumn.

If the GCMs' overestimation of summertime temperature variability in dryland
climates is indeed directly linked to low mean soil moisture content, then we
expect to observe an increase in $\Var(T)$ as summer progresses.
% 
Figure \ref{fig:compobs_ssn_Var_T} and \ref{fig:comp_ssn_nrm_mbar} show the
difference between second and first summer month (\eg July minus June in the
Northern Hemisphere) in surface temperature variance and normalized
surface-layer soil moisture content respectively. In the latter, the
normalization is made about the respective summer mean $m$.  
%
We first notice that, averaged across the analyzed 30-year sample, the
University of Delaware temperature record does not show any regions of large
increase in $\Var(T)$ over the course of the summer in dryland climates. 
% 
Parts of Australia and the southern banks of the North Sea in Europe show a
slight increase in $\Var(T)$ from summer month 1 to summer month 2.
%
Note also that some locations in the Northern Hemisphere high latitudes feature
a decrease intra-seasonal temperature variability possibly associated with
decreased synoptic scale variability through summer.  Overall, both reanalysis
products agree well with observations. 
%
Conversely, both GCMs feature a large increase in surface temperature variance
from June to July in the central eastern United States and Eastern Europe.
% 
This is a particular and important characteristic of the GCMs' $\Var(T)$ errors.
%
However, the locations that feature a seasonal $\Var(T)$ increase are not always
accompanied by drying in the surface soil layer (figure
\ref{fig:comp_ssn_nrm_mbar}) nor do they always correspond to locations that are
drier in the GCMs than in the reanalysis products.  (figure
\ref{fig:comp_mbar}). 
%
In the \hadgem, for example, parts of the central US and Eastern Europe do dry
considerably from the month June to July, but no so in the \ccsm.  Hence,
figures \ref{fig:compobs_ssn_Var_T} and \ref{fig:comp_ssn_nrm_mbar} suggest that
the erroneous seasonal increase in surface temperature variance can only be
partly or indirectly linked to an erroneous seasonal decrease in climatological
soil moisture.
%
Moreover, in the reanalysis products, the soil moisture content in the surface
layer remains almost constant from first to second summer month (hence the
normalization in figure \ref{fig:comp_ssn_nrm_mbar}).
%
\citet{li05} found that both the \ncep and \era replicate the seasonal cycle in
soil moisture in the top 100 \cm of soil reasonably well, suggesting that in
nature \emph{surface-layer} soil moisture is relatively constant throughout the
summer.

Soil moisture variability in, as well as below, the surface soil layer are also
important considerations in our understanding of the links between soil moisture
and land-atmosphere interactions. They are discussed in more details in chapter
\ref{chp:toy_model}.
%
For now, figure \ref{fig:comp_sig_m} shows the square root of the summertime
surface-layer soil moisture variance (\ie summer average then square root).  We
noticed that the two GCMs have a very different representation of the
variability in monthly average $m$. Moreover, we note that the two reanalysis
products agree well with each other everywhere except in desert climates.

We next examine mean surface temperature in summer. From figure
\ref{fig:comp_Tbar_bias}, summertime mean temperature in the central US is
overestimated by 2 to 3 \K.  As a consequence of the Clausius-Clapeyron
relation, increased evapotranspiration is expected in warmer surface conditions
\emph{ceteris paribus}; which may partly explain why the climatological soil
moisture decreases in summer in the GCMs compared to the reanalysis products
(which have little or no drying over the summer season).
%
It is important to note; however, that the errors in mean surface temperature
are not always collocated with errors in surface temperature variability (see
figure \ref{fig:comp_Var_T_bias}). 
%
Nonetheless the general pattern of errors in the mean summertime temperature is
further evidence for large errors in land-atmosphere interactions in dryland
climates in the GCMs.

%\vfill
%\newpage

%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Var_T_bias}

\caption[Summertime error ratio of surface temperature variance.]%
%
{Summertime error ratio (equation \ref{eq:Err}) of surface temperature
variance.}

\label{fig:comp_Var_T_bias}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_mbar}

\caption[Summertime mean surface-layer soil moisture content.]%
%
{Summertime mean surface-layer (\ie in the top 10$\cm$ of soil) soil moisture
content (units are $\mm$ of water).}

\label{fig:comp_mbar}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/compobs_ssn_Var_T}

\caption[Difference between the second summer month and the first summer month
for surface temperature variance.]%
%
{Difference between the second summer month (July in NH, January in SH) and the
first summer month (June in NH, December in SH) for surface temperature
variance.}

\label{fig:compobs_ssn_Var_T}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_ssn_nrm_mbar}

\caption[Normalized difference of second summer month and first summer month for
mean surface-layer soil moisture content.]%
%
{Normalized difference of second summer month and first summer month average
surface-layer soil moisture content. Normalization is made about the datasets'
respective summer mean surface-layer soil moisture.}

\label{fig:comp_ssn_nrm_mbar}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_sig_m}

\caption{Square root of summertime surface-layer soil moisture variance.}

\label{fig:comp_sig_m}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Tbar_bias}

\caption{Difference between summer average surface temperature in each dataset
and the observed summer average surface temperature.}

\label{fig:comp_Tbar_bias}
\end{center}
\end{figure}

%\newpage

\subsection{Precipitation and radiation forcing dataset inter-comparison}
\label{ssec:P_and_F}
%
% comp_Fbar
% comp_sig_F
% comp_Pbar_bias
% comp_Var_P_bias
%
% -------------------------------------------------------------------------------

It is well known that climate models have great difficulties in simulating
precipitation, on local or global scales \citep{ipcc_ar4_wg1}.  It follows then
to ask: could the errors in surface temperature variability in the GCMs be due
to errors in precipitation?

In figure \ref{fig:comp_Pbar_bias}, we see that the pattern of errors in
summertime mean precipitation does not correspond to the pattern of errors in
the surface temperature variance shown in figure \ref{fig:comp_Var_T_bias}. That
said, GCMs underestimate summertime mean precipitation over drylands which may,
in some locations, partly contribute to their soil moisture deficit discussed in
section \ref{ssec:T_var_err_and_m} and hence may possibly be linked to the GCMs'
excessive summertime surface temperature variability.
%
Alternatively, biases in mean precipitation over drylands may be a consequence
--- and not the cause --- of the soil moisture bias through decreased recycling
of soil moisture.  In this regard, the toy model will give us better insights on
how to interpret GCM errors in precipitation.
%
Turning to the two reanalysis products, from figure \ref{fig:comp_Pbar_bias}
notice that the \ncep dataset features a wide-spread overestimation of
summertime mean precipitation.  On the other hand, the \era performs well across
the globe with the exception of Australia and the Sahara desert where it
underestimates the observed precipitation.

Summertime precipitation variability error ratios for the four datasets are
shown in figure \ref{fig:comp_Var_P_bias}. In the two GCMs, they do not match
the error patterns for temperature variance (see figure
\ref{fig:comp_Var_T_bias} and \ref{fig:comp_Var_P_bias}). 
%
We also note that both reanalysis products feature large errors in summertime
precipitation variability. Errors are most pronounced and wide-spread in the
\ncep: almost all continental locations suffer from a very large overestimation
of the precipitation variability. 
%
In this regard, \citet{li05} commented, after an evaluation of the \ncep's
performance versus soil moisture observations, that this dataset's tendency to
generate prolonged soil moisture anomalies may artificially amplify its
precipitation variability. That is, the large errors in precipitation in the
\ncep reanalysis product may be due to a misrepresentation of the land surface
in that model.

Surface temperatures are, to zeroth-order, governed by the amount of radiation
reaching the surface. The summertime mean radiation forcing (defined as $F
\equiv \Fsw\down{-}\Fsw\up{+}\Flw\down$ as in equation \ref{eq:sfc_engy}) is
shown in figure \ref{fig:comp_Fbar} for each of the four datasets.
%
We note that both reanalysis products agree well on mean radiation forcing. The
four datasets show similar features, but there are notable differences. 
%
The most striking difference occurs in the high latitudes in the \ccsm where
$\Fbar$ is roughly 50 \Wm[-2] less than in the other three datasets.
%
The errors in mean radiation forcing collocate with the errors in mean
summertime surface temperature where the \ccsm suffers from a cold bias in the
high latitudes (see figure \ref{fig:comp_Tbar_bias}).
%

The summer average variability in radiation forcing, defined as the square root
of the summer average variance in $F$ ($\Var(F)^\half$), is presented in figure
\ref{fig:comp_sig_F}.
%
The \ccsm has considerable discrepancies: its $\Var(F)^\half$ is roughly 10
\Wm[-2] greater in the high latitudes than the other three datasets.
%
The \hadgem and the \era have very similar $\Var(F)$ patterns worldwide. 
%
As with precipitation, linking dataset biases in $\Var(F)$ to biases in soil
moisture is non-trivial because $m$ errors could potentially be both a cause or
a consequence of the errors in summer mean $F$ and $\Var(F)$.


%% figures
\afterpage{\clearpage}
%\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Pbar_bias}

\caption[Summertime error ratio of mean precipitation.]%
%
%{Summertime error ratio (equation \ref{eq:Err}) of mean precipitation.}
{The summertime ratio of the mean precipitation in each of the four datasets to
the observed mean precipitation, $\Pbar\subt{data}\big/\Pbar\subt{obs}$.}

\label{fig:comp_Pbar_bias}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Var_P_bias}

\caption{Summertime error ratio of precipitation variance.}

\label{fig:comp_Var_P_bias}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Fbar}

\caption[Summertime mean radiation forcing]%
%
{Summertime radiation forcing, $\Fbar$.}

\label{fig:comp_Fbar}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_sig_F}

\caption[Square root of summertime radiation forcing variance.]%
%
{Square root of summertime radiation forcing, $\Var(F)^\half$.}

\label{fig:comp_sig_F}
\end{center}
\end{figure}

%\newpage

\subsection{Insights from correlation patterns}
\label{ssec:data_corr}
%
% table: m_GM
% comp_Cor_ET_OvGM
% comp_Cor_EF_OVGM 
% comp_Cor_Em_OvGM  
% comp_Cor_EF0_OvGM
% plot_m_crit
% comp_Cor_mHs
%
% -------------------------------------------------------------------------------

Next, we examine correlation patterns between evapotranspiration and surface
temperature. Figure \ref{fig:comp_Cor_ET_OvGM} shows maps of the summer average
monthly correlation coefficients in the four datasets (see equation
\ref{eq:corr}). 
%
We notice that most dryland locations are characterized by negative
evapotranspiration surface temperature correlations (shorten to $E$-$T$
correlations in this section) whereas in tropical, mid-latitude wet and
high-latitude climates are characterized by positive $E$-$T$ correlations. This
robust dichotomy pattern in $\corr(E,T)$ highlights that latent cooling from $E$
can be overcompensated for under certain scenarios.
%
The four datasets disagree in desert climates. However, as evapotranspiration is
negligible in these regions (see figure \ref{fig:comp_bowen_mean} in section
\ref{ssec:scales}), this disagreement has little impact on subsequent analyses.
%
The solid green line in figure \ref{fig:comp_Cor_ET_OvGM} indicates where the
summer average surface-layer soil moisture is equal to the global mean
surface-layer sol moisture for the respective models.
%
The values are listed in table \ref{tab:m_GM} and are present in numerous
figures throughout this work. 
%
Consistently across all datasets, positive $E$-$T$ correlations are found in
regions that are wetter than the global mean (wet soils hereafter) and negative
$E$-$T$ correlations are found in regions that are dryer than the global mean
(dry soils hereafter). 
%
%Notable exceptions include eastern parts of the Nunavut territories in Canada
%in both GCMs (dry soils and $\corr(E,T)>0$) and mid-latitude wet climates in
%the \ncep (wet soils and weak correlations). 
% 
Note that overall $E$-$T$ correlations are weaker in the \ncep reanalysis
products and negative $E$-$T$ correlations have a greater areal extent in the
\ncep than in the other three datasets. These results are consistent with the
findings of \citet{jimenez11} who concluded that the \ncep's LSM performs poorly
in terms of the surface fluxes.

The dichotomy pattern in the $E$-$T$ correlations has been documented in past
studies.  First in \citet{sene06_coupling} and more completely in
\citet{sene10}, the authors linked positive $E$-$T$ correlations to, in their
words, an atmospheric control of evapotranspiration and negative $E$-$T$
correlations to soil moisture control of $E$.  
%
We caution that the positive $E$-$T$ correlations observed over wet soils are
not necessarily causal. Higher surface temperature does increase the evaporation
demand through the Clausius-Clapeyron relation \emph{ceteris paribus}; however,
surface temperature is positively correlated with radiation forcing (see figure
\ref{fig:hadgem1_Cor_land_atm_OvGM} in section \ref{sec:la_summary}), a presumed
driver of evapotranspiration.

Figure \ref{fig:comp_Cor_EF_OvGM} presents summertime correlation coefficients
between the radiation forcing $F$ and evapotranspiration, superposed here as
well with the global mean soil moisture contour (see table \ref{tab:m_GM}).
%
As with the $E$-$T$ correlations (figure \ref{fig:comp_Cor_ET_OvGM}), the global
mean soil moisture effectively delineates the regions of correlations of
different sign.  Over wet soils, $F$ is strongly positively correlated with
evapotranspiration; as expected in energy-limited locations.  Thus, in the four
datasets analyzed, we can associate wetter-than-global-mean soils to the
energy-limited evapotranspiration regime.  
%
On the other hand, $F$ is negatively correlated with $E$ over dry soils.  We
then ask: how can anomalously high radiation forcing --- a presumed driver for
evapotranspiration --- be associated with anomalously low $E$?
%
For hints, we investigate correlations between $E$ and surface-layer soil
moisture content.
%
In figure \ref{fig:comp_Cor_Em_OvGM}, we see that over dry soils,
evapotranspiration is strongly positively correlated with soil moisture
anomalies in the four datasets, which is the signature of the moisture-limited
evapotranspiration regime.  That said, it is important to notice that, over wet
soils, correlations of $E$ with surface-layer soil moisture are mainly negative. 
%
To see why this is the case, consider the following decomposition of the
radiation forcing anomalies $F'$:

\begin{equation}
  F' \;=\; F_0' \;-\; L\alpha \,P'
  \enskip.
  \label{eq:F_decomp}
\end{equation}

\noindent%
%
In equation \ref{eq:F_decomp}, $L$ is the latent heat of vaporization (a
constant as in equation \ref{eq:E_from_LH}), $P'$ is the precipitation anomaly
field and $\alpha$ is a unitless coefficient such that $L\alpha P'$ represents
the component of the radiation forcing fluctuations explained by (or parallel
to) precipitation anomalies.
%
In turn, $F_0'$ is the component of $F'$ orthogonal to $P'$; we call this the
\emph{non-precipitating radiation forcing anomalies}.
%
The coefficient $\alpha$ is positive for most of the world; that is,
precipitation anomalies are negatively correlated with radiation forcing
anomalies.
%
The decomposition of equation \ref{eq:F_decomp} proves to be vital for an
understanding of the effects of soil moisture on surface temperature. A detailed
discussion is provided in section \ref{sec:tm_method}. For now, we simply
investigate the correlation patterns between $E'$ and non-precipitating
radiation forcing anomalies $F_0'$. 

From figure \ref{fig:comp_Cor_EF0_OvGM}, non-precipitating radiation forcing
anomalies are almost orthogonal to $E'$ over dry soils. So, the negative $E$-$F$
correlations over dry soils, seen in figure \ref{fig:comp_Cor_EF_OvGM}, are a
result of radiation forcing anomalies associated with precipitation anomalies
(the $-L\alpha P'$ term in equation \ref{eq:F_decomp}). 
%
Positive precipitation anomalies induce positive soil moisture tendencies and
reduce the radiation forcing by decreasing the amount of the shortwave energy
reaching the surface.  Equivalently, anomalously low precipitation can lead to
anomalously high radiation forcing.  So, over dry soils, an anomalously high
radiation forcing is associated with a soil moisture deficit and reduced
evapotranspiration, as indicated by the negative $E$-$F$ correlation in figure
\ref{fig:comp_Cor_EF_OvGM}. 
%
Conversely, over wet soils, anomalously high soil moisture is associated with
anomalously low radiation forcing and thus reduced $E$: a negative $\corr(E,m)$
as illustrated in figure \ref{fig:comp_Cor_Em_OvGM}. Over wet soils,
$\corr(E,F_0)$ is positive as is $\corr(E,F)$ but of lesser magnitude suggesting
that the radiative effects of precipitating clouds are important factors for
controlling evapotranspiration fluctuations within the energy-limited regime.

The robust patterns highlighted in figures \ref{fig:comp_Cor_ET_OvGM},
\ref{fig:comp_Cor_EF_OvGM}, \ref{fig:comp_Cor_Em_OvGM} and
\ref{fig:comp_Cor_EF0_OvGM} suggests that evapotranspiration anomalies are
strongly controlled by either soil-moisture availability or radiation forcing
input, depending on the soil moisture content at a given location.
%
Moreover, the two predominant controls for $E$ do not overlap: over dry soils,
$E$ anomalies are due to soil moisture anomalies and are largely independent of
radiation forcing anomalies; over wet soils, $E$ anomalies are due to radiation
forcing anomalies and are largely independent of soil moisture anomalies. 

With regard to the Koster diagram (figure \ref{fig:koster}), our findings
suggest that the critical soil moisture level dividing the moisture-limited and
energy-limited $E$ regimes ($m\subt{crit}$ in figure \ref{fig:koster}) is
approximatively equal to the global mean soil moisture content (although this is
certainly a coincidence).
%
%It is important to note that, \emph{a priori}, the global mean soil moisture
%value is not a physical quantity.
%
Our findings (see table \ref{tab:m_GM}) show that $m\subt{crit}$ --- which is
thought to be determined mostly by biophysical processes --- is smaller in the
GCMs than in the reanalysis. 
%
Moreover, we note (\eg from figure \ref{fig:comp_Cor_ET_OvGM}) that the location
of the critical soil moisture level ($m\subt{crit}$) is dataset dependent.
%
To show this more clearly, assuming that the \era reanalysis is the best
estimate of the true surface-layer soil moisture content, we compute for the
three other datasets:

\begin{equation}
%
  \Big\{ 1 \,-\, \mcal{H} \big[
  (\pt \mbar_i - m\subt{crit,i} \pt) \times 
  (\pt \mbar\subt{ERA40} - m\subt{crit,ERA40} \pt) \big]
  \Big\} \;\times\;
  \sgn\big[ (\pt \mbar_i - m\subt{crit,i} \pt) \big]
  \enskip,
  \label{eq:m_crit}
%
\end{equation}

\noindent%
%
where $\mcal{H}(\,\cdot\,)$ is the Heaviside step function, $\sgn(\,\cdot\,)$ is
the sign function, $m\subt{crit,i}$ is the equal to the global mean
surface-layer soil moisture value for each of the three datasets other than the
\era (denoted by the index $i$) and $\mbar_i$ is the summertime mean soil
moisture for each of the three datasets other than the \era.
%
If expression \ref{eq:m_crit} yields a negative (positive) value for a given
location, we conclude that this location is misplaced (with respect to the \era)
in the moisture-limited (energy-limited) regime.
%
The results are shown in figure \ref{fig:plot_m_crit}. Note that only locations
where the \ccsm, \hadgem and \ncep have different evapotranspiration regime
compared to the \era are plotted in figure \ref{fig:plot_m_crit}.
%
We see that the moisture-limited evapotranspiration has greater areal extent in
Europe and in North America in both GCMs compared to the \era reanalysis.

Seeking an understanding of surface energy budget as a whole, we next examine
the sensible heat flux.  Figure \ref{fig:comp_Cor_mHs} shows the summertime
correlation coefficient of the sensible flux and surface-layer soil moisture.
Unlike the previous correlation patterns involving evapotranspiration, the
correlation between $H_s'$ and $m'$ has the same sign everywhere across all four
datasets, with the exception of the Sahara desert which features weak
correlations. In brief, the sensible heat flux does not partition into regimes
of variability based on mean surface-layer soil moisture content.
%
The link, let alone the negative correlations, between sensible heat flux
anomalies and soil moisture is non-trivial and poorly documented in past
literature. Is there really a causal, physically meaningful dependence between
soil moisture anomalies and sensible heat flux anomalies? The analysis conducted
in chapter \ref{chp:toy_model} and \ref{chp:interpret} suggests that indeed,
there is one. As we will show, understanding the behavior of the sensible heat
flux is crucial step toward a complete understanding land-atmosphere
interactions and their effect on temperature variability.

%\vfill
\newpage

%% figures & tables
\afterpage{\clearpage}
%\newpage

\vspace*{1em} 

\begin{table}[H]
\begin{center}

\begin{tabular}[t]{c|c|c|c}
	 \ccsm & \hadgem & \ncep & \era
  \\ \hline\hline
   19.4 & 20.5 & 28.5 & 23.5 
\end{tabular}

\end{center}

\caption%
%
[Global mean summertime surface-layer soil moisture content.]
%
{Global mean (not area weighed) summertime surface-layer soil moisture content
(in \mm of water) over all land points excluding Antarctica and latitudes
poleward of 77$^\circ$N.} 

\label{tab:m_GM}
\end{table}

\vspace*{6cm}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Cor_ET_OvGM}

\caption[Summertime correlation coefficient of evapotranspiration and surface
temperature.]%
%
{Summertime correlation coefficient of evapotranspiration and surface
temperature $\corr(E,T)$ (colored contours; see equation \ref{eq:corr}). 
%
Superposed is the contour line where summertime surface-layer soil moisture is
equal to the global mean surface-layer soil moisture for the respective dataset
(solid green line; see table \ref{tab:m_GM}).}

\label{fig:comp_Cor_ET_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Cor_EF_OvGM}

\caption[Summertime correlation coefficient of evapotranspiration and radiation
forcing.]%
%
{Summertime correlation coefficient of evapotranspiration and radiation forcing
$\corr(E,F)$ (colored contours).
%
Superposed is the contour line where summertime surface-layer soil moisture is
equal to the global mean surface-layer soil moisture for the respective dataset
(solid green line, see table \ref{tab:m_GM}).}

\label{fig:comp_Cor_EF_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Cor_Em_OvGM}

\caption[Summertime correlation coefficient of evapotranspiration and
surface-layer soil moisture.]%
%
{Summertime correlation coefficient of evapotranspiration and surface-layer soil
moisture $\corr(E,m)$ (colored contours).
%
Superposed is the contour line where summertime surface-layer soil moisture is
equal to the global mean surface-layer soil moisture for the respective dataset
(solid green line, see table \ref{tab:m_GM}).}

\label{fig:comp_Cor_Em_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Cor_EF0_OvGM}

\caption[Summertime correlation coefficient of evapotranspiration and \newline
non-precipitating radiation forcing.]%
%
{Summertime correlation coefficient of evapotranspiration and non-precipitating
radiation forcing $\corr(E,F_0)$ (colored contours).
%
Superposed is the contour line where summertime surface-layer soil moisture is
equal to the global mean surface-layer soil moisture for the respective dataset
(solid green line, see table \ref{tab:m_GM}).}

\label{fig:comp_Cor_EF0_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.45\linewidth]{chap2/plot_m_crit}

\caption[Critical soil moisture level check.]%
%
{Evaluation of the critical soil moisture level in the \ccsm, \hadgem and \ncep
with respect to the \era. 
%
Brown (green) contours indicates locations that are misplaced (with respect to
the \era) in the moisture-limited (energy-limited) regime.
%
This figure was generated using equation \ref{eq:m_crit} and by interpolating
\ccsm, \hadgem and \ncep data down to the resolution of the \era.  }

\label{fig:plot_m_crit}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap2/comp_Cor_mHs}

\caption[Summertime correlation coefficient of surface-layer soil moisture and
surface sensible heat flux.]%
%
{Summertime correlation coefficient of surface-layer soil moisture and surface
sensible heat flux $\corr(H_s,m)$.}

\label{fig:comp_Cor_mHs}
\end{center}
\end{figure}

%\newpage

