%
% Chapter 4
%
% -------------------------------------------------------------------------------
\section{Links between toy model coefficients and the mean state}
\label{sec:tm_mean_state}
%
% - a better name for 'gamma' than 'surface temperature resistance' ?
%
% - (maybe) compare beta nu_s mechanistically (kinda already did in 3.4)
% - chialt's 0 contour follow unity Bowen ratio line (not really actually)
%
% - more dataset-dependent stuff ??? NO!!
%
% -------------------------------------------------------------------------------

%% Intro to section

The toy model describes the variability in surface temperature $T'$ and
surface-layer soil moisture $m'$ as a function of forcing $F'$ and $P'$, while
parameterizing the fluxes of energy and moisture at the land surface in terms of
state variables ($T'$, $m'$) and forcings ($F'$, $P'$) only. 
%
The toy model expressions for surface temperature and surface-layer soil
moisture anomalies do not feature any explicit dependences on mean-state
variables (see equations \ref{eq:tm_T} and \ref{eq:tm_m}).
%
%Instead, the toy model coefficients modulate the contributions of two forcing
%functions.  For example, in section \ref{sec:tm_expr}, we remarked that the
%sign of the coupling coefficient $\chi$ determines whether soil moisture damps
%or amplifies temperature variability.
%
%That is, the spatial and dependence properties of toy model coefficients are
%worth investigating.
%
To understand how variability in surface temperature and soil moisture is
related to the climatological mean state, in this section, we attempt to
establish links between toy model coefficients --- which reflect the relative
importance of the land-surface processes that effect $T'$ and $m'$ --- and the
climatological mean state variables (in section \ref{ssec:coeffs_state}) and
forcings (in section \ref{ssec:coeffs_forc}).

To not unnecessary lengthen the document, only results derived from \hadgem data
are presented in sections \ref{ssec:coeffs_state}, \ref{ssec:coeffs_forc} and
\ref{ssec:coeffs_alt}. In these sections, the results presented are consistent
across the four datasets. Refer to \FigFolder for all
figures.
%
Comments on dataset dependencies are included in section
\ref{ssec:coeffs_datadep}.

\subsection{In state-variable space}
\label{ssec:coeffs_state}
%
% hadgem1_gamma , hadgem1_lambda , hadgem1_beta
% hadgem1_nus , hadgem1_chi    
%
%   insert as 2 separate multi-panel figures (tm_coeffs1, tm_coeffs2)
% -------------------------------------------------------------------------------

Figures \ref{fig:tm_coeffs1} and \ref{fig:tm_coeffs2} present summer average
values of the toy model coefficients as a function of summertime mean (surface)
temperature $\Tbar$ and (surface-layer) soil moisture $\mbar$.

%% gamma

The surface temperature resistance ($\gamma$) displays a marginal dependence on
$\Tbar$.  Overall, warmer climates are more resistant to energy inputs. The
Sahara desert is an exception however.  There is little noticeable dependence on
$\mbar$ overall.
%
Using the effective heat capacity of the land surface
($C\subt{eff}{=}1.37{\times}10^6\J\K[-1]\m[-2]$ as computed in section
\ref{ssec:bud_res}), the characteristic temperature anomaly time scales
($\tau{=}C\subt{eff}\big/\gamma$) range from 0.5 to 3 days.  They are much
smaller than one month; temperature responses rapidly to forcing anomalies. This
is consistent with the scalings of section \ref{ssec:bud_res} where we found
that surface energy storage is negligible on monthly time scales.
%
Analyzing the three components of the surface temperature resistance
($\gammalw$, $\gammaHs$, $\gammaG$), the resistance associated with the sensible
heat damping ($\gammaHs$) is responsible for the spatial patterns observed in
figure \ref{fig:tm_coeffs1}. Warm, non-desert soils are more susceptible to
large sensible heat flux anomalies.  Theses results are shown in section
\ref{ssec:coeffs_alt}.

%% lambda

The evapotranspiration efficiency $(\lambda)$ is a strong function of mean-state
soil moisture. For a given radiation forcing anomaly, more evapotranspiration is
generated in wetter climates.  The largest evapotranspiration efficiencies are
found in warm, wet climates.  
%
For $\mbar>20\mm$, $\lambda$ increases as $\Tbar$ increases (see figure
\ref{fig:tm_coeffs1}).
%
Recall that over dry soils $E$ anomalies are driven by soil moisture; radiation
forcing has little impact on $E$ and $\lambda$ is small.
%
The $\lambda{=}0.2$ values follows approximatively the global mean soil moisture
($\mbar{=}20.5\mm$ for the \hadgem) value which is the boundary between dry and
wet soils (or moisture-limited and energy-limited evapotranspiration regimes,
see section \ref{ssec:data_corr}).
%
Hence, dry and wet soils feature very different evapotranspiration efficiencies.

%% beta

The fraction of precipitation anomalies evacuated by runoff and infiltration
$(\beta)$ is largest in the high latitudes. Values of $\beta$ greater than one
can be found in some high latitude locations. They are associated snow melt
runoff covarying with precipitation anomalies (see section
\ref{ssec:param_xim}).
%
As a result, $\beta$ shows a strong dependence on $\Tbar$, where colder climates
a greater fraction precipitated water to runoff. 
%
Among the warm locations (\ie of $\Tbar> 295\K$), $\beta$ features a noticeable
mean-state soil moisture dependence, where dryer soils are associated with
smaller $\beta$ values.
% 
That said, we caution against linking spatial patterns in $\beta$ to mean-state
(or forcing) variables: the proximity to rivers, water basins and frozen soils
might be the main reasons behind the pattern seen in figure
\ref{fig:tm_coeffs1}.

%% nus 

Figure \ref{fig:tm_coeffs2} shows that the rate at which soil moisture anomalies
seep away from the surface soil layer ($\nus$, the seeping rate for short) is
largest in tropical and mid-latitude wet climates. In these regions, the typical
time scale for soil moisture anomalies to seep from surface layer is about one
day or less.
%
The seeping rate shows a pronounced mean-state soil moisture dependence:
increasing $\mbar$ indicates faster seeping rates.  Additionally, within the wet
climates ($\mbar>20\mm$), a dependence on $\Tbar$ is observed: higher mean-state
temperatures are associated with faster seeping rates.
%
Mechanistically, vegetated areas of the tropical and mid-latitude wet climates
have more porous soils in comparison to other climate zones
\citep{sene06_memory}.  Hence, they are more susceptible to larger soil moisture
seeping rates via more pronounced infiltration rates.
%
%(as $\nuE{=}0$ by construction in these climates zones).

%% chi

The coupling coefficient ($\chi\equiv(\nuE{-}\nuHs)\big/\nus$) is negative over
wet soils (since $\nuE{=}0$  from the $\innerp{E'}{m'}$ definition of wet soils,
see section \ref{ssec:tm_coeffs}).
%
In figure \ref{fig:tm_coeffs2}, we notice that $\chi$ is positive over most dry
soil locations (here with respect to the global mean soil moisture
$\mbar>20.5\mm$). Exceptions include: a few grid points in the Middle East and
on the Arctic Coast where $\chi>0$ and $\mbar<20.5\mm$.
%
Hence, overall, the sign of $\chi$ also efficiently delineates wet soils from
dry soils.  That is, \emph{the soil moisture anomalies have a damping effect on
temperature over wet soils and an amplifying effect over dry soils}. 
%
This link between mean-state soil moisture and the net effect of soil moisture
anomalies on temperature is one of the major results in this study. It is
revisited more thoroughly in section \ref{sec:two_regimes} and put in context in
section \ref{ssec:coeffs_alt}.
% 
In addition, take note that the coupling coefficient is small in magnitude over
tropical and mid-latitude wet climates. The large seeping rates found in these
regions make the effects of soil moisture anomalies on temperature variability
negligible. 

%% figures 
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/hadgem1_gamma}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/hadgem1_lambda}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/hadgem1_beta}}

\caption[Summertime toy model coefficients as functions of summertime mean
surface temperature and soil moisture (1 of 2).]%
%
{Summertime toy model coefficients $\gamma$ (panel a), $\lambda$ (panel b) and
$\beta$ (panel c) in $\mbar$-$\Tbar$ space and on a world map.  [\hadgem data].}

\label{fig:tm_coeffs1}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/hadgem1_nu_s}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/hadgem1_chi}}

\caption[Summertime toy model coefficients as functions of summertime mean
temperature and soil moisture (2 of 2).]%
%
{Summertime toy model coefficients $\nus$ (panel a) and $\chi$ (panel b) in
$\mbar$-$\Tbar$ space and on a world map. [\hadgem data].}

\label{fig:tm_coeffs2}
\end{center}
\end{figure}

\subsection{In forcing-function space}
\label{ssec:coeffs_forc}
%
% hadgem1_sc_FP_gamma , hadgem1_scFP_lambda_beta_nus_chi
%
% -------------------------------------------------------------------------------

Figure \ref{fig:tm_coeffs_FP} presents the summer average values of the toy
model coefficients as a function of the summertime mean radiation forcing
$\Fbar$ and precipitation rate $\Pbar$.
%
Overall, the dependence of the toy model coefficients on mean-state temperature
is matched by the dependence on $\Fbar$: $\gamma$ is largest in regions of large
$\Fbar$ and $\beta$ is largest in regions of small $\Fbar$.
%
That said, the space spanned by the summertime mean forcing functions is more
limited than that of the state variables; there are only a few locations of
small $\Fbar$ and large $\Pbar$.
%
Since the mean-state surface temperature is controlled by radiation forcing to
first order, the map from $\Tbar$ to $\Fbar$ is close to being isomorphic (not
shown). 
%
However, the wet soils of the high latitude climates and the mid-latitude wet
climates are partly caused by inter-seasonal water storage. Furthermore, melting
snow and frozen soil water are significant sources of soil moisture; hence, the
map from $\mbar$-space to $\Pbar$-space is not isomorphic. 
% 
%Therefore, the mean-state moisture dependencies remarked in figures
%\ref{fig:tm_coeffs1} and \ref{fig:tm_coeffs2} of $\lambda$, $\chi$ and $\nus$
%have both a $\Tbar$ and an $\Fbar$ component in figure \ref{fig:tm_coeffs_FP}.
%
As a result, the toy model coefficients are better delineated using the
mean-state moisture ($\mbar$) --- regardless of source --- than by using the
mean-state precipitation ($\Pbar$).

For applications of the toy model to problems such as the attribution of the
surface temperature variability errors in GCMs or improved deductions about the
climate of \twentyfirst century, the established links between toy model
coefficients and the climatological mean state variables are essential.
%
In particular, the coupling coefficient ($\chi$) and the evapotranspiration
efficiency ($\lambda$) are intricately linked to mean-state moisture which
differs between datasets. 
% 
Additionally, both $\chi$ and $\lambda$ feature a pronounced change of behavior
across the wet/dry soil boundary (\eg soil moisture anomalies are associated
with temperature anomalies of the same sign over dry soils and of opposite sign
over wet soils).
%
Might the temperature variance errors in the GCMs be linked to an erroneous
placement of the boundary between wet and dry regions compared to nature?
%
We start to examine this question in section \ref{ssec:coeffs_datadep} and a
full investigation is included in chapter \ref{chp:apps}.

\vfill

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.5\linewidth]{chap4/hadgem1_sc_FP_gamma}}
\vskip1em
\sidesubfloat[]{%
\includegraphics[width=0.8\linewidth]{chap4/hadgem1_scFP_lambda_beta_nus_chi}}

\caption[Summertime toy model coefficients as function summertime mean radiation
forcing and precipitation.]
%
{Summertime toy model coefficients $\gamma$ (panel a), $\lambda$, $\beta$,
$\nus$ and $\chi$ (panel b) in $\Pbar$-$\Fbar$ space. [\hadgem data].}

\label{fig:tm_coeffs_FP}
\end{center}
\end{figure}

\subsection{Comments on the alternative formulation}
\label{ssec:coeffs_alt}
%
% hadgem1_gamma_tilde
% hadgem1_chi_tilde
% hadgem1_eta       (maybe just state it, it is uninteresting)
%
% fig:tm_coeffs_alt
%
% -------------------------------------------------------------------------------

In section \ref{ssec:param_Hs}, we concluded that parameterizing sensible heat
flux anomalies using sequential projections onto temperature anomalies then soil
moisture anomalies was the most appropriate parameterization algorithm for the
toy model. 
%
That said, the $m'$-then-$F'$ algorithm is able to explain more $H_s$ variance.
We set $H_s'=-L\nuHsalt m'{+}\eta F'$ and recall that $\nuHsalt{\neq}\nuHs$. In
a similar fashion to section \ref{ssec:tm_coeffs}, we define the
\emph{alternative} toy model coefficients: 

\begin{equation}
  \gammaalt \;\equiv\; \gammalw + \gammaG 
  \quad,\quad
  \chialt \;\equiv\; \frac{\nuE-\nuHsalt}{\nus} 
  \enskip.
  \label{eq:tm_coeffs_alt}
\end{equation}

\noindent%
%
The above coefficients are interpreted the same way as $\gamma$ and $\chi$:
$\gammaalt$ is the surface temperature resistance and $\chialt$ represents the
net effect of soil moisture on temperature fluctuations. The unitless
coefficient $\eta$ is the efficiency at which radiation forcing anomalies induce
sensible heat anomalies. It is analogous to $\lambda$ introduced in the $E'$
parameterization algorithm.
%
The alternative toy model expression for $T'$ is:

\begin{equation}
 T' \;=\; 
 \frac{1}{\;\gammaalt\;}\,
 \Big[ \, 
 \big( \pt 1 - \lambda (1 - \chialt) - \eta \pt \big)\, F_0' 
 \;-\; L\, \big( \pt 
 \alpha (1 - \lambda - \eta) + \chialt (1 + \alpha\lambda - \beta) 
 \pt \big)\, P'
 \, \Big]
 \enskip. 
 \label{eq:tm_T_alt}
\end{equation}

\noindent%
%
Figure \ref{fig:tm_coeffs_alt} presents the summer average alternative
coefficients $\gammaalt$, $\chialt$ and $\eta$ on world maps and color-coded
scatter plots.
%
We notice that $\chialt$ is significantly smaller than $\chi$ over dry soils.
The alternative formulation does not feature any locations where the coupling
coefficient is greater than 0.6.
%
The magnitude of $\chialt$ is similar to $\chi$ in the high latitudes and in
tropical climates where the coupling coefficient is strongly modulated by the
soil moisture seeping rate $\nus$.

Importantly, we note that the zero contour of $\chi$ (see figure
\ref{fig:tm_coeffs2}) does not collocate with the zero contour of $\chialt$.
Parameterizing $H_s'$ with the $m'$-then-$F'$ algorithm makes $\nuHsalt$ greater
than $\nuHs$ over some dry soil locations near the wet/dry boundary
($\mbar{=}20\mm$ in the \hadgem), yielding a negative $\chialt$.
%
That is, positive soil moisture anomalies generate greater positive sensible
heat flux anomalies than negative latent heat flux anomalies leading to a net
warming by the net turbulent heat flux which, in turn, dampens the effects of
the forcing on surface temperatures.
%
%sensible heat flux anomalies respond more vigorously to soil moisture anomalies
%than $E$ anomalies producing a net warming effect given a positive
%precipitation anomaly, damping the effect of the forcing on surface
%temperatures.
%
This marks an important caveat in the proposed link between the sign of the
coupling coefficient and the net effect of moisture on surface temperature
fluctuations.
%
By parameterizing $H_s'$ with temperature anomalies first, we include parts of
the variability associated with $m'$ --- and all other variables covarying with
$T'$ --- in $\gammaHs$ and hence in $\gamma$.  On the other hand, in the
alternative formulation, by parameterizing $H_s'$ with soil moisture anomalies
first, we include parts of the variability associated with all variables
covarying with $m'$ in $\nuHsalt$ and hence in $\chialt$.
% 
That is, the temperature control on sensible heat flux anomalies is
overestimated in $\gamma$ and the soil moisture control on $H_s'$ is
underestimated in $\nuHs$.
%
Correspondingly, over dry soils where the soil moisture controls on $E'$ and
$H_s'$ compete, the net effect of soil moisture on surface temperature
fluctuations is overestimated (too much amplification) in $\chi$ and
underestimated (too little amplification or even some damping) in $\chialt$.
%
The projection ordering in the $H_s'$ parameterization has large implications in
our interpretation of the role of soil moisture in the toy model.
%
This issue is revisited in section \ref{ssec:role_of_m}.
%
For now, we note that $\chialt$ exhibits a mean-state soil moisture dependence
similar to that for $\chi$: dry soils are associated with larger coupling
coefficients than wet soils.
%
Moreover, the coefficient $\eta$ is small in comparison to $\lambda$, its
analogue in the $E'$ parametrization, and features little dependence on
mean-state variables.
%
%Moreover, $\gammaalt$ is smaller than $\gamma$ everywhere and almost constant
%throughout the globe. Recall the heat diffusion through ground is small term in
%the anomaly surface energy budget. So, we conclude that surface temperature
%resistance associated with upwelling longwave radiation has little spatial
%variations and mean-state dependencies.

\vfill

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.85\linewidth]{chap4/hadgem1_gamma_tilde}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.85\linewidth]{chap4/hadgem1_chi_tilde}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.85\linewidth]{chap4/hadgem1_eta}}

\caption[Summertime  alternative toy model coefficients as function of
summertime mean temperature and soil moisture.]%
%
{Summertime alternative toy model coefficients $\gammaalt$ (panel a), $\chialt$
(panel b) and $\eta$ (panel c) in $\mbar$-$\Tbar$ space and on a world map.
[\hadgem data].}

\label{fig:tm_coeffs_alt}
\end{center}
\end{figure}

\subsection{Comments on dataset dependencies}
\label{ssec:coeffs_datadep}
%
% compsc_lambda , compsc_chi
%
% fig:compsc_lambda_chi
% 
% -------------------------------------------------------------------------------

Figure \ref{fig:compsc_lambda_chi} shows an inter-dataset comparison of the
evapotranspiration efficiency and coupling coefficient in state-variable space.
%
Similar comparison plots of the other toy model coefficients are available in
\FigFolder.
%
%Recall that the \ccsm features few locations of the mid-latitude wet climate
%zone. Consistent with a link between $\lambda$ and mean-state soil moisture,
%the evapotranspiration efficiency, overall, is weak in \ccsm.  
%
As noted in section \ref{ssec:coeffs_state}, the $\lambda{=}0.2$ contours follow
wet/dry boundary in every dataset (see figure \ref{fig:compsc_lambda_chi}).
%
%Being the driest dataset in the global mean, the \ccsm has the most positive
%locations of positive $\chi$. 
%
%Correspondingly, the two reanalysis products feature more locations of negative
%$\chi$.  % This is further evidence for a link between $\chi$ and mean-state
%soil moisture content.
%
In addition, figure \ref{fig:compsc_lambda_chi} shows that $\chi$ (and also
$\chialt$, not shown) is larger in magnitude over dry soils in the GCMs than in
the two reanalysis products.
%
This strongly suggests that the amplifying effects of soil moisture on surface
temperature fluctuations over dry soils is too large in the two GCMs analyzed
compared to the two reanalysis products.
%
In brief, the toy model coefficients $\lambda$ and $\chi$ have different
characteristics over wet and dry soils.
%
Since the regions of wet and dry soils differ in the four datasets examined in
this study, the toy model can be used to describe the source of the
inter-dataset differences on geographical placement of the wet/dry soil boundary
discussed in section \ref{sec:data_results} (see figure \ref{fig:plot_m_crit}).

%% show these plots???
%Moreover, $\lambda$ is much larger in \ccsm in tropics and much larger in \ncep
%in high latitudes. 
%
%$\beta$ in the \ccsm is much smaller ($<0.1$) over dry soils than three other
%datasets.
%
%$\nus$ in the \ccsm is much larger in the mid-latitudes (because of larger
%$\nuE$ 
%
%$\chi$ (and $\chialt$) is more negative in \ccsm and \ncep over Arctic regions
%(link to snow melt???)

\vfill
\newpage

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.95\linewidth]{chap4/compsc_lambda}}
\vskip1em
\sidesubfloat[]{%
\includegraphics[width=0.95\linewidth]{chap4/compsc_chi}}

\caption[Inter-dataset comparison of summertime  toy model coefficients as
function of summertime surface temperature and soil moisture.]%
%
{Inter-dataset comparison of summertime toy model coefficients (panel a)
$\lambda$ and (panel b) $\chi$ in $\mbar$-$\Tbar$ space.}

\label{fig:compsc_lambda_chi}
\end{center}
\end{figure}

