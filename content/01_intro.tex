%
% Chapter 1
%
% -------------------------------------------------------------------------------
\chapter{Introduction}
\label{chp:intro}
%
% - 2nd sentence could be better. You are not quantifying Var(T) really.
% - jaeger11 reference could be better.
% - reformulate hypothesis, method ! Make clearer!
%
%
% - add more references (bookmarks on cyclone?)
% - include the sene10 diagrams, IPCC soil moisture projections?
% - mention that soil moisture projection diverge a lot?
% - mention Nemani paper of plant growth controls?
%
% -------------------------------------------------------------------------------

%% Q: Why would we be interested in studying summertime surface temperature

Surface temperature variability is an important characteristic of the climate
system. 
%
Extreme surface temperature events, such as heat waves and droughts, are capable
of inflicting severe health and economical stress on society.
%
Hence, quantifying the magnitude of typical deviations around climatological
mean temperatures is critical for assessing climate impacts.
%
Events such as the 2003 European heat wave, the 2010 Russian wild fires, the
2012 central United States drought serve as recent examples. 
% 
For instance, during the height of the 2003 European heat wave, France saw its
mortality rate increase by 54\% \citep{fischer07}.
%
Crop failures were also significant during the 2003 European heat wave: the
associated financial loss for Central and Eastern Europe is estimated at \$12.3
billion \citep{fischer07}.
%
In this regard, results from General Circulation Models (or climate models,
shorten to GCMs herein) simulations suggest that the recent summers of extreme
warmth may become the norm in the late \twentyfirst century.

In recent years, various studies have suggested that increased carbon dioxide
concentrations provoke a pronounced increase in inter-annual surface temperature
variability during summer.
%
From regional GCMs \twentyfirst century simulations, \citet{schar04} noted a
significant increase in surface temperature variability over large parts of
Europe. In some continental Europe locations, the summertime surface temperature
variance is projected to double by the end of the \twentyfirst century.  Their
findings were robust across several greenhouse gas (GHG) scenarios and GCMs. 
%
\citet{vidale07}  analyzed a similar set of simulations and reached similar
conclusions. These authors found that the regions of largest temperature
variability change do not collocate with regions of largest mean temperature
change.
%
\cite{weish05}, using a multi-GCM ensemble, found that extreme warm summers are
expected to be 50\% more frequent in Central North America at the end of the
\twentyfirst century than in the \twentyth century.

It is important to stress that in some of the aforementioned locations even a
slight increase in temperature variability can have severe effects on society.
%
For instance, in the tropics and sub-tropics crop-based models for major grains
in these regions show direct yield losses in the range of 2.5 to 16\% for every
1\degC increase in seasonal temperature \citep{battisti09}.
%
In Europe, society and agriculture production are adapted to a summer climate
with interannual variations of approximatively $1\K$, roughly three times
smaller than in the wintertime climate \citep{vidale07}.
%
Therefore, a thorough understanding of the projected trends in summertime
temperature variability is desirable.

% -------------------------------------------------------------------------------

%% Q: Why do IPCC GCMs project large increase in temperature variability (esp.
%%    in Europe)?

One of the goals of this study is to explore why GCMs project large increases in
surface temperature variability for the end of the \twentyfirst century.
%
Several publications have proposed that the interaction between land and
atmosphere lies at the root of these projected increases in surface temperature
variability, where \emph{land-atmosphere interaction} is defined as the set of
all processes that involve the land-surface interface including one-way
dependence relations and feedback loops \citep{sene10}.
%
The focus is primarily on summer as land-atmosphere interactions are more
pronounced in summer than in other seasons \citep{hirschi11}.

\citet{sene06_coupling} examined a set of four regional numerical model
simulations over Europe in an attempt to isolate the effects of land-atmosphere
interactions in summer.
%
They compared simulations with freely-evolving soil moisture content to runs
with prescribed constant (climatological mean) soil moisture in the late
\twentyth century and the late \twentyfirst century under increasing carbon
dioxide.
%
With or without interactive soil moisture, the \twentyfirst century runs feature
an increase in surface temperature variability over most of the domain. 
%
That said, they concluded that two-thirds of the increase in the standard
deviation of surface temperature was due to interactions involving variability
in soil moisture. 
%
\citet{sene06_coupling} also noted that the effects of soil moisture on surface
temperature variability in their simulations were more pronounced in the late
\twentyfirst century than in the late \twentyth century.

\citet{fischer07}, in a similar set of numerical experiments, found that soil
anomalies act to increase the variance in the surface turbulent fluxes and
temperature.
%
Finally, \citet{jaeger11} examined surface temperature variability in GCM
simulations, with and without interactive soil moisture (\ie soil moisture
anomalies), and concluded that soil moisture variability has a pronounced impact
on surface temperature variability on year-to-year time scales.

% -------------------------------------------------------------------------------

%% Q: Can GCM be trusted with regards to 21st century projection?

It is important to remember that all the aforementioned studies are based on
numerical model simulations.
%
Observational studies of land-atmosphere interactions are extremely limited
because observations of soil moisture and surface energy and moisture fluxes are
not available at the global scale \citep{sene10}.  
%
%In fact, in the study of land-atmosphere interactions numerical modeling using
%\emph{Land Surface Models} (or LSMs) is omnipresent due to a lack of
%observations of land-surface variables.
%%
%Ground-based observations of land-surface variables such as turbulent heat
%fluxes and soil moisture content are, to this day, unavailable at the global
%scale. 
%
While remote sensing methods of these processes and state variables exist, they
do not adequately constraint the budgets of surface energy and moisture for the
purposes of understanding the role of land-atmosphere interaction in relation
to surface temperature variability.

\newpage

We then ask: can a GCM be used to quantify the role of land-atmosphere
interactions for shaping summertime surface temperature variability?
%
Figure \ref{fig:cmip5_Var_T_bias} suggests that we should be cautious when
interpreting past studies.
%
Using an ensemble of 25 GCMs (see table \ref{tab:cmip5} for a complete list) of
the Coupled Model Inter-comparison Project Phase 5 (CMIP5), we find that the
typical CMIP5 GCM overestimates summertime surface temperature variance by 25\%
to 100\% over the 1969-1999 time period in most continental locations around the
globe; similar results are found for the CMIP3 GCMs (see figure
\ref{fig:cmip3_Var_T_bias} and table \ref{tab:cmip3}).
%
Note that figures \ref{fig:cmip5_Var_T_bias} and \ref{fig:cmip3_Var_T_bias} show
the ensemble mean of the summertime variances (and not the variance of the
ensemble mean).
%
In figures \ref{fig:cmip5_Var_T_bias} and \ref{fig:cmip3_Var_T_bias} as well as
throughout this document, summer is defined as June-July-August in the Northern
Hemisphere and December-January-February in Southern Hemisphere (see glossary
for definitions of important terms in this work).
%
The surface air temperature is taken to be the temperature 2 meters above
ground.  To lighten the text, the surface air temperature is referred to as
simply the \emph{surface temperature} with symbol $T$.
%
Observations of surface temperature are taken from the University of Delaware
database \citep{udel}.
%
The bias shown in figures \ref{fig:cmip5_Var_T_bias} and
\ref{fig:cmip3_Var_T_bias} are consistent with remarks made in \citet{vidale07},
\citet{weish05} and \citet{dirmeyer06}.

% cite IPCC AR4 about soil moisture projections (very diverging results)? 
% same conclusion in \citet{vidale07}.

% -------------------------------------------------------------------------------

%% Q: Why do GCM feature large temperature variability errors in 20th climate?

In this study, we propose that summertime surface temperature variability
simulated by the CMIP5 GCMs is biased high compared to observations due to an
improper representation of soil moisture and/or its impact on land-atmosphere
interactions.
%
It follows that the projected changes in surface temperature variance in the
late \twentyfirst century must also be biased. 
%
To test these hypotheses, we first develop a simplified theoretical framework of
land-atmosphere interactions. 
%
We make use of data of two Coupled Model Inter-comparison Project Phase 3
(CMIP3) GCMs, although we do not undertake numerical experiments with them.
%
Instead, we attempt to quantify the effects of land-surface interaction on
surface temperature variability using physical relationships.
%
For lack of a better name, we call the resulting framework the \emph{toy model
for summertime surface temperature variability}; or simply, the \emph{toy
  model}.

% -------------------------------------------------------------------------------

%% Q: How exactly are soil moisture and land-atmosphere interactions linked?

In essence, in developing the toy model, we investigate the links between soil
moisture, land-atmosphere interactions and summertime surface temperature
variability. 
% 
To first order, the land surface interface is subject to the \emph{surface
energy budget}:

\begin{equation}
  \derv{U}{t} \;=\; F \;-\; L\pt E \;-\; H_s \;-\; \Flu \;-\; G
  \enskip,
  \label{eq:sfc_engy}
\end{equation}

\noindent%
%
where $U$ is the surface energy in $\J\m[-2]$ (or enthalpy, more later) which is
proportional to the surface temperature $T$, $L$ is the latent heat of
vaporization for water, $E$ the surface evapotranspiration flux, $H_s$ the
surface sensible heat flux, $\Flu$ surface upwelling longwave radiative flux and
$G$ the ground heat flux.
% 
We define the \emph{radiation forcing} $F$ as sum of the net shortwave radiative
flux at the surface and the downwelling longwave radiative flux at surface;
symbolically $F \equiv \Fsw\down{-}\Fsw\up{+}\Flw\down$. 
%
From the perspective of the land surface, $F$ can be considered, to first order,
an \emph{external forcing} as it provokes changes onto the land surface while
the land surface has little effect on it. We revisit this assumption in chapters
\ref{chp:toy_model} and \ref{chp:interpret}.

We first turn our attention to the evapotranspiration flux (shorten to $E$).
Evapotranspiration includes all fluxes of moisture from the soil to the
atmosphere; it is accomplished by evaporation of soil moisture, transpiration
and evaporation from plant canopies. Over land, global estimates show that $E$
is comprised by 80\% transpiration and 20\% evaporation \citep{jasechko13}.
%
A useful qualitative framework for understanding evapotranspiration on monthly
to seasonal time scale was proposed in \citet{koster04}. 
%
An adapted\footnote{%
%
The diagrams in \citet{koster04}, \citet{koster09_droughts} and \citet{sene10}
are partitioned into three regimes: dry ($m$ below the wilting point),
transition ($m\subt{wilt}\leq m < m\subt{crit}$) and wet ($m>m\subt{crit}$). We
found the partitioning shown in figure \ref{fig:koster} more relevant to our
methodology (more in chapter \ref{chp:land_atm}).}
%
version is shown in figure \ref{fig:koster}. Although non-standard, figure
\ref{fig:koster} is referred to as the \emph{Koster diagram} throughout this
document.
%
The Koster diagram partitions $E$ into regimes of different behavior with
respect to soil moisture content ($m$) and net surface radiative flux
$F\subt{net} (\equiv F{-}\Flw\up \pt$).
%
Observational evidence for the Koster diagram has been documented in
\citet{dirmeyer06} and \citet{teuling09}.
%

For large values of soil moisture, $E$ is unaffected by variations in soil
moisture; its dominant control becomes the amount of radiation at the land
surface. In other words, the evaporative fraction ($LE\big/F\subt{net}$) is
independent of soil moisture in this regime and $E$ is equal to the
\emph{potential evapotranspiration} ($E\subt{pot}$). This evapotranspiration
regime is labeled as \emph{energy-limited}.
%
For soil moisture values below some critical value ($m\subt{crit}$ in figure
\ref{fig:koster}), the evaporative fraction (as well as $E$) is strongly
controlled by soil moisture variations: the \emph{moisture-limited} $E$ regime.
%
\citet{koster09_droughts} proposed that the two-regime behavior can be explained
mostly by vegetation physiology: to conserve moisture plants tend to constrict
the stomatal apertures on their leaves when soil moisture around them is low.
The reduced apertures impede the flow of water through the plants to the
atmosphere, reducing transpiration.  As soil moisture increases, the
constriction of the stomatal aperture eases and plants transpire more. At some
moisture value (\ie $m\subt{crit}$), plants no longer feel water stressed:
further wetting does not cause further opening of the stomata and increasing
transpiration.
%

% Factor limiting growth (maybe) link to Koster 08 argument for the LE/F curve.
% Make analogy with \citet{nemani03} and net plant production.

Evapotranspiration can be linked to surface temperature variability through the
surface energy budget (equation \ref{eq:sfc_engy}).
%
In the energy-limited $E$ regime, radiation forcing anomalies are effectively
damped by large latent heat fluxes ($LE$). Whereas, the moisture-limited $E$
regime is characterized by smaller latent heat fluxes.
%
Therefore, given a radiation forcing anomaly, it is plausible to suppose that a
surface temperature anomaly is larger in the moisture-limited regime.
%
However, the above argument makes no reference to other land-atmosphere process
affecting surface temperature (\eg the other constituents of equation
\ref{eq:sfc_engy}).
% 
That is, the Koster diagram lacks a physical link between soil moisture and
surface temperature variability.
%
%While useful, the Koster diagram is incomplete.

There are many processes that affect the link between soil moisture and surface
temperature variability.
%
For instance, in a review paper, \citet{sene10} proposed that decreasing soil
moisture leads to decreased evapotranspiration which in turn increases soil
moisture: a negative feedback loop.
%
Simultaneously, decreased evapotranspiration reduces surface latent cooling
leading to a temperature increase and thus contributes to surface temperature
variability.
%
%The temperature increase is associated with a greater $E$ demand (through the
%Clausius-Clapeyron relation).
%
If the $E$ response to $T$ is more pronounced than its response to $m$, a
positive feedback loop is formed between decreasing $m$ and increasing $T$.
%
In addition, decreased $E$ depletes the atmosphere from a moisture source
potentially leading to decreased precipitation, forming a separate positive
feedback loop involving soil moisture.
%
This mechanism is often referred to as the soil moisture-precipitation feedback
\citep{dodorico04}. The process where soil moisture becomes a source for
atmospheric moisture is called soil-moisture recycling
\citep{eltahir94,eltahir96,dominguez06,vanderent10}.
%
While these are plausible mechanisms linking soil moisture and surface
temperature variability, quantifying each branch of the above feedbacks loops
would require GCM simulations, which have been shown to contain large errors in
the \twentyth climate (see figure \ref{fig:cmip5_Var_T_bias}).

Alternatively, the concept of \emph{soil moisture memory} is deemed to be a key
aspect of land-atmosphere interactions \citep{sene06_memory}.
%
Whereas atmospheric variables, such as the radiation forcing and precipitation,
are often characterized as white noise signals on seasonal time scales, soil
moisture anomalies can remain coherent across several months.
%
Hence, soil moisture anomalies can have prolonged impacts on surface
temperature.
%
That said, mechanistic descriptions of how soil moisture memory impacts surface
temperature variability use Koster diagram-like arguments
\citep{sene06_memory,vidale07,jaeger11} which is far from ideal.

% -------------------------------------------------------------------------------

%% Outline of the document

In this study, we attempt to explicitly link land-atmosphere interactions to
surface temperature variability. In terms of complexity, our approach lies
in-between the Koster diagram, which links only evapotranspiration with soil
moisture and net radiation at the surface, and complex feedbacks loops only
quantifiable through numerical simulations.
%
In short, our toy model makes a three-way link between soil moisture,
land-atmosphere interactions and surface temperature variability. 
%
The toy model serves as a framework:
%
\emph{(i)} for understanding the links between soil moisture, land-atmosphere
interactions and surface temperature variability, 
%
\emph{(ii)} for understanding the source of the biases in surface temperature
variance in state-of-the-art GCMs and 
%
\emph{(iii)} for formulating a more insightful outlook on the projected
temperature variance in the \twentyfirst century.

The thesis is structured as follows. 
%
Chapter \ref{chp:land_atm} presents the datasets used in this study and examines
our hypothesis which links GCM errors in summertime surface temperature
variability to errors in soil moisture.
%
In chapter \ref{chp:toy_model}, we develop the toy model. The fundamental
assumptions are presented and justified. Additionally, the toy model's
performance is assessed in detail.
%
In chapter \ref{chp:interpret}, we use the toy model to better understand
\emph{(i)} the links between soil moisture, land-atmosphere interactions and
surface temperature variability in two state-of-the-art GCMs (\ccsm, \hadgem)
and two reanalysis products (\era, \ncep).
%
Chapter \ref{chp:apps} presents two applications of the toy model: attributing
GCM errors, point \emph{(ii)}, and investigating late \twentyfirst century
projections, point \emph{(iii)}. A summary and suggestions for future work are
included in chapter \ref{chp:conclu}.

Note that all figures and tables in this document are placed at the end of the
subsection of first reference. All figures found in this document as well as
supplementary figures are saved in a folder called \FigFolder which is available
upon request.  The author can be contacted via emal at \Email.

%  %% when talking about chapter 2!
%  Make distinction between \emph{GCMs} and \emph{reanalysis products}.
%  \emph{Datasets} and \emph{observations}.
%% datasets, GCMs ...
%Note that, in this document, we refer to the \emph{datasets} (\ccsm, \hadgem,
%\ncep, \era) in a separate manner than the University of Delaware observations
%(\eg in figure \ref{fig:obs_T_P}) which are referred to as simply
%\emph{observations}. 

% -------------------------------------------------------------------------------

%% figures and tables
\afterpage{\clearpage}
\newpage

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap1/cmip5_Var_T_bias}

\caption[Ratio of a CMIP5 ensemble mean of summer average monthly surface
temperature variance to University of Delaware observations (1969-1999).]%
%
{Ratio of the ensemble average variance in summer average monthly surface
temperature variance from the CMIP5 GCMs to the observed (University of
Delaware) summer average monthy surface temperature variance.
%
The model ouputs are taken from the historical runs of the CMIP5 GCMs over the
same period as the observed variance: 1969-1999.
%
See table \ref{tab:cmip5} for a list of ensemble members. Summer is defined as
JJA in the Northern Hemisphere and DJF in the Southern Hemisphere.}

\label{fig:cmip5_Var_T_bias}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap1/cmip3_Var_T_bias}

\caption[Ratio of a CMIP3 ensemble mean of summer average monthly surface
temperature variance to University of Delaware observations (1969-1999).]%
%
%{Ratio of the ensemble average variance in summer average monthly surface
%temperature variance from the CMIP3 GCMs to the observed (University of
%Delaware) summer average monthy surface temperature variance.
%
%The model ouputs are taken from the historical runs of the CMIP5 GCMs over the
%same period as the observed variance: 1969-1999.
%
{As in figure \ref{fig:cmip5_Var_T_bias} but using an ensemble of CMIP3 GCMs.
%
See table \ref{tab:cmip3} for a list of ensemble members.}
%
%Summer is defined as
%JJA in the Northern Hemisphere and DJF in the Southern Hemisphere.}

\label{fig:cmip3_Var_T_bias}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.9\linewidth]{koster}

\caption[The Koster diagram adapted from \citet{koster09_droughts}.]%
%
{The Koster diagram adapted from \citet{koster09_droughts}. The evaporative
fraction ($LE\big/F\subt{net}$) as a function of soil moisture content ($m$).
The symbol $m\subt{wilt}$ is the wilting point and $m\subt{crit}$ is the minimum
soil moisture value where evapotranspiration ($E$) is equal to the potential
evapotranspiration ($E\subt{pot}$), delimiting the moisture-limited and the
energy-limited evapotranspiration regimes.} 


\label{fig:koster}
\end{center}
\end{figure}

%\newpage

% old one with all members
%\begin{table}[H]
%\begin{center}
%
%\begin{tabular}[t]{c|c|c|c}
%  & Institutions & Climate Models & Ensemble Members \\ \hline\hline
%  & CSIRO (Australia)   & ACCESS      & 1 \\
%  & NCAR (USA)          & CCSM4.0     & 6 \\
%  & CNRM (France)       & CM5         & 10 \\
%  & CSIRO (Australia)   & Mk5         & 1 \\
%  & Env. Canada         & CanESM2     & 5 \\
%  & GFDL (USA)          & GFDL-CM3         & 1 \\
%  &                     & GFDL-ESM2G       & 1 \\
%  &                     & GFDL-ESM2M       & 1 \\
%  & Hadley Centre (UK)  & HadCM3      & 4 \\
%  &                     & HadGEM2-AO  & 1 \\
%  &                     & HadGEM2-CC  & 1 \\
%  &                     & HadGEM2-ES  & 4 \\
%  & IPSL (France)       & CM5A-LR     & 4 \\
%  & MIROC (Japan)       & MIROC-ESM-CHEM    & 1 \\
%  &                     & MIROS-ESM         & 1 \\
%  &                     & MIROC4h     & 1 \\
%  &                     & MIROC5      & 1 \\
%  & MPI (Germany)       & MPI-ESM-LR      & 1 \\
%  &                     & MPI-ESM-MR      & 3 \\
%  &                     & MPI-ESM-P       & 2 \\
%  & MRI (Japan)         & CGCM3       & 5 \\
%  & NCC (Norway)        & NorESM1-ME  & 1 \\
%  &                     & NorESM1-M   & 3 \\
%  & BCC (China)         & BCC-CSM1-1      & 3 \\
%  & INM (Russia)        & INMCM4      & 1 \\ \hline
%  Total:  & 14 & 25 & 63 
%\end{tabular}
%
%\end{center}
%
%\caption{List of CMIP5 ensemble members used in figure
%\ref{fig:cmip5_Var_T_bias}. } \label{tab:cmip5}
%
%\end{table}

\begin{table}[H]
\begin{center}

\begin{tabular}[t]{c|c|c|c}
  & Institutions & Climate Models  \\ \hline\hline
  & CSIRO (Australia)   & ACCESS       \\
  &                     & Mk5         \\
  & NCAR (USA)          & CCSM4.0      \\
  & CNRM (France)       & CM5         \\
  & Env. Canada         & CanESM2     \\
  & GFDL (USA)          & GFDL-CM3    \\
  &                     & GFDL-ESM2G  \\
  &                     & GFDL-ESM2M  \\
  & Hadley Centre (UK)  & HadCM3      \\
  &                     & HadGEM2-AO  \\
  &                     & HadGEM2-CC  \\
  &                     & HadGEM2-ES  \\
  & IPSL (France)       & CM5A-LR     \\
  & MIROC (Japan)       & MIROC-ESM-CHEM  \\
  &                     & MIROS-ESM       \\
  &                     & MIROC4h     \\
  &                     & MIROC5      \\
  & MPI (Germany)       & MPI-ESM-LR  \\
  &                     & MPI-ESM-MR  \\
  &                     & MPI-ESM-P   \\
  & MRI (Japan)         & CGCM3       \\
  & NCC (Norway)        & NorESM1-ME  \\
  &                     & NorESM1-M   \\
  & BCC (China)         & BCC-CSM1-1  \\
  & INM (Russia)        & INMCM4      \\ \hline
  Total:  & 14 & 25 
\end{tabular}

\end{center}

\caption[List of CMIP5 ensemble members used in figure
\ref{fig:cmip5_Var_T_bias}.]%
%
{List of CMIP5 ensemble members used in figure \ref{fig:cmip5_Var_T_bias}. Only
outputs from the models' first ensemble member (code name \texttt{r1ilp1}) was
used for figure \ref{fig:cmip5_Var_T_bias}.} 

\label{tab:cmip5}

\end{table}

\begin{table}[H]
\begin{center}

\begin{tabular}[t]{c|c|c|c}
  & Institutions & Climate Models  \\ \hline\hline
  & CSIRO (Australia)   & CSIRO3.5       \\
  & NCAR (USA)          & CCSM3.0      \\
  &                     & PCM1      \\
  & CNRM (France)       & CNRM-CM3       \\
  & Env. Canada         & CGCM3.1     \\
  & GFDL (USA)          & GFDL2.0    \\
  &                     & GFDL2.1    \\
  & NASA GISS (USA)     & GISS (ER)    \\
  & Hadley Centre (UK)  & HadGEM1      \\
  & IPSL (France)       & IPSL-CM3     \\
  & MPI (Germany)       & ECHAM5      \\
  & MIUB (Germany)      & MIUB      \\
  & MRI (Japan)         & CGCM3       \\
  & BCCR (Norway)       & BCCR-BCM2.0  \\
  & INM (Russia)        & INM-CM3.0   \\ \hline
  Total:  & 13 & 15 
\end{tabular}

\end{center}

\caption[List of CMIP3 ensemble members used in figure
\ref{fig:cmip3_Var_T_bias}.]%
%
{List of CMIP3 ensemble members used in figure \ref{fig:cmip3_Var_T_bias}. Only
outputs from one of the models' ensemble member was used for figure
\ref{fig:cmip3_Var_T_bias}.} 

\label{tab:cmip3}

\end{table}

%\begin{figure}[H]
%\begin{center}
%
%\sidesubfloat[]{%
%  \includegraphics[]{sene10_loops1}}
%\vskip1em
%\sidesubfloat[]{%
%  \includegraphics[]{sene10_loops2}}
%
%\caption{\citet{sene10} land-atmosphere feedback loops.}
%
%\label{fig:sene10_loops}
%\end{center}
%\end{figure}

%\newpage
% -------------------------------------------------------------------------------
