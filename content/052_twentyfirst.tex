%
% Chapter 5
% 
% -------------------------------------------------------------------------------
\section{Temperature variability at the end of the 21st century}
\label{sec:twentyfirst}
%
% scenario_mbar_Ov20 , scenario_rel_diff_mbar_Ov20
% scenario_sig_T_Ov20 , scenario_ratio_Var_T_Ov20 
%
% hadgem1_scenario_regime_shift , hadgem1_scenario_ratio_Var_T_shift
%
% -------------------------------------------------------------------------------

Several IPCC GCMs project significant changes in mean-state soil moisture by the
end of the \twentyfirst century over many locations \citep{ipcc_ar4_wg1}. Using
results taken from our analysis of the toy model, we investigate whether
projected \twentyfirst century trends in temperature variability can be linked
to changes in mean-state soil moisture.
%
For the projected changes in the \twentyfirst century, we use outputs from the
GCMs forced by the SRES A1b emission scenario and we restrict our analysis to
the same two GCMs: the \ccsm and the \hadgem.

Figure \ref{fig:scenario_mbar} (a) presents the \twentyth century (1969-1999 as
in previous analysis) and projected \twentyfirst century (defined as the
2069-2099 time period) summertime mean-state (surface layer) soil moisture.  
%
Note that the panels on the left-hand side of figure \ref{fig:scenario_mbar} (a)
are identical to figure \ref{fig:comp_mbar}.
%
Previously, we showed that the global mean soil moisture is an adequate
delimiter between the land-surface damped and land-surface amplified temperature
variability regimes (equivalently between the energy-limited and
moisture-limited evapotranspiration regimes, see figure
\ref{fig:comp_scale_gammaT_OvGM}). 
%
By assuming that this particular value of soil moisture is able to delimit the
two temperature variability regimes in the \twentyfirst century as well, we can
identify locations susceptible to a large change in temperature variability from
figure \ref{fig:scenario_mbar} by finding the regions that have shifted from the
land-surface damped (amplified) to the land-surface amplified (damped) regime:
in these regions we expect there to be a large increase (decrease) in
temperature variability.
%
In figure \ref{fig:scenario_mbar}, the 20\mm soil moisture contour
(approximatively equal to the global mean soil moisture for the two GCMs, see
table \ref{tab:m_GM}) is highlighted with a solid green line.
%
The \ccsm projects only minimal changes in soil moisture (less than 10\%
everywhere), whereas, the \hadgem projects a large (up to 50\%) decrease in soil
moisture in the \twentyfirst century in the central US, Western and Southern
Europe.
%
Importantly, the extent of the mid-latitude wet climate zone shrinks
significantly, especially in Western Europe and the in central US.
%
%As a result, the \hadgem projects a significant increase in temperature
%variability for Western Europe and the central US by the end of the
%\twentyfirst century.

Figure \ref{fig:scenario_Var_T} compares \twentyfirst century (SRES A1b
2069-2099) and \twentyth century (1969-1999 as before) summer average (surface)
temperature standard deviations. 
%
We choose to not detrend the \twentyth century (as in previous analysis) and
\twentyfirst century temperature anomalies in generating figure
\ref{fig:scenario_Var_T}; hence, note the inter-annual variability is slightly
overestimated in figure \ref{fig:scenario_Var_T}.
%
Nonetheless, we notice that some of the largest changes in temperature
variability occur near the \twentyth century boundary between land-surface
amplified and damped regimes (shown as the solid black line).
%
Large $\Var(T)$ changes also occur in high-latitude regions in both GCMs
possibly related to melting frozen soil water.
%
Finally, the \hadgem projects a large (relative) $\Var(T)$ increase in the
tropics which is not correlated with drying surface layer soils (see figure
\ref{fig:scenario_mbar}).
%
Dryland locations of the \twentyth century climate that remain dryland regions
at the end of the \twentyfirst century show the smallest changes in temperature
variability in figure \ref{fig:scenario_Var_T}.
%
This includes Eastern Europe and Spain in the \hadgem and the continental US and
Southern Europe in the \ccsm.
%
This suggests that temperature variability is independent of mean-state soil
moisture within the land-surface amplified regime, consistent with our previous
findings (see section \ref{ssec:role_of_m}).
%
To better assess whether the large increase temperature variability over the
course of the \twentyfirst century in the \hadgem (displayed in figure
\ref{fig:scenario_Var_T}) are due to a shift of temperature variability regime,
consider the following expression:

\begin{equation}
%
  \Big\{ \pt \mcal{H} \big[
  (\pt \mbar\subt{20th} - m\subt{crit} \pt) \times 
  (\pt \mbar\subt{21st} - m\subt{crit} \pt) \big]
  - 1 \pt \Big\} \;\times\;
  \sgn\big[ (\pt \mbar\subt{20th} - m\subt{crit} \pt) \big]
  \enskip.
  \label{eq:regime_shift}
%
\end{equation}

\noindent%
%
In the above, $\mcal{H}(\,\cdot\,)$ is the Heaviside step function,
$\sgn(\,\cdot\,)$ is the sign function, $m\subt{crit}$ is the critical soil
moisture value delimiting temperature variability regimes (equal to the global
mean soil moisture value in over the 1996-1999 time period, see sections
\ref{sec:data_results} and \ref{sec:two_regimes}), $\mbar\subt{20th}$ is the
summertime mean soil moisture in the \twentyth century (years 1969-1999) and
$\mbar\subt{21st}$ is the summertime mean soil moisture in the \twentyfirst
century (years 2069-2099).
%
Equation \ref{eq:regime_shift} defines a mask that yields a negative (positive)
value at a location that experiences a temperature variability regime shift from
the land-surface damped (amplified) regime to the land-surface amplified
(damped) regime.
%
The results are shown in panel (a) of figure \ref{fig:scenario_shift}.  Note
that only locations where expression \ref{eq:regime_shift} is different than
zero are plotted.
%
We note that the \hadgem features shifts that are unidirectional from the
land-surface damped regime to the land-surface amplified regime over the course
of the \twentyfirst century.
%
Panel (b) of figure \ref{fig:scenario_shift} shows the summertime ratio of the
\twentyfirst century to the \twentyth century temperature variance in locations
that are projected to experience a regime shift.
%
We see that temperature variance is greater in the \twentyfirst century than in
the \twentyth century in every region where soils have dried enough to shift
from the land-surface damped regime to the land-surface amplified regime.
%
The results of figure \ref{fig:scenario_shift} suggest that a shift in
temperature variability regime from the land-surface damped to the land-surface
amplified due to dryer soils in summer may lead to a large (greater than 25\%)
increase in temperature variability.

It follows to ask: can we believe the projected changes in surface temperature
variance? Recall that both GCMs have too little soil moisture in summer (and
thus and too much temperature variance) in the \twentyth century simulations.
%
Since our results show that some of the largest changes in temperature variance
occur when a given location shifts from wet (land-surface damped) to dry
(land-surface amplified) conditions it is likely that the area of land that will
experience an increase in temperature variance is underestimated in the \ccsm
and \hadgem.
%
%Then, as both GCMs analyzed in this study suffer from large soil moisture
%deficits (compared to the \ncep and \era reanalysis products), predicting
%locations most susceptible to large $\Var(T)$ trends solely from GCM data is
%fallacious.
%
We can formulate a better estimate by considering the reanalysis (say the
\era's) global mean soil moisture value as the present-day delimitation between
the land-surface amplified and damped regimes as initial condition to the GCMs'
$\Var(T)$ trends.
%
From previous analysis (\eg figure \ref{fig:comp_Cor_EF_OvGM}), the \hadgem
places the amplified/damped boundary too far east in the central US and too far
west in Europe in comparison to the \era reanalysis product.
%
From the IPCC AR4 report, regions where soil moisture is robustly expected to
decrease in the \twentyfirst century include the central US and Western Europe
\citep{ipcc_ar4_wg1}.
%
Note that the IPCC ensemble results are qualitatively similar to the \hadgem
simulation shown in figure \ref{fig:scenario_mbar}.
%
Therefore, with insight from the toy model on temperature variability regimes,
we infer that the central US and Western Europe are most susceptible to a large
amplification of temperature variability over the course of the \twentyfirst
century.

%% Change in soil moisture over the Amazon and the Congo are not robust!

\newpage

%% figures
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.96\linewidth]{chap5/scenario_mbar_Ov20}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.9\linewidth]{chap5/scenario_rel_diff_mbar_Ov20}}

\caption[Summertime surface layer soil moisture for the years 1969-1999 and
2069-2099.]%
%
{Panel (a): Summertime surface layer soil moisture for the years 1969-1999
(-20c3m; left panels) and 2069-2099 according to the SRES A1B scenario (-A1b;
right panels).
%
The solid black line is the 20\mm soil moisture contour for each simulation.
The top panels show the \ccsm results and bottom panels the \hadgem results.
Panel (b): Summertime relative difference between the 2069-2099 and the
1969-1999 soil moisture, with respect to the 1969-1999 value. The solid green
line in each panel is the 1969-1999 20\mm soil moisture contour.}

\label{fig:scenario_mbar}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.96\linewidth]{chap5/scenario_sig_T_Ov20}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.9\linewidth]{chap5/scenario_ratio_Var_T_Ov20}}

\caption[Square-root of summer average surface temperature variance for the
years 1969-1999 and 2069-2099]%
%
{Panel (a): Square-root of summertime surface temperature variance for the years
1969-1999 (-20c3m; left panels) and 2069-2099 according to the SRES A1B scenario
(-A1b; right panels).  The solid black line is the 20\mm soil moisture contour
for each simulation.  
%
The top panels show the \ccsm results and bottom panels the \hadgem results.
Panel (b): Summertime ratio of the 2069-2099 to the 1969-1999 surface
temperature variance.  The solid black line in each panel  is the 1969-1999
20\mm soil moisture contour.}

\label{fig:scenario_Var_T}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\sidesubfloat[]{%
  \includegraphics[width=0.9\linewidth]{chap5/hadgem1_scenario_regime_shift}}
\vskip1em
\sidesubfloat[]{%
  \includegraphics[width=0.9\linewidth]{chap5/hadgem1_scenario_ratio_Var_T_shift}}

\caption[Summertime ratio of the 2069-2099 temperature variance to the 1969-1999
temperature variance in regions projected to experience a land-surface regime
shift over the course \twentyfirst century.]%
%
{Panel (a): Shift in temperature variability (or evapotranspiration) regimes
from the 1969-1999 time period (20c3m simulation) to the 2069-2099 time period
(SRES A1B scenario).
%
Brown (green) indicates locations that are projected to shift from the
land-surface damped (amplified) regime to the land-surface amplified (damped)
regime. 
%
This figure was generated using equation \ref{eq:regime_shift}.
%
Panel (b): Summertime ratio of the 2069-2099 to the 1969-1999 surface
temperature variance in locations that are projected to experience a temperature
variability regime shift.
%
[\hadgem data].}

\label{fig:scenario_shift}
\end{center}
\end{figure}
