%
% Chapter 4
%
% -------------------------------------------------------------------------------
\section{Physical interpretation}
\label{sec:two_regime_interp}
%
% - Over wet soils recall that anomaly Bowen ratio is positive in 4.3.1?
% - make E' on SW' regressions ???
%
% -------------------------------------------------------------------------------

\subsection{Interpreting sensible heat flux anomalies}
\label{ssec:Hs_interp}
%
% - comp_scale_Var_LE_Hs_OvGM   , Var(LE)+Var(Hs) / Var(LE+Hs) 
%                                 with or without _OvGM ??
% - comp_Res_Hs_gain_E_OvGM     , Hs' param gain w.r.t m' by E' - 4 panels
% - gcms_Hs_regrs_gain_DeltaT   , Hs' param gain w.r.t m' (Tsk-T)' - 2 panels
% - gcms_aero_regrs     , (Tsk-T)' and C' onto m' - 4 panels
%
% -------------------------------------------------------------------------------

The most intriguing result encountered during our quest for the optimal toy
model parameterization in section \ref{sec:param} involved the sensible heat
flux.  Of the toy model's two state variables and two forcing functions, we
found that $H_s'$ is best predicted by soil moisture anomalies --- globally
across the four analyzed datasets.
%
Here, we further examine the sensible heat flux anomalies to better understand
the proposed link with soil moisture.

In past publications on land-atmosphere interactions, the effects of sensible
heat on surface temperature are often neglected. Many studies consider
evapotranspiration as the principal control on $\Var(T)$ and sensible heat
merely as constrained by evapotranspiration.
%
Several studies, such as \citet{schar04}, \citet{koster09_droughts}
\citet{jaeger11}, \citet{fischer07} and \citet{taylor12}, mention that
\emph{soil moisture controls the partitioning of latent and sensible heat} with
no further comment.
%
More specifically, \cite{sene10} proposed (without investigation) that when soil
moisture limits evapotranspiration, more energy is transported by the sensible
heat flux.

In figure \ref{fig:comp_Cor_mHs}, we showed that sensible heat flux is
negatively correlated with soil moisture almost everywhere. 
%
Since $m'$ and $E'$ are positively correlated in moisture-limited regions,
$H_s'$ and $E'$ have opposite signs in moisture-limited regions; anomalies in
the latent heat flux are compensated for by opposite-signed anomalies in the
sensible heat flux.
%
Figure \ref{fig:comp_scale_Var_LE_Hs_OvGM} quantifies this compensation: it
presents the ratio of the sum of the two turbulent heat flux variance,
$\Var(LE){+}\Var(H_s)$, to the variance of their sum, $\Var(LE{+}H_s)$.
%
Over dry soils the compensation is significant: $E$ and $H_s$ covary in such a
way to make the net effect of the turbulent fluxes a small fraction of the sum
of their individual variances (typically less than 10\%).

We then ask, are sensible heat flux anomalies triggered by evapotranspiration
anomalies? To investigate, we compare the fractions of $H_s$ variance explained
(equation \ref{eq:frac_Var}) by lone projections onto $m'$ and $E'$.
%
Figure \ref{fig:comp_Res_Hs_gain_E} shows the difference between the fraction of
$H_s$ variance explained by projecting $H_s'$ onto $E'$ and the fraction of
variance $H_s$ explained by projecting $H_s'$ onto $m'$. 
%
We notice that $E'$ is a slightly better predictor of $H_s'$ than is $m'$ (shown
by the positive values in figure \ref{fig:comp_Res_Hs_gain_E}) over dry soils
suggesting that in fact $E'$ controls $H_s'$. 
%
Mechanistically, we expect that (surface) latent heat affects the skin
temperature $\Tsk$ more than the surface (air) temperature $T$.
% 
As the sensible heat flux is proportional to the surface-to-air temperature
difference ($(\Tsk{-}T)$, see equation \ref{eq:Hs_bulk}), the $H_s$ response
would oppose $E$.
%
That is, evapotranspiration cools the surface reducing $\Tsk$ which in turn
reduces the sensible heat lost by turbulence.

To further examine the sensible heat flux anomalies, we proceed by decomposing
the sensible heat flux as:

\begin{equation}
  H_s\;=\; C\,(\pt \Tsk{-}T \pt) 
  \enskip,
  \label{eq:Hs_decomp}
\end{equation}

\noindent%
%
where $C$ is the drag coefficient. It is computed\footnote{%
%
The \era does not archived the surface skin temperature. Hence, only the GCM
datasets ($\Tsk$ is the \texttt{ts} archive variable) are analyzed herein in
this section.}
%
using the ratio $C=H_s\big/(\Tsk{-}T)$ where $H_s$ and $(\Tsk{-}T)$ are taken
from the GCM archives. 
%
Figure \ref{fig:gcms_Hs_regrs_gain_DeltaT} shows the difference between the
fraction of $H_s$ variance explained by projecting $H_s'$ onto $(\Tsk{-}T)'$ and
the fraction of $H_s$ variance explained by projecting $H_s'$ onto $m'$. 
%
We see that $(\Tsk{-}T)'$ is a better predictor of $H_s'$ than $m'$ almost
everywhere in the two GCMs (from the positive values in figure
\ref{fig:gcms_Hs_regrs_gain_DeltaT}). 
%
This is consistent with a link between $E'$ and $H_s'$ via $(\Tsk{-T})'$ (\ie
turbulence) over dry soils as depicted in figure \ref{fig:comp_Res_Hs_gain_E}.
%
Over wet soils, interpreting figures \ref{fig:comp_Res_Hs_gain_E} and
\ref{fig:gcms_Hs_regrs_gain_DeltaT} is not as straight forward.
Evapotranspiration is a poor predictor of $H_s'$ and projecting $H_s'$ onto
$(\Tsk{-}T)'$ yields vastly different results in tropical climates and high
latitude climates, suggesting that perhaps the mechanisms linking $H_s'$ and
soil moisture are different in those two wet climate zones.

We next examine the effects of soil moisture on the sensible heat flux anomalies
by projecting both $H_s$ components (as in equation \ref{eq:Hs_decomp}) onto
soil moisture anomalies.
%
The left panels of figure \ref{fig:gcms_aero_regrs} present the fraction of drag
coefficient ($C$) variance explained by a projection onto $m'$ and the right
panels of figure \ref{fig:gcms_aero_regrs} present the fraction of
surface-to-air temperature difference ($\Tsk{-}T$) variance explained by a
projection onto $m'$.
%
Soil moisture appears to significantly control ($\Tsk{-}T$) fluctuations in most
dry and wet soil locations. But interestingly, soil moisture is able to predict
drag coefficient anomalies in the \hadgem's tropical climate zone.

We then ask: how can soil moisture anomalies induce sensible heat flux
anomalies, as well as $(\Tsk{-}T)$ anomalies, without the effects of evaporative
cooling on surface air turbulence?
%
In this study, we propose a mechanism involving the soil heat capacity, but we
do not attempt to demonstrate it leaving the above question unanswered.
%
We propose that soil moisture anomalies create appreciable anomalies in the heat
capacity of the surface soil layer. Adding water on saturated soils increases
the heat capacity. The limiting case would make the soil heat capacity equal the
heat capacity of liquid water, which is more than double the heat capacity of
most dry soils \citep{soils}.
%
We then expect the variations in heat capacity to affect the skin temperature
more readily than the surface (air) temperature. 
%
Therefore, $(\Tsk{-}T)$ would decreases with increasing soil moisture in the
absence of large variations in the drag coefficient.

In summary, our results suggest that the $H_s$ is linked to $E$ over dry soils
(\ie land-surface amplified regime, see figure \ref{fig:comp_Res_Hs_gain_E})
through differential heating/cooling of the lowermost 2\m of atmosphere. 
%
The toy model describes the interplay between $H_s$ and $E$ through the coupling
coefficient ($\chi$) or, more precisely, through $\nuE{-}\nuHs$. Hence, we
conclude that the coupling coefficient's numerator is governed by the efficiency
at which $E$ anomalies are converted into sensible heat flux anomalies.
%
For completeness but without investigations, we propose that over wet soils (\ie
land-surface damped regime) the sensible heat flux's dependence on soil moisture
is related to effects of soil moisture anomalies on the soil heat capacity.

%\newpage
\vfill

%% figures and tables
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_scale_Var_LE_Hs_OvGM}

\caption[Summertime ratio of the sum of the latent and sensible heat flux
variances to the variance of their sum.]%
%
{Summertime ratio of the sum of the latent and sensible heat flux variances to
the variance of their sum $\big(\Var(LE)+\Var(H_s)\big)\big/\Var(LE{+}H_s)$
(colored contours). Superposed is the line where soil moisture is equal to the
global soil moisture (solid red line).}

\label{fig:comp_scale_Var_LE_Hs_OvGM}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/comp_Res_Hs_gain_E_OvGM}

\caption[Summertime difference between the fraction of sensible heat flux
variance explained by a projection onto evapotranspiration and onto soil
moisture.]
%
{Summertime difference between the fraction of $H_s$ variance explained by
projecting $H_s'$ onto $E'$ and the fraction of variance $H_s$ explained by
projecting $H_s'$ onto $m'$ (colored contours). 
%
Positive (negative) values indicate that $E'$ explains more (less) $H_s$
variance than $m'$.
%
Superposed is the line where soil moisture is equal to the global soil moisture
(solid green line).}

\label{fig:comp_Res_Hs_gain_E}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=0.6\linewidth]{chap4/gcms_Hs_regrs_gain_DeltaT}

\caption[Summertime difference between the fraction of sensible heat flux
variance explained by a projection onto the surface-to-air temperature
difference and onto soil moisture.]%
%
{Summertime difference between the fraction of $H_s$ variance explained by
projecting $H_s'$ onto $(\Tsk{-}T)'$ (see equation \ref{eq:Hs_decomp}) and the
fraction of variance $H_s$ explained by projecting $H_s'$ onto $m'$. Positive
(negative) values indicate that $(\Tsk{-}T)'$ explains more (less) $H_s$
variance than $m'$.
%
[\ccsm (top panel) and \hadgem (bottom panel) data].}

\label{fig:gcms_Hs_regrs_gain_DeltaT}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{chap4/gcms_aero_regrs}

\caption[Fraction of drag coefficient and surface-to-air temperature difference
variance explained by a projection onto soil moisture anomalies.]%
%
{Left panels: fraction of drag coefficient ($C$; see equation
\ref{eq:Hs_decomp}) variance explained by a projection onto $m'$. Right panels:
fraction of surface-to-air temperature difference ($\Tsk{-}T$) variance
explained by a projection onto $m'$.  [\ccsm (top panels) and \hadgem (bottom
panels) data].}

\label{fig:gcms_aero_regrs}
\end{center}
\end{figure}

\subsection{Interpreting the land-surface amplified regime}
\label{ssec:amplified_interp}
%
% diagram: tm_interp_dry
%
% -------------------------------------------------------------------------------

Here, we interpret the land-surface response to given forcing anomalies in
typical locations of the land-surface amplified temperature variability regime.
This discussion summarizes several results presented in this chapter.

Figure \ref{fig:tm_interp_dry} is a cartoon illustrating the land-surface
amplified response. Arrows represent fluxes and state variable are circled.  As
in the toy model expression for temperature anomalies (equation \ref{eq:tm_T}),
we separate the forcings as the non-precipitating radiation forcing $F_0'$ and
precipitation $P'$. The cartoon is drawn with respect to positive $F_0'$ and
$P'$; land-surface responses to negative anomalies can be determined by
multiplying each process by $(-1)$.

Starting from the response to $F_0'$ (the left panel), a positive $F_0'$ is
converted to a positive temperature anomaly with little damping by the land
surface as the evapotranspiration efficiency ($\lambda$) is small.  In other
words, $F_0'$ is largely unattenuated.

Given a positive $P'$, the radiative effects of precipitation cool the surface
(hence the blue $\alpha P'$ arrow). Since $\lambda$ is small, $E$ does not
respond to precipitating radiation forcing anomalies. 
%
In turn, the $P'>0$ induces a positive soil moisture anomalies. A fraction of it
runoffs (instantaneously) and infiltrates (on time scales of about a week).
%
The positive soil moisture anomaly causes an increase in $E$ (the
moisture-limited behavior) which cools the skin temperature and reduces the
turbulent transport of sensible heat; thus, partially compensates the cooling
effects of evapotranspiration. 
%
The net effect of the turbulent (sensible plus latent) heat fluxes is to cool
the land surface, amplifying the radiative effect of precipitation.
%
Specifically, the surface temperatures in the land-surface amplified regime are
more variable than would be expected solely form the input radiation forcing due
to effect of precipitation (of significant component of $F'$) on the moisture
budget and subsequently on the turbulent heat fluxes.
%
In figure \ref{fig:hadgem1_scale_engy_OvGM}, we showed that the (surface) latent
heat flux variability $L^2\Var(E)$ is even greater than the direct radiation
forcings $\Var(F)$ in the amplified regime. 
%
Hence, latent heat flux variability --- controlled by soil moisture
variability --- significantly alters surface temperatures in the amplified
regime.

%\comment{I was thinking a making a summary table of the associated made over dry
%and wet soils regarding ($\corr(E,m)$, $\chi$, $\lambda$, $\gamma^2\Var(T)$ and
%$\mbar$). Relevant or redundant?}

%Make table of findings over dry soils. ($\corr(E,m)$, $\chi$, $\lambda$,
%$\gamma^2\Var(T)$ and $\mbar$).

%\vfill
%\newpage

%% figures and tables
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{tm_interp_dry}

\caption[Response of the land surface in the land-surface amplified regime, as
described by the toy model.]%
%
{Response of the land surface to $F_0'$ (left panel) and $P'$ (right panel) in
the land-surface amplified regime. Blue (red) arrows represent cooling (warming)
processes. The black arrow are moisture fluxes that behave the same in both
temperature variability regimes.}

\label{fig:tm_interp_dry}
\end{center}
\end{figure}

\subsection{Interpreting the land-surface damped regime}
\label{ssec:damped_interp}
%
% diagram: tm_interp_wet
%
% maybe fig to show that SW dominates ...
%
% -------------------------------------------------------------------------------

We turn our attention to the land-surface damped temperature variability regime.
Figure \ref{fig:tm_interp_wet} is analogous to figure \ref{fig:tm_interp_dry}
but is for the land-surface damped regions.

Given a positive non-precipitating radiation forcing anomaly, the land-surface
significantly \newline damps the temperature response (by 60\% in the tropics,
40\% in the high-latitudes, see figure \ref{fig:comp_fact_F0}) mostly by using a
large fraction of $F_0'$ for evapotranspiration (\ie $\lambda$ large). 
%
The sensible heat response to $F_0'$-generated soil moisture anomalies
(quantified by $\lambda\chi$) is small and omitted in figure
\ref{fig:tm_interp_wet}.

Precipitation anomalies have little net effect on surface temperature in the
damped regime (see figure \ref{fig:comp_fact_P}).
%
Surface cooling induced by radiative effects of precipitation ($\alpha P'$, for
$P'>0$) is balanced by warming associated with reduced $E$ (the energy-limited
behavior).
%
Precipitation anomalies generate soil moisture anomalies, a fraction of $P'$ is
lost to runoff and infiltration.
%
%That is, the proposed soil-moisture dependence of the sensible heat flux
%anomalies has little consequence in our interpretation of the tropics' surface
%temperature variability.
%
In high-latitude and mid-latitude wet climate zones, positive soil moisture
anomalies appreciably reduce the loss of sensible heat by turbulence (possibly
through a modification of the soils' heat capacity), further opposing the
radiative direct effects of precipitation.
%
In tropical climates, infiltration occurs readily and soil moisture anomalies
are short lived; hence, the net effect of soil moisture anomalies on $T'$ is
small.

In summary, in the land-surface damped regime, the effects on temperature of
both $F_0'$ and $P'$ are damped by the land surface. When evapotranspiration is
controlled by the radiation forcing, it damps the effects of radiation on
surface temperature.
%
In the land-surface amplified regime, $E$ is controlled by soil moisture
fluctuations and acts to amplify the effects of precipitating radiation forcing
anomalies on temperature anomalies.

% -- In other words $L^2\Var(E)/\Var(F)<1$ in section 4.2.2!

%\comment{Should I include a map showing that the shortwave component of
%$F'$ is responsible for most of the $F'$ control on $E'$? Is this important?}

%If we accept to above mechanism, then how can the heat capacity vary enough over
%the course of a month to affect the sensible heat while having negligible
%impacts on the (neglected) storage term?

%% figures and tables
\afterpage{\clearpage}

\begin{figure}[H]
\begin{center}

\includegraphics[width=\linewidth]{tm_interp_wet}

%\caption{Response of the land surface to $F_0'$ (left panel) and $P'$ (right
%panel) in the land-surface damped regime. Blue (red) arrows represent cooling
%(warming) processes.}

\caption[Same as figure \ref{fig:tm_interp_dry} but for the land-surface damped
regime.]%
%
{Same as figure \ref{fig:tm_interp_dry} but for the land-surface damped regime.}

\label{fig:tm_interp_wet}
\end{center}
\end{figure}
